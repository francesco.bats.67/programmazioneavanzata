<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<h5>Serie A1 di pallanuoto femminile (stagioni 2019-20 e 2020-21)</h5>

<h1>Elenco Squadre</h1>
 
<c:if test="${fn:length(messaggio) > 0}"><h4 style="color:red">${messaggio}</h4></c:if>

<div class="row">
	<div class="col-8">

<c:if test="${numeroSquadre lt 1}"><br><br><h3 style="color:red">nessuna squadra presente in archivio</h3></c:if>

<c:if test="${numeroSquadre gt 0}">

<i>Numero totale di squadre: ${numeroSquadre}</i>

		<table class="table table-striped">
			<thead>
				<tr class="info">
				    <th>Sigla</th>
		    		<th>Denominazione della società sportiva</th>
				    <th>Sede</th>
				</tr>
		 	</thead>
	 		<tbody>
	<c:forEach items="${squadre}" var="s">
 				<tr>
		    		<td>${s.idSquadra}</td>
 					<td class="text-uppercase"><a href="<c:url value="/squadre/${s.idSquadra}/vediDettagli/" />"><span class="glyphicon glyphicon-file"></span> ${s.denominazioneSquadra}</a></td>
		    		<td>${s.luogoSquadra}</td>
				</tr>
	</c:forEach>
				<tr class="info">
					<td colspan="5">
				</tr>
			</tbody>

		</table>

</c:if>

	</div>
	<div class="col-4" align="right">
		<img src="<c:url value="/media/wp.jpg"></c:url>" alt="SQUADRE"/>
	</div>
</div>


