<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
 
<c:url value="/atleti/gestione/salva" var="action_url" />

<h5>Serie A1 di pallanuoto femminile (stagioni 2019-20 e 2020-21)</h5>

<h1>
<c:if test="${empty atleta.idAtleta}">Inserimento di una nuova giocatrice</c:if>
<c:if test="${not empty atleta.idAtleta}">Modifica dati di ${atleta.nomeCompleto}</c:if></h1>

<c:if test="${fn:length(messaggio) > 0}"><h4 style="color:red">${messaggio}</h4></c:if>

<form:form method="POST" action="${action_url}" modelAttribute="atleta">

	<table class="table table-striped">

<c:if test="${empty atleta.idAtleta}">
		<tr>
			<td class="control-label col-xs-4"><form:label path="idAtleta">ID Atleta</form:label></td>
			<td><form:input path="idAtleta"/></td>
		</tr>
</c:if>
		<tr>
			<td class="control-label col-xs-4"><form:label path="nome">Nome</form:label></td>
			<td><form:input path="nome"/></td>
		</tr>
		<tr>
			<td class="control-label col-xs-4"><form:label path="cognome">Cognome</form:label></td>
			<td><form:input path="cognome"/></td>
		</tr>
<c:if test="${empty atleta.sesso}">
		<tr>
			<td class="control-label col-xs-4"><form:label path="sesso">Sesso</form:label></td>
			<td><form:input path="sesso"/></td>
		</tr>
</c:if>
		<tr>
			<td class="control-label col-xs-4"><form:label path="annoNascita">Anno di nascita</form:label></td>
			<td><form:input path="annoNascita"/></td>
		</tr>
		<tr>
			<td class="control-label col-xs-4"><form:label path="nazione">Nazionalità</form:label></td>
			<td><form:input path="nazione"/></td>
		</tr>
		<tr>
			<td><input type="submit" value="SUBMIT"/></td>
		</tr>
	</table>

	<form:hidden path="idAtleta" />
	<form:hidden path="sesso" />

</form:form>
	