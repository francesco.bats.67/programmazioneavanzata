<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<h5>Serie A1 di pallanuoto femminile (stagioni 2019-20 e 2020-21)</h5>

<h1>Elenco Giocatrici</h1>

<c:if test="${fn:length(messaggio) > 0}"><h4 style="color:red">${messaggio}</h4></c:if>

<c:if test="${numeroAtleti lt 1}"><br><br><h3 style="color:red">nessuna giocatrice presente in archivio</h3></c:if>

<c:if test="${numeroAtleti gt 0}">

<p>[<a href="<c:url value="/atleti/gestione/aggiungi/" />"> <span class="glyphicon glyphicon-plus"></span> INSERISCI UNA NUOVA GIOCATRICE</a> ]
- <i>Numero totale di giocatrici in archivio: ${numeroAtleti}</i></p>

<table class="table table-striped">
	<thead>
		<tr class="info">
			<td></td>
			<td>Nome e Cognome</td>		
			<td class="text-center">Anno di nascita</td>
			<td class="text-center">Nazionalità</td>
			<td></td>
		</tr>
	</thead>
 	<tbody>
<c:forEach items="${atleti}" var="a">
		<tr>
			<td>${a.idAtleta}</td>
			<td class="text-uppercase"><a href="<c:url value="/atleti/${a.idAtleta}/vediDettagli/" />"><span class="glyphicon glyphicon-file"></span> ${a.nomeCompleto}</a></td>
			<td class="text-center">${a.annoNascita}</td>
			<td class="text-center">${a.nazione}</td>
			<td class="text-center">[<a href="<c:url value="/atleti/gestione/${a.idAtleta}/modifica/" />"> <span class="glyphicon glyphicon-pencil"></span> modifica </a>]&nbsp;&nbsp;&nbsp;
				[<a href="<c:url value="/atleti/gestione/${a.idAtleta}/elimina/"/>"> <span class="glyphicon glyphicon-trash"></span> elimina </a>]</td>
		</tr>
</c:forEach>
		<tr class="info">
			<td colspan="5">
		</tr>
	</tbody>
</table>

</c:if>

<p>[<a href="<c:url value="/atleti/gestione/aggiungi/" />"> <span class="glyphicon glyphicon-plus"></span> INSERISCI UNA NUOVA GIOCATRICE</a> ]</p>


