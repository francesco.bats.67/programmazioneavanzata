<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authorize access="hasRole('ADMIN')" var="isAdmin" />
<sec:authorize access="hasRole('USER')" var="isUser" />
<sec:authorize access="isAuthenticated()" var="isAuth" />

 <div class="row justify-content-md-center">
    <div class="col-3"><img src="<c:url value="/media/wp_small.jpg"></c:url>" height="100%" alt="(..)"/><b><i>VELA WATERPOLO</i></b></div>
	<div class="col-9">
		<ul class="nav nav-tabs">
			<li><a href="<c:url value="/" />">HOME</a></li>
			<li><a href="<c:url value="/partite/elenca" />">PARTITE</a></li>
			<li><a href="<c:url value="/squadre/elenca" />">SQUADRE</a></li>
			<li><a href="<c:url value="/atleti/elenca" />">GIOCATRICI</a></li> 
			<li><c:if test="${isAuth}"><a href="<c:url value="/scores/elenca" />">SCORE</a></c:if></li>
			<li><c:if test="${isAuth}"><a href="<c:url value="/logout" />">LOGOUT</a></c:if></li>
			<li><c:if test="${! isAuth}"><a href="<c:url value="/login" />">LOGIN</a></c:if></li>
		</ul>
	</div>
	<div class="col-12"><p class="text-right"><c:if test="${isAuth}"><i>utente registrato: <b><sec:authentication property="principal.username" /></b></i></p></c:if></div>
</div>

<body>

<h2 style="color:red">Sorry, you do not have permission to view this page.</h2>

</body>

</html>