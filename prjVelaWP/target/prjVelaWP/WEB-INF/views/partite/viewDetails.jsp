<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
  
<h5>Serie A1 di pallanuoto femminile (stagioni 2019-20 e 2020-21)</h5>
  
<h1>Dettaglio Partita</h1>

<c:if test="${fn:length(messaggio) > 0}"><h4 style="color:red">${messaggio}</h4></c:if>

<div class="row">
	<div class="col-9">

<h4>
	<c:if test="${partita.esitoPartita == 'C'}"><span style="color:#CC0000">PARTITA DA GIOCARE</span> - 
		[ <a href="<c:url value="/partite/gestione/${partita.idPartita}/scegliFormazione/"/>">
		<span class="glyphicon glyphicon-edit"></span> SCEGLI LE FORMAZIONI</a> ]
	</c:if>
	<c:if test="${partita.esitoPartita == 'F'}"><span style="color:#CC0000">PARTITA DA GIOCARE</span> - 
		[<a href="<c:url value="/scores/${partita.idPartita}/scoreboard"/>">ACQUISISCI LO SCORE DELLA PARTITA</a>]
	</c:if>
	<c:if test="${partita.esitoPartita == 'G'}"><span style="color:#CC0000">PARTITA INIZIATA</span> - 
		[<a href="<c:url value="/scores/${partita.idPartita}/scoreboard/"/>">CONTINUA LO SCORE DELLA PARTITA</a>]
	</c:if>
	<c:if test="${partita.esitoPartita == 'T'}">PARTITA TERMINATA - SCORE ACQUISITO</c:if>
</h4>

<table class="table table-striped">
	<tr>
		<th colspan="2" class="info">Dati della partita</th>
	</tr>
	<tr>
		<td width="40%">Progressivo partita</td>
		<td width="60%">${partita.idPartita}</td>
	</tr>
	<tr>
		<td>Data partita</td>
		<td>${partita.data}</td>
	</tr>
	<tr>
		<td>Campionato e fase</td>
		<td>${partita.competizione}</td>
	</tr>
	<tr>
		<td>Esito partita</td>
		<td>${partita.esitoPartita}</td>
	</tr>
	<tr>
		<td>Piscina</td>
		<td><a href="<c:url value="/partite/piscina/${partita.piscina.idPiscina}/elencaPartite"/>">${partita.piscina.nomePiscina}</a>, località: ${partita.piscina.luogoPiscina}</td>
	</tr>
	<tr>
		<td>Arbitro 1</td>
		<td><a href="<c:url value="/partite/arbitro/${partita.arbitro1.idArbitro}/elencaPartite/"/>">${partita.arbitro1.nomeCompleto}</a> di ${partita.arbitro1.provenienza}</td>
	</tr>
	<tr>
		<td>Arbitro 2</td>
		<td><a href="<c:url value="/partite/arbitro/${partita.arbitro2.idArbitro}/elencaPartite/"/>">${partita.arbitro2.nomeCompleto}</a> di ${partita.arbitro2.provenienza}</td>
	</tr>
	<tr>
		<td>Squadra Bianco</td>
		<td class="text-uppercase"><a href="<c:url value="/squadre/${partita.squadraBianco.idSquadra}/vediDettagli/" />">${partita.squadraBianco.denominazioneSquadra}</a></td>
	</tr>
	<tr>
		<td>Squadra Nero</td>
		<td class="text-uppercase"><a href="<c:url value="/squadre/${partita.squadraNero.idSquadra}/vediDettagli/" />">${partita.squadraNero.denominazioneSquadra}</a></td>
	</tr>
	<tr>
		<td>Tecnico Bianco</td>
		<td>${partita.tecnicoBianco.nomeCompleto}</td>
	</tr>
	<tr>
		<td>Tecnico Nero</td>
		<td>${partita.tecnicoNero.nomeCompleto}</td>
	</tr>
	<tr>
		<td>Reti Bianco</td>
		<td>${partita.retiBianco}</td>
	</tr>
	<tr>
		<td>Reti Nero</td>
		<td>${partita.retiNero}</td>
	</tr>
	<tr>
		<td>Periodo di gioco</td>
		<td>${partita.periodoGioco}</td>
	</tr>
	<tr>
		<td>Risultati parziali</td>
		<td>${partita.parzialiBianco[0]}-${partita.parzialiNero[0]},
			${partita.parzialiBianco[1]}-${partita.parzialiNero[1]},
			${partita.parzialiBianco[2]}-${partita.parzialiNero[2]},
			${partita.parzialiBianco[3]}-${partita.parzialiNero[3]}</td>
	</tr>
	<tr>
		<td>Superiorità Numeriche Bianco</td>
		<td>${partita.supNumBianco[0]}/${partita.supNumBianco[1]}</td>
	</tr>
	<tr>
		<td>Superiorità Numeriche Nero</td>
		<td>${partita.supNumNero[0]}/${partita.supNumNero[1]}</td>
	</tr>
</table>

<p>[<a href="<c:url value="/partite/${partita.idPartita}/vediReport/"/>">REPORT</a>]
- [<a href="<c:url value="/partite/${partita.idPartita}/vediBoxScore/"/>">BOXSCORE</a>]
- [<a href="<c:url value="/partite/${partita.idPartita}/vediPlayByPlay/"/>">PLAY-BY-PLAY</a>]</p>

	</div>
	<div class="col-3" align="right">
		<img src="<c:url value="/media/wp.jpg"></c:url>" alt="PARTITE"/>

		<ul class="nav">
			<li class="nav-item"><a href="#" class="nav-link-active"> <span class="glyphicon glyphicon-file"></span> Dettaglio Partita </a></li>
			<li class="nav-item"><a href="<c:url value="/partite/${partita.idPartita}/vediReport/"/>" class="nav-link"> <span class="glyphicon glyphicon-list-alt"></span> REPORT </a></li>
			<li class="nav-item"><a href="<c:url value="/partite/${partita.idPartita}/vediBoxScore/"/>" class="nav-link"> <span class="glyphicon glyphicon-th"></span> BOXSCORE </a></li>
			<li class="nav-item"><a href="<c:url value="/partite/${partita.idPartita}/vediPlayByPlay/"/>" class="nav-link"> <span class="glyphicon glyphicon-menu-hamburger"></span> PLAY-BY-PLAY </a></li> 
		</ul>
	</div>
</div>
