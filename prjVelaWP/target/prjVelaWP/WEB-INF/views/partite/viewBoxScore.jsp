<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<h5>Serie A1 di pallanuoto femminile (stagioni 2019-20 e 2020-21)</h5>

<h1>BOX SCORE Partita</h1>

<c:if test="${fn:length(messaggio) > 0}"><h4 style="color:red">${messaggio}</h4></c:if>

<div class="row">
	<div class="col-9">

		<table class="table table-striped">
			<tr>
				<td colspan="3"><b>Campionato:</b> ${partita.competizione}.
					<b>Luogo e data:</b> ${partita.piscina.nomePiscina}, ${partita.data}</td>
			</tr>
			<tr class="info">
				<th width="40%" class="text-center text-uppercase" style="font-size: 1.5em;">
					<a href="<c:url value="/squadre/${partita.squadraBianco.idSquadra}/vediDettagli/" />">${partita.squadraBianco.denominazioneSquadra}</a>
				</th>
				<th width="20%" class="text-center" style="font-size: 1.5em;">${partita.retiBianco} - ${partita.retiNero}</th>
				<th width="40%" class="text-center text-uppercase" style="font-size: 1.5em;">
					<a href="<c:url value="/squadre/${partita.squadraNero.idSquadra}/vediDettagli/" />">${partita.squadraNero.denominazioneSquadra}</a>
				</th>
			</tr>
			<tr>
				<td colspan="3" class="text-center">( parziali: ${partita.parzialiBianco[0]}-${partita.parzialiNero[0]},
					${partita.parzialiBianco[1]}-${partita.parzialiNero[1]},
					${partita.parzialiBianco[2]}-${partita.parzialiNero[2]},
					${partita.parzialiBianco[3]}-${partita.parzialiNero[3]} )</td>
			</tr>
			<tr>
				<td colspan="3" class="text-center">
					<b>Arbitri:</b>
					${partita.arbitro1.nomeCompleto} (${partita.arbitro1.provenienza}) e
					${partita.arbitro2.nomeCompleto} (${partita.arbitro2.provenienza}).
					<b>Superiorità Numeriche:</b>
					${partita.squadraBianco.nomeSquadra} ${partita.supNumBianco[0]}/${partita.supNumBianco[1]},
					${partita.squadraNero.nomeSquadra} ${partita.supNumNero[0]}/${partita.supNumNero[1]}.
				</td> 
			</tr>
		</table>

		<h4>
<c:if test="${partita.esitoPartita == 'C'}">
			<span style="color:#CC0000">PARTITA DA GIOCARE</span> - 
			[ <a href="<c:url value="/partite/gestione/${partita.idPartita}/scegliFormazione/"/>">
			<span class="glyphicon glyphicon-check"></span> SCEGLI LE FORMAZIONI</a> ]
</c:if>
<c:if test="${partita.esitoPartita == 'F'}">
			<span style="color:#CC0000">PARTITA DA GIOCARE</span> - 
			[<a href="<c:url value="/scores/${partita.idPartita}/scoreboard"/>">
			<span class="glyphicon glyphicon-edit"></span> ACQUISISCI LO SCORE DELLA PARTITA</a>]
</c:if>
<c:if test="${partita.esitoPartita == 'G'}">
			<span style="color:#CC0000">PARTITA INIZIATA</span> - 
			[<a href="<c:url value="/scores/${partita.idPartita}/scoreboard/"/>">
			<span class="glyphicon glyphicon-edit"></span> CONTINUA LO SCORE DELLA PARTITA</a>]
	</c:if>
</h4>

<c:if test="${partita.esitoPartita >= 'G'}">
		<table class="table table-striped">				
			<tr>
				<td>
					<table class="table table-condensed">
						<tr>
							<th colspan="2" class="info text-left">&nbsp;${partita.squadraBianco.denominazioneSquadra}</th>
							<th class="info text-center">Reti</th>
							<th class="info text-center">Tiri</th>
							<th class="info text-center">Falli</th>
							<th class="info text-center">Rig.</th>
							<th class="info text-center">T.SN</th>
							<th class="info text-center">T.Az</th>
							<th class="info text-center">T.CB</th>
							<th class="info text-center">T.6M</th>
							<th class="info text-center">T.CF</th>
							<th class="info text-center">EC</th>
							<th class="info text-center">EV</th>
							<th class="info text-center">ER</th>	
							<th class="info text-center">EO</th>
							<th class="info text-center">FR</th>
						</tr>
<c:forEach items="${tabelliniBianco}" var="tbb">
	<c:if test="${tbb.numeroCalottina == 0}">
						<tr class="active">
							<td colspan=2><i>Tecnico: ${partita.tecnicoBianco.nomeCompleto}</i></td>
							<th class="text-center">${tbb.reti}</th>
							<th class="text-center">${tbb.tiri}</th>
							<th class="text-center">${tbb.falli}</th>
							<th class="text-center">${tbb.statReti[0]}/${tbb.statTiri[0]}</th>
							<th class="text-center">${tbb.statReti[1]}/${tbb.statTiri[1]}</th>
							<th class="text-center">${tbb.statReti[2]}/${tbb.statTiri[2]}</th>
							<th class="text-center">${tbb.statReti[3]}/${tbb.statTiri[3]}</th>
							<th class="text-center">${tbb.statReti[4]}/${tbb.statTiri[4]}</th>
							<th class="text-center">${tbb.statReti[5]}/${tbb.statTiri[5]}</th>
							<th class="text-center">${tbb.statFalli[0]}</th>
							<th class="text-center">${tbb.statFalli[1]}</th>
							<th class="text-center">${tbb.statFalli[2]}</th>	
							<th class="text-center">${tbb.statFalli[3]}</th>	
							<th class="text-center">${tbb.statFalli[4]}</th>
	</c:if>
	<c:if test="${tbb.numeroCalottina > 0}">
						<tr>
							<td class="text-right">${tbb.numeroCalottina}</td>
							<td class="text-left">${tbb.atleta.nomeCompleto}</td>
							<td class="text-center">${tbb.reti}</td>
							<td class="text-center">${tbb.tiri}</td>
							<td class="text-center">${tbb.falli}</td>
							<td class="text-center">${tbb.statReti[0]}/${tbb.statTiri[0]}</td>
							<td class="text-center">${tbb.statReti[1]}/${tbb.statTiri[1]}</td>
							<td class="text-center">${tbb.statReti[2]}/${tbb.statTiri[2]}</td>
							<td class="text-center">${tbb.statReti[3]}/${tbb.statTiri[3]}</td>
							<td class="text-center">${tbb.statReti[4]}/${tbb.statTiri[4]}</td>
							<td class="text-center">${tbb.statReti[5]}/${tbb.statTiri[5]}</td>
							<td class="text-center">${tbb.statFalli[0]}</td>	
							<td class="text-center">${tbb.statFalli[1]}</td>
							<td class="text-center">${tbb.statFalli[2]}</td>	
							<td class="text-center">${tbb.statFalli[3]}</td>	
							<td class="text-center">${tbb.statFalli[4]}</td>
	</c:if>
						</tr>
</c:forEach>					
						<tr>
							<th colspan="2" class="info text-left">&nbsp;${partita.squadraNero.denominazioneSquadra}</th>
							<th class="info text-center">Reti</th>
							<th class="info text-center">Tiri</th>
							<th class="info text-center">Falli</th>
							<th class="info text-center">Rig.</th>
							<th class="info text-center">T.SN</th>
							<th class="info text-center">T.Az</th>
							<th class="info text-center">T.CB</th>
							<th class="info text-center">T.6M</th>
							<th class="info text-center">T.CF</th>
							<th class="info text-center">EC</th>
							<th class="info text-center">EV</th>
							<th class="info text-center">ER</th>	
							<th class="info text-center">EO</th>
							<th class="info text-center">FR</th>
						</tr>		
						
<c:forEach items="${tabelliniNero}" var="tbn">
	<c:if test="${tbn.numeroCalottina == 0}">
						<tr class="active">
							<td colspan=2><i>Tecnico: ${partita.tecnicoNero.nomeCompleto}</i></td>
							<th class="text-center">${tbn.reti}</th>
							<th class="text-center">${tbn.tiri}</th>
							<th class="text-center">${tbn.falli}</th>
							<th class="text-center">${tbn.statReti[0]}/${tbn.statTiri[0]}</th>
							<th class="text-center">${tbn.statReti[1]}/${tbn.statTiri[1]}</th>
							<th class="text-center">${tbn.statReti[2]}/${tbn.statTiri[2]}</th>
							<th class="text-center">${tbn.statReti[3]}/${tbn.statTiri[3]}</th>
							<th class="text-center">${tbn.statReti[4]}/${tbn.statTiri[4]}</th>
							<th class="text-center">${tbn.statReti[5]}/${tbn.statTiri[5]}</th>
							<th class="text-center">${tbn.statFalli[0]}</th>	
							<th class="text-center">${tbn.statFalli[1]}</th>
							<th class="text-center">${tbn.statFalli[2]}</th>
							<th class="text-center">${tbn.statFalli[3]}</th>
							<th class="text-center">${tbn.statFalli[4]}</th>
	</c:if>
	<c:if test="${tbn.numeroCalottina > 0}">
						<tr>
							<td class="text-right">${tbn.numeroCalottina}</td>
							<td class="text-left">${tbn.atleta.nomeCompleto}</td>
							<td class="text-center">${tbn.reti}</td>
							<td class="text-center">${tbn.tiri}</td>
							<td class="text-center">${tbn.falli}</td>
							<td class="text-center">${tbn.statReti[0]}/${tbn.statTiri[0]}</td>
							<td class="text-center">${tbn.statReti[1]}/${tbn.statTiri[1]}</td>
							<td class="text-center">${tbn.statReti[2]}/${tbn.statTiri[2]}</td>
							<td class="text-center">${tbn.statReti[3]}/${tbn.statTiri[3]}</td>
							<td class="text-center">${tbn.statReti[4]}/${tbn.statTiri[4]}</td>
							<td class="text-center">${tbn.statReti[5]}/${tbn.statTiri[5]}</td>
							<td class="text-center">${tbn.statFalli[0]}</td>	
							<td class="text-center">${tbn.statFalli[1]}</td>
							<td class="text-center">${tbn.statFalli[2]}</td>	
							<td class="text-center">${tbn.statFalli[3]}</td>	
							<td class="text-center">${tbn.statFalli[4]}</td>
	</c:if>
						</tr>
</c:forEach>
					</table>
					
					<table class="table table-condensed col-6">				
						<tr>
							<th colspan="2" class="info text-left">PORTIERI</th>
							<th class="info text-center">Parate</th>
							<th class="info text-center">P.Rig</th>
							<th class="info text-center">P.SN</th>
						</tr>
<c:forEach items="${tabelliniBianco}" var="tbb">
	<c:if test="${tbb.statPorta != null}">
		<c:if test="${tbb.numeroCalottina == 0}">
						<tr class="active">
							<th colspan=2>${partita.squadraBianco.denominazioneSquadra}</th>
							<th class="text-center">${tbb.statPorta[0]}/${tbb.statPorta[3]}</th>
							<th class="text-center">${tbb.statPorta[1]}/${tbb.statPorta[4]}</th>
							<th class="text-center">${tbb.statPorta[2]}/${tbb.statPorta[5]}</th>

		</c:if>
		<c:if test="${tbb.numeroCalottina > 0}">
						<tr>
							<td class="text-right">${tbb.numeroCalottina}</td>
							<td class="text-left">${tbb.atleta.nomeCompleto}</td>
							<td class="text-center">${tbb.statPorta[0]}/${tbb.statPorta[3]}</td>
							<td class="text-center">${tbb.statPorta[1]}/${tbb.statPorta[4]}</td>
							<td class="text-center">${tbb.statPorta[2]}/${tbb.statPorta[5]}</td>
		</c:if>	
						</tr>
	</c:if>
</c:forEach>
<c:forEach items="${tabelliniNero}" var="tbn">
	<c:if test="${tbn.statPorta != null}">
		<c:if test="${tbn.numeroCalottina == 0}">
						<tr class="active">
							<th colspan=2>${partita.squadraNero.denominazioneSquadra}</th>
							<th class="text-center">${tbn.statPorta[0]}/${tbn.statPorta[3]}</th>
							<th class="text-center">${tbn.statPorta[1]}/${tbn.statPorta[4]}</th>
							<th class="text-center">${tbn.statPorta[2]}/${tbn.statPorta[5]}</th>
		</c:if>
		<c:if test="${tbn.numeroCalottina > 0}">
						<tr>
							<td class="text-right">${tbn.numeroCalottina}</td>
							<td class="text-left">${tbn.atleta.nomeCompleto}</td>
							<td class="text-center">${tbn.statPorta[0]}/${tbn.statPorta[3]}</td>
							<td class="text-center">${tbn.statPorta[1]}/${tbn.statPorta[4]}</td>
							<td class="text-center">${tbn.statPorta[2]}/${tbn.statPorta[5]}</td>
		</c:if>
						</tr>
	</c:if>
</c:forEach>
					</table>
		
				</td>
			</tr>
		</table>

<p>[<a href="<c:url value="/partite/${partita.idPartita}/vediDettagli/"/>">Partita</a>]
- [<a href="<c:url value="/partite/${partita.idPartita}/vediBoxScore/"/>">BOXSCORE</a>]
- [<a href="<c:url value="/partite/${partita.idPartita}/vediPlayByPlay/"/>">PLAY-BY-PLAY</a>]
<c:if test="${isScorer}">- [<a href="<c:url value="/scores/${partita.idPartita}/matchscore/"/>">ACQUISISCI SCORE</a>]</c:if></p>

</c:if>

	</div>
	<div class="col-3" align="right">
		<img src="<c:url value="/media/wp.jpg"></c:url>" alt="PARTITE"/>

		<ul class="nav">
			<li class="nav-item"><a href="<c:url value="/partite/${partita.idPartita}/vediDettagli/"/>" class="nav-link"> <span class="glyphicon glyphicon-file"></span> Dettaglio Partita </a></li>
			<li class="nav-item"><a href="<c:url value="/partite/${partita.idPartita}/vediReport/"/>" class="nav-link"> <span class="glyphicon glyphicon-list-alt"></span> REPORT </a></li>
			<li class="nav-item"><a href="#" class="nav-link-active"> <span class="glyphicon glyphicon-th"></span> BOXSCORE </a></li>
			<li class="nav-item"><a href="<c:url value="/partite/${partita.idPartita}/vediPlayByPlay/"/>" class="nav-link"> <span class="glyphicon glyphicon-menu-hamburger"></span> PLAY-BY-PLAY </a></li> 
		</ul>
	</div>
</div>
