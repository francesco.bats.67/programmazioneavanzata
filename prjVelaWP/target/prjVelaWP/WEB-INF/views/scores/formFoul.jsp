<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
   
<h3>Inserisci Tiro ${colore} - ${squadra.nomeSquadra}</h3>

<c:url value="/scores/${partita.idPartita}/salvaFallo/${colore}" var="action_url" />

<form:form method="POST" action="${action_url}" modelAttribute="tabellino">

<table>
	<tr>
		<td>Periodo</td>
		<td>${partita.periodoGioco}° tempo</td>
	</tr>
	<tr>
		<td>Squadra in difesa</td>
		<td>${squadra.denominazioneSquadra}</td>
	</tr>
	<tr>		
		<td>Atleta che ha commesso il fallo</td>
		<td>
			<select name="idTabellino">
<c:forEach items="${tabellini}" var="tb">
				<option value="${tb.idTabellino}">${tb.atleta.nomeCompleto}</option> 
</c:forEach>
			</select>
		</td>
	</tr>
	<tr>		
		<td>Esito del Fallo</td>
		<td>
			<select name="esitoFallo">
				<option value="centro" selected>Espulsione 20 secondi al centro</option>
				<option value="perimetro">Espulsione 20 secondi dal perimetro</option>
				<option value="ripartenza">Espulsione 20 secondi in ripartenza</option>
				<option value="ostruzione">Espulsione 20 secondi per ostruzione</option>
				<option value="rigore">Fallo da rigore</option>
			</select>
		</td>
	</tr>
	<tr>		
		<td>Minuto di gioco</td>
		<td>
			<select name="minuto">
				<option value="7">7</option>
				<option value="6">6</option>
				<option value="5">5</option>
				<option value="4">4</option>
				<option value="3">3</option>
				<option value="2">2</option>
				<option value="1">1</option>
				<option value="0" selected>0</option>
			</select>
		</td>
	</tr>
	<tr>		
		<td>Secondo di gioco</td>
		<td>
			<select name="secondo">
				<option value="59">59</option>
				<option value="58">58</option>
				<option value="57">57</option>
				<option value="56">56</option>
				<option value="55">55</option>
				<option value="54">54</option>
				<option value="53">53</option>
				<option value="52">52</option>
				<option value="51">51</option>
				<option value="50">50</option>
				<option value="49">49</option>
				<option value="48">48</option>
				<option value="47">47</option>
				<option value="46">46</option>
				<option value="45">45</option>
				<option value="44">44</option>
				<option value="43">43</option>
				<option value="42">42</option>
				<option value="41">41</option>
				<option value="40">40</option>
				<option value="39">39</option>
				<option value="38">38</option>
				<option value="37">37</option>
				<option value="36">36</option>
				<option value="35">35</option>
				<option value="34">34</option>
				<option value="33">33</option>
				<option value="32">32</option>
				<option value="31">31</option>
				<option value="30">30</option>
				<option value="29">29</option>
				<option value="28">28</option>
				<option value="27">27</option>
				<option value="26">26</option>
				<option value="25">25</option>
				<option value="24">24</option>
				<option value="23">23</option>
				<option value="22">22</option>
				<option value="21">21</option>
				<option value="20">20</option>
				<option value="19">19</option>
				<option value="18">18</option>
				<option value="17">17</option>
				<option value="16">16</option>
				<option value="15">15</option>
				<option value="14">14</option>
				<option value="13">13</option>
				<option value="12">12</option>
				<option value="11">11</option>
				<option value="10">10</option>
				<option value="9">9</option>
				<option value="8">8</option>
				<option value="7">7</option>
				<option value="6">6</option>
				<option value="5">5</option>
				<option value="4">4</option>
				<option value="3">3</option>
				<option value="2">2</option>
				<option value="1">1</option>
				<option value="0" selected>0</option>
			</select>
		</td>
	</tr>
	<tr>
		<td colspan=2><input type="submit" value="SUBMIT"/></td>
	</tr>
</table>

</form:form>
