<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
   
<h3>Inserisci Sprint ${colore} - ${squadra.nomeSquadra}</h3>

<c:url value="/scores/${partita.idPartita}/salvaSprint/${colore}" var="action_url" />

<form:form method="POST" action="${action_url}" modelAttribute="tabellino">

<table>
	<tr>
		<td>Periodo</td>
		<td>${partita.periodoGioco}° tempo</td>
	</tr>
	<tr>
		<td>Squadra</td>
		<td>${squadra.denominazioneSquadra}</td>
	</tr>
	<tr>		
		<td>Opportunità</td>
		<td>
			<select name="supNum">
				<option value="false" selected>parità numerica</option>
				<option value="true">superiorità numerica</option>
			</select>
		</td>
	</tr>
	<tr>		
		<td>Atleta che ha vinto lo sprint</td>
		<td>
			<select name="idTabellino">
<c:forEach items="${tabellini}" var="tb">
				<option value="${tb.idTabellino}">${tb.atleta.nomeCompleto}</option> 
</c:forEach>
			</select>
		</td>
	</tr>
	<tr>
		<td colspan=2><input type="submit" value="SUBMIT"/></td>
	</tr>
</table>

</form:form>
