<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
   
<h3>Inserisci Persa ${colore} - ${squadra.nomeSquadra}</h3>

<c:url value="/scores/${partita.idPartita}/salvaPersa/${colore}" var="action_url" />

<form:form method="POST" action="${action_url}" modelAttribute="squadra">

<table>
	<tr>
		<td>Periodo</td>
		<td>${partita.periodoGioco}° tempo</td>
	</tr>
	<tr>
		<td>Squadra in attacco</td>
		<td>${squadra.denominazioneSquadra}</td>
	</tr>
	<tr>		
		<td>Opportunità</td>
		<td>
			<select name="supNum">
				<option value="false" selected>parità numerica</option>
				<option value="true">superiorità numerica</option>
			</select>
		</td>
	</tr>
	<tr>		
		<td>Esito del Tiro</td>
		<td>
			<select name="esitoPersa">
				<option value="recupero" selected>recupero della squadra avversaria</option>
				<option value="fuori">palla uscita fuori dalla vasca</option>
				<option value="fallo">controfallo</option>
				<option value="30sec">infrazione 30 secondi</option>
			</select>
		</td>
	</tr>
	<tr>
		<td><form:hidden path="idSquadra" /><td>
	<tr>
		<td colspan=2><input type="submit" value="SUBMIT"/></td>
	</tr>
</table>

</form:form>
