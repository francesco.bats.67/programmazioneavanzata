<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page session="false"%>

<sec:authorize access="isAuthenticated()" var="isAuth" />

<div class="row">
	<div class="col-6">
		<h2 style="font-weight:bold; color:darkblue"><span style="color:red; font-size:1.3em">V</span>&nbsp;E&nbsp;L&nbsp;A
			<span style="color:red; font-size:1.3em">W</span>&nbsp;A&nbsp;T&nbsp;E&nbsp;R
			<span style="color:red; font-size:1.3em">P</span>&nbsp;O&nbsp;L&nbsp;O</h2>
	</div>
	<div class="col-6" align="right">
		<h4 style="font-family:Verdana; color:darkblue"><i>pallanuoto femminile, Campionato di Serie A1</h4>
		<h5>partite e score della Vela Nuoto Ancona nelle stagioni 2019-20 e 2020-2021</i></h5>
	</div>
</div>

<div align="center">
	<img src="<c:url value="/media/cover_home.jpg"></c:url>" width="100%" alt="${nomeApp}"/>
</div>

<div class="row">
	<div class="col-4">
		<h3>CALENDARIO PARTITE</h3>
		<h4><a href="<c:url value="/partite/elenca/" />">ELENCO PARTITE DELLA VELA</a></h4>
<c:if test="${isAuth}">		<p><a href="<c:url value="/partite/aggiungi/" />">inserisci nuova partita</a></p></c:if>
	</div>
	<div class="col-4">
		<h3>SQUADRE A1</h3>
		<h4><a href="<c:url value="/squadre/elenca/" />">ELENCO DI TUTTE LE SQUADRE</a></h4>
<c:if test="${isAuth}">		<p><a href="<c:url value="/squadre/aggiungi/" />">inserisci nuova squadra</a></p></c:if> 
	</div>
	<div class="col-4">
		<h3>GIOCATRICI A1</h3>
		<h4><a href="<c:url value="/atleti/elenca/" />">ELENCO DI TUTTE LE GIOCATRICI</a></h4>
<c:if test="${isAuth}">		<p><a href="<c:url value="/atleti/gestione/aggiungi/" />">inserisci nuova giocatrice</a></p></c:if> 
	</div>
</div>

<hr />

<p>Data e ora: ${serverTime}.</p>
