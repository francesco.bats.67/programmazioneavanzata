<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<h5>Serie A1 di pallanuoto femminile (stagioni 2019-20 e 2020-21)</h5>

<h1>${squadra.denominazioneSquadra}, ${squadra.luogoSquadra}</h1>

<c:if test="${fn:length(messaggio) > 0}"><h4 style="color:red">${messaggio}</h4></c:if>

<div class="row">
	<div class="col-8">

<table class="table table-striped">
	<tr class="info">
		<th colspan="2">Dati della società sportiva</th>
	</tr>
	<tr>
		<td width="30%">Codice Squadra</td>
		<td width="70%">${squadra.idSquadra}</td>
	</tr>
	<tr>
		<td>Nome Squadra</td>
		<td class="text-uppercase">${squadra.nomeSquadra}</td>
	</tr>
	<tr>
		<td>Denominazione completa</td>
		<td class="text-uppercase">${squadra.denominazioneSquadra}</td>
	</tr>
	<tr>
		<td>Luogo</td>
		<td class="text-uppercase">${squadra.luogoSquadra}</td>
	</tr>
<c:if test="${fn:length(squadra.tecnici) > 0}">
	<tr class="info">
		<th colspan="2">Tecnici della squadra in A1 (2019-20 e 2020-21): ${fn:length(squadra.tecnici)}</th>
	</tr>
	<tr>
		<td colspan="2">
			<ul>
<c:forEach items="${squadra.tecnici}" var="t">
				<li><span class="text-uppercase">${t.nomeCompleto}</span>, ${t.sesso}, ${t.nazione}</li>
</c:forEach>
			</ul>
		</td>
	</tr>
</c:if>
	<tr class="info">
		<td colspan="2">
	</tr>
</table>

<h3>Giocatrici della squadra in A1 (2019-20 e 2020-21): ${fn:length(squadra.atleti)} > ${fn:length(atleti)}</h3>

<c:if test="${fn:length(squadra.atleti) > 0}">

<table class="table table-sm">
	<thead>
		<tr class="info">
			<th>Nome e Cognome</th>		
			<th class="text-center">Anno di nascita</th>
			<th class="text-center">Nazionalità</th>
			<th></th>
		</tr>
	</thead>
<c:forEach items="${atleti}" var="a">
	<tbody>
		<tr>
			<td class="text-uppercase"><a href="<c:url value="/atleti/${a.idAtleta}/vediDettagli/" />">${a.nomeCompleto}</a></td>
			<td class="text-center">${a.annoNascita}</td>
			<td class="text-center">${a.nazione}</td>
			<td class="text-center">[<a href="<c:url value="/squadre/gestione/${squadra.idSquadra}/atleta/${a.idAtleta}/dissocia/"/>"> <span class="glyphicon glyphicon-link"></span> dissocia </a>]</td>
		</tr>
</c:forEach>
		<tr class="info">
			<td colspan="4">
		</tr>
	</tbody>

</table>

</c:if>

<p>[ <a href="<c:url value="/squadre/gestione/${squadra.idSquadra}/atleta/scegli/"/>"> <span class="glyphicon glyphicon-link"></span> ASSOCIA ALLA SQUADRA UNA NUOVA GIOCATRICE</a> ]</p>

	</div>
	<div class="col-4" align="right">
	
		<img src="<c:url value="/media/wp.jpg"></c:url>" alt="SQUADRE"/>

		<h4>Partite in A1 <c:if test="${ squadra.idSquadra!='ANV' }"> contro la Vela</c:if></h4>

		<ul style="list-style-type: none;">
<c:forEach items="${partite}" var="p">
			<li><a href="<c:url value="/squadre/${p.squadraBianco.idSquadra}/vediDettagli/" />">${p.squadraBianco.nomeSquadra}</a> ${p.retiBianco}
			 - <a href="<c:url value="/squadre/${p.squadraNero.idSquadra}/vediDettagli/" />">${p.squadraNero.nomeSquadra}</a> ${p.retiNero}
			&nbsp;&nbsp;${p.data}&nbsp;&nbsp;
			[<a href="<c:url value="/partite/${p.idPartita}/vediDettagli/" />"> <span class="glyphicon glyphicon-file"></span> vedi </a>]</li>
</c:forEach>
		</ul>
	</div>
</div>
