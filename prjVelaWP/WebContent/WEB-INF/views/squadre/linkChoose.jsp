<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<h5>Serie A1 di pallanuoto femminile (stagioni 2019-20 e 2020-21)</h5>

<h3>Associa atleti alla squadra ${squadra.denominazioneSquadra}, ${squadra.luogoSquadra}</h3>

<c:if test="${fn:length(messaggio) > 0}"><h4 style="color:red">${messaggio}</h4></c:if>

<c:url value="/squadre/gestione/associa" var="action_url" />

<form:form method="POST" action="${action_url}" modelAttribute="squadra">

<table class="table table-striped">
	<thead>
		<tr class="info">
			<th>Squadra</th>
			<th>Giocatrici associate alla squadra</th>
			<th></th>
			<th>Tutte le giocatrici disponibili</th>
		</tr>
	</thead>
	</tbody>
		<tr>	
			<th valign="top"><h4><a href="<c:url value="/squadre/${squadra.idSquadra}/vediDettagli/" />" class="text-uppercase">${squadra.denominazioneSquadra}</a></h4></th>
			<th valign="top" class="text-uppercase">
				<ul style="list-style-type: none;">
<c:forEach items="${atletiSquadra}" var="as">
					<li><a href="<c:url value="/atleti/${as.idAtleta}/vediDettagli/" />">${as.nomeCompleto}</a></li>
</c:forEach>
				</ul>
			</th>
			<td valign="top">
				<span class="glyphicon glyphicon-chevron-left"></span>
				<input type="submit" value="ASSOCIA" class="btn btn-primary btn-md" />
			</td>
			<td valign="top">
				<select name="idAtleta" size=10>
<c:forEach items="${atletiDisponibili}" var="a">
					<option value="${a.idAtleta}">${a.nomeCompleto}</option> 
</c:forEach>
				</select>
			</td>
		</tr>
		<tr class="info">
			<td colspan="4">
		</tr>
	</tbody>
</table>

<form:hidden path="idSquadra" value="${squadra.idSquadra}"></form:hidden>

</form:form>

<i>Squadra: ${squadra.idSquadra}, Numero di giocatrici associate: ${fn:length(atletiDisponibili)}</i>
