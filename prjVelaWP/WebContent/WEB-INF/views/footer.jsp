<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page session="false"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authorize access="hasRole('ADMIN')" var="isAdmin" />
<sec:authorize access="hasRole('USER')" var="isUser" />
<sec:authorize access="isAuthenticated()" var="isAuth" />

<hr/>

<p class="text-center">VELA WATERPOLO - Progetto di Francesco Battistelli 2021 - 
<a href="<c:url value="/" />">Home</a>
 - <a href="<c:url value="/partite/elenca" />">Partite</a>
 - <a href="<c:url value="/squadre/elenca" />">Squadre</a>
 - <a href="<c:url value="/atleti/elenca" />">Giocatrici</a>
<c:if test="${isAuth}"> - <a href="<c:url value="/scores/elenca" />">Score</a></c:if> 
<c:if test="${isAdmin}"> - <a href="<c:url value="/squadre/elenca" />">Score</a></c:if>
<c:if test="${isAuth}">- <a href="<c:url value="/logout" />">Logout</a></c:if>
<c:if test="${! isAuth}">- <a href="<c:url value="/login" />">Login</a></c:if>
</p>

<c:if test="${isAuth}">AUTORIZZATO</c:if>
<c:if test="${isAdmin}">ADMIN</c:if>
<c:if test="${isUser}">USER</c:if>
<c:if test="${! isAuth}">NON AUTORIZZATO</c:if>
<c:if test="${! isAdmin}">NON ADMIN</c:if>
<c:if test="${! isUser}">NON USER</c:if>