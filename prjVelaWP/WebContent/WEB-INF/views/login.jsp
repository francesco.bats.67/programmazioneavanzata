<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<h2>VELA WATER POLO - Login Form</h2>

<c:if test="${not empty messaggioErrore}">
	<div style="color: red; font-weight: bold; margin: 30px 0px;">${messaggioErrore}</div>
</c:if>

<form name='login' action="<c:url value="/login" />" method='POST'>
	<table class="table col-6">
		<tr>
			<td>UserName:</td>
			<td><input type='text' name='username' value=''></td>
		</tr>
		<tr>
			<td>Password:</td>
			<td><input type='password' name='password' /></td>
		</tr>
		<tr>
			<td colspan='2'><input name="submit" type="submit" value="ACCEDI" class="btn btn-primary" /></td>
		</tr>
	</table>
	<%--
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
         --%>
</form>
