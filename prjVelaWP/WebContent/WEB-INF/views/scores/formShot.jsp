<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:url value="/scores/${partita.idPartita}/salvaTiro/${colore}" var="action_url" />
   
<h5>Serie A1 di pallanuoto femminile (stagioni 2019-20 e 2020-21)</h5>

<h1>SCOREBOARD Partita</h1>

<h3>Inserisci TIRO IN PORTA ${colore} ( ${squadra.nomeSquadra} )</h3>

<c:if test="${fn:length(messaggio) > 0}"><h4 style="color:red">${messaggio}</h4></c:if>

<form:form method="POST" action="${action_url}" modelAttribute="tabellino">

<div class="row">
	<div class="col-6">

		<table class="table table-striped">
			<tr>
				<td class="control-label">Periodo, minuto e secondo di gioco</td>
				<td>${partita.periodoGioco}° TEMPO, 
					<select name="minuto">
<% for ( int i=7; i>0; i-- ) { %>
						<option value="<%= i %>"><%= i %></option>
<% } %>
						<option value="0" selected>0</option>
					</select> ' :					
					<select name="secondo">
<% for ( int i=59; i>0; i-- ) { %>
						<option value="<%= i %>"><%= i %></option>
<% } %>
						<option value="0" selected>0</option>
					</select> "
				</td>
			</tr>
			<tr>
				<td>Squadra in attacco</td>
				<td class="text-uppercase">${squadra.denominazioneSquadra}</td>
			</tr>
			<tr>		
				<td>Opportunità</td>
				<td>
					<select name="supNum">
						<option value="false" selected>parità numerica</option>
						<option value="true">superiorità numerica</option>
					</select>
				</td>
			</tr>
			<tr>		
				<td>Atleta che ha tirato</td>
				<td>
					<select name="idTabellino">
		<c:forEach items="${tabellini}" var="tb">
						<option value="${tb.idTabellino}">${tb.atleta.nomeCompleto}</option> 
		</c:forEach>
					</select>
				</td>
			</tr>
			<tr>		
				<td>Tipo del Tiro</td>
				<td>
					<select name="tipoTiro">
						<option value="azione" selected>Tiro su azione</option>
						<option value="centro">Tiro da centro boa</option>
						<option value="libero">Tiro libero da 6 metri</option>
						<option value="fuga">Tiro in controfuga</option>
						<option value="supnum">Tiro in superiorità numerica</option>
						<option value="rigore">Tiro di rigore</option>
					</select>
				</td>
			</tr>
			<tr>		
				<td>Esito del Tiro</td>
				<td>
					<select name="esitoTiro">
						<option value="rete" selected>RETE SEGNATA</option>
						<option value="parato">parato dal portiere</option>
						<option value="paratoCorner">parato dal portiere > deviato in corner</option>
						<option value="paratoRimb">parato dal portiere > rimbalzo offensivo</option>
						<option value="legno">palo o traversa</option>
						<option value="legnoRimb">palo o traversa > rimbalzo offensivo</option>
						<option value="bloccato">bloccato da difensore</option>
						<option value="bloccatoRimb">bloccato da difensore > rimbalzo offensivo</option>
						<option value="fuori">fuori</option>
					</select>
				</td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" value="SUBMIT"/ class="btn btn-primary"></td>
			</tr>
		</table>

	</div>
	<div class="col-4" align="right">
		<img src="<c:url value="/media/tiro.jpg"></c:url>" alt="TIRO IN PORTA"/>
	</div>
</div>

</form:form>
