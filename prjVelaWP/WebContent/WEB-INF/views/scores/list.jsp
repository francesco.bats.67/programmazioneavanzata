<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<h5>Serie A1 di pallanuoto femminile (stagioni 2019-20 e 2020-21)</h5>

<h1>Elenco partite con score da iniziare o completare</h1>

<c:if test="${fn:length(messaggio) > 0}"><h4 style="color:red">${messaggio}</h4></c:if>

<div class="row">
	<div class="col-9">

<table class="table table-striped">
	<thead>
		<tr class="info">
			<th>Competizione</th>
			<th>Data</th>
			<th>Squadre e risultato parziale</th>
			<th></th>
		</tr>
	</thead>

<c:forEach items="${partiteScore}" var="p">
<c:if test="${p.esitoPartita == 'F'}">
	<tr style="background-color:#FFFF99">
		<td>${p.competizione}</td>
		<td>${p.data}</td>
		<td class="text-uppercase"><a href="<c:url value="/squadre/${p.squadraBianco.idSquadra}/vediDettagli/" />">${p.squadraBianco.nomeSquadra}</a> ${p.retiBianco},
			<a href="<c:url value="/squadre/${p.squadraNero.idSquadra}/vediDettagli/" />">${p.squadraNero.nomeSquadra}</a> ${p.retiNero}</td>
		<td class="text-center">[<a href="<c:url value="/scores/${p.idPartita}/scoreboard/" />"> <span class="glyphicon glyphicon-file"></span> vedi</a> ]</td>
	</tr>
</c:if>
<c:if test="${p.esitoPartita == 'G'}">
	<tr style="background-color:#FFCC99">
		<td>${p.competizione}</td>
		<td>${p.data}</td>
		<td class="text-uppercase"><a href="<c:url value="/squadre/${p.squadraBianco.idSquadra}/vediDettagli/" />">${p.squadraBianco.nomeSquadra}</a> ${p.retiBianco},
			<a href="<c:url value="/squadre/${p.squadraNero.idSquadra}/vediDettagli/" />">${p.squadraNero.nomeSquadra}</a> ${p.retiNero}</td>
		<td class="text-center">[<a href="<c:url value="/scores/${p.idPartita}/scoreboard/" />"> <span class="glyphicon glyphicon-file"></span> vedi</a> ]</td>
	</tr>
</c:if>

</c:forEach>
</table>

	</div>
	<div class="col-3" align="right">
		<img src="<c:url value="/media/wp.jpg"></c:url>" alt="PARTITE"/>
		<table>
			<tr>
				<td style="background-color:#FFFF99">&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td>&nbsp;&nbsp;partite da iniziare (formazioni inserite)</td>
			</tr>
			<tr>
				<td style="background-color:#FFCC99">&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td>&nbsp;&nbsp;partite iniziate da completare</td>
			</tr>
		</table>
	</div>
</div>


