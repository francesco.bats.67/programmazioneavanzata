<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:url value="/scores/${partita.idPartita}/salvaPortiere/${colore}" var="action_url" />
   
<h5>Serie A1 di pallanuoto femminile (stagioni 2019-20 e 2020-21)</h5>

<h1>SCOREBOARD Partita</h1>

<h3>Inserisci o modifica PORTIERE ${colore} ( ${squadra.nomeSquadra} )</h3>

<c:if test="${fn:length(messaggio) > 0}"><h4 style="color:red">${messaggio}</h4></c:if>

<form:form method="POST" action="${action_url}" modelAttribute="tabellino">

<div class="row">
	<div class="col-6">

		<table class="table table-striped">
			<tr>
				<td class="control-label">Periodo, minuto e secondo di gioco</td>
				<td>${partita.periodoGioco}° TEMPO, 
					<select name="minuto">
<% for ( int i=7; i>0; i-- ) { %>
						<option value="<%= i %>"><%= i %></option>
<% } %>
						<option value="0" selected>0</option>
					</select> ' :					
					<select name="secondo">
<% for ( int i=59; i>0; i-- ) { %>
						<option value="<%= i %>"><%= i %></option>
<% } %>
						<option value="0" selected>0</option>
					</select> "
				</td>
			</tr>
			<tr>
				<td>Squadra</td>
				<td class="text-uppercase">${squadra.denominazioneSquadra}</td>
			</tr>
			<tr>		
				<td>Giocatrice in porta</td>
				<td>
					<select name="idPortiere">
		<c:forEach items="${tabellini}" var="tb">
						<option value="${tb.idTabellino}">${tb.atleta.nomeCompleto}</option> 
		</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" value="SUBMIT"/ class="btn btn-primary"></td>
			</tr>
		</table>

	</div>
	<div class="col-4" align="right">
		<img src="<c:url value="/media/wp.jpg"></c:url>" alt="FALLO GRAVE"/>
	</div>
</div>

</form:form>
