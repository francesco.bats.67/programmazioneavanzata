<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
   
<h5>Serie A1 di pallanuoto femminile (stagioni 2019-20 e 2020-21)</h5>

<h1>SCOREBOARD Partita</h1>

<c:if test="${fn:length(messaggio) > 0}"><h4 style="color:red">${messaggio}</h4></c:if>

<div class="row">
	<div class="col-10">

		<table class="table table-striped">
			<tr>
				<td colspan="3" class="text-center"><b>Campionato:</b> ${partita.competizione}.
					<b>Luogo e data:</b> ${partita.piscina.nomePiscina}, ${partita.data}</td>
			</tr>
			<tr>
				<th class="text-center"><i>SQUADRA IN CALOTTINA BIANCA</i></th>
				<td>&nbsp;</td>
				<th class="text-center"><i>SQUADRA IN CALOTTINA NERA/BLU</i></th>
			</tr>
			<tr style="background-color:gold">
				<th width="40%" class="text-center text-uppercase" style="font-size: 1.5em;">
					<a href="<c:url value="/squadre/${partita.squadraBianco.idSquadra}/vediDettagli/" />">${partita.squadraBianco.denominazioneSquadra}</a>
				</th>
				<th width="20%" class="text-center" style="font-size: 1.5em;">
					${partita.retiBianco} - ${partita.retiNero}
				</th>
				<th width="40%" class="text-center text-uppercase" style="font-size: 1.5em;">
					<a href="<c:url value="/squadre/${partita.squadraNero.idSquadra}/vediDettagli/" />">${partita.squadraNero.denominazioneSquadra}</a>
				</th>
			</tr>
			<tr>
				<td class="text-center">Superiorità Numeriche: ${partita.supNumBianco[0]}/${partita.supNumBianco[1]}</td>
				<th class="text-center">${partita.parzialiBianco[0]}-${partita.parzialiNero[0]},
					${partita.parzialiBianco[1]}-${partita.parzialiNero[1]},
					${partita.parzialiBianco[2]}-${partita.parzialiNero[2]},
					${partita.parzialiBianco[3]}-${partita.parzialiNero[3]}</th>
				<td class="text-center">Superiorità Numeriche: ${partita.supNumNero[0]}/${partita.supNumNero[1]}</td>
			</tr>
			<tr>
				<td colspan="3" class="text-center">
					<b>Arbitri:</b>
					${partita.arbitro1.nomeCompleto} (${partita.arbitro1.provenienza}) e
					${partita.arbitro2.nomeCompleto} (${partita.arbitro2.provenienza}).
				</td> 
			</tr>
		</table>
		
		<table class="table table-striped">
			<tr>
				<td>
					<table class="table" border="1">
						<tr>
							<td>
								<table class="table table-condensed">
									<tr>
										<th colspan=2 class="text-uppercase" style="color:#006699">&nbsp;${partita.squadraBianco.denominazioneSquadra}</th>
											<th class="text-center" style="color:#006699">reti</th>
											<th class="text-center" style="color:#006699">falli</th>
										</th>
									</tr>		
<c:forEach items="${tabelliniBianco}" var="tbb">
	<c:if test="${tbb.numeroCalottina > 0}">
									<tr>
		<c:if test="${tbb.numeroCalottina == 1 or tbb.numeroCalottina == 13}">
										<td class="text-center" style="color:#FFFFFF; background-color:#CC0000">${tbb.numeroCalottina}</td>
		</c:if>
		<c:if test="${tbb.numeroCalottina > 1 and tbb.numeroCalottina < 13}">
										<td class="text-center" style="color:#006699">${tbb.numeroCalottina}</td>
		</c:if>
										<td>${tbb.atleta.nomeCompleto}</td>
										<td class="text-center">${tbb.reti}</td>
										<td class="text-center">${tbb.falli}</td>
									</tr>
	</c:if>
</c:forEach>
								</table>
								<p class="text-center"><b>Portiere:</b> ${partita.portiereBianco.atleta.nomeCompleto}<br>
								[<a href="<c:url value="/scores/${partita.idPartita}/portiere/bianco/${squadraBianco.idSquadra}"/>"> <span class="glyphicon glyphicon-refresh"></span> sostituisci portiere</a> ]</p>				
							</td>
							<td class="text-center">
								<h3>${partita.periodoGioco}° TEMPO</h3>
<c:if test="${partita.attacco == null}">
								<p><a href="<c:url value="/scores/${partita.idPartita}/sprint/bianco/${squadraBianco.idSquadra}"/>" class="btn btn-info">SPRINT&nbsp;bianco</a>&nbsp;&nbsp;&nbsp;
								<a href="<c:url value="/scores/${partita.idPartita}/sprint/nero/${squadraNero.idSquadra}"/>" class="btn btn-primary">&nbsp;SPRINT&nbsp;nero&nbsp;</a></p>
</c:if>
<c:if test="${partita.attacco == 'bianco'}">
								<h4><span class="glyphicon glyphicon-arrow-left"></span> <b>POSSESSO BIANCO</b></h4>
								<p>FALLO&nbsp;bianco - <a href="<c:url value="/scores/${partita.idPartita}/fallo/nero/${squadraNero.idSquadra}"/>" class="btn btn-primary">&nbsp;FALLO&nbsp;nero&nbsp;</a></p>
								<p><a href="<c:url value="/scores/${partita.idPartita}/tiro/bianco/${squadraBianco.idSquadra}"/>" class="btn btn-info">TIRO&nbsp;bianco</a> - &nbsp;TIRO&nbsp;nero&nbsp;</p>
								<p><a href="<c:url value="/scores/${partita.idPartita}/persa/bianco/${squadraBianco.idSquadra}"/>" class="btn btn-info">PERSA&nbsp;bianco</a> - &nbsp;PERSA&nbsp;nero&nbsp;</p>
								<p><a href="<c:url value="/scores/${partita.idPartita}/timeout/bianco/${squadraBianco.idSquadra}"/>" class="btn btn-info">TIMEOUT&nbsp;bianco</a> -&nbsp;TIMEOUT&nbsp;nero&nbsp;</p>
								<p><a href="<c:url value="/scores/${partita.idPartita}/fineSup/bianco/${squadraBianco.idSquadra}"/>" class="btn btn-info">FINE SUPERIORITA'&nbsp;bianco</a></p>
								<p><a href="<c:url value="/scores/${partita.idPartita}/fine/?periodoGioco=${partita.periodoGioco}"/>" class="btn btn-success">FINE TEMPO</a></p>
</c:if>
<c:if test="${partita.attacco == 'nero'}">
								<h4><b>POSSESSO NERO</b> <span class="glyphicon glyphicon-arrow-right"></span> </h4>
								<p><a href="<c:url value="/scores/${partita.idPartita}/fallo/bianco/${squadraBianco.idSquadra}"/>" class="btn btn-info">FALLO&nbsp;bianco</a> - &nbsp;FALLO&nbsp;nero&nbsp;</p>
								<p>TIRO&nbsp;bianco - <a href="<c:url value="/scores/${partita.idPartita}/tiro/nero/${squadraNero.idSquadra}"/>" class="btn btn-primary">&nbsp;TIRO&nbsp;nero&nbsp;</a></p>
								<p>PERSA&nbsp;bianco - <a href="<c:url value="/scores/${partita.idPartita}/persa/nero/${squadraNero.idSquadra}"/>" class="btn btn-primary">&nbsp;PERSA&nbsp;nero&nbsp;</a></p>
								<p>TIMEOUT&nbsp;bianco - <a href="<c:url value="/scores/${partita.idPartita}/timeout/nero/${squadraNero.idSquadra}"/>" class="btn btn-primary">&nbsp;TIMEOUT&nbsp;nero&nbsp;</a></p>
								<p><a href="<c:url value="/scores/${partita.idPartita}/fineSup/nero/${squadraNero.idSquadra}"/>" class="btn btn-primary">&nbsp;FINE SUPERIORITA'&nbsp;nero&nbsp;</a></p>
								<p><a href="<c:url value="/scores/${partita.idPartita}/fine/?periodoGioco=${partita.periodoGioco}"/>" class="btn btn-success">FINE TEMPO</a></p>
</c:if>
							</td>
							<td>
								<table class="table table-condensed">
									<tr>
										<th colspan=2 class="text-uppercase" style="color:#FFFFFF; background-color:#006699">&nbsp;${partita.squadraNero.denominazioneSquadra}</th>
											<th class="text-center" style="color:#FFFFFF; background-color:#006699">reti</th>
											<th class="text-center" style="color:#FFFFFF; background-color:#006699">falli</th>
										</th>
									</tr>			
<c:forEach items="${tabelliniNero}" var="tbn">
	<c:if test="${tbn.numeroCalottina > 0}">
									<tr>
		<c:if test="${tbn.numeroCalottina == 1 or tbn.numeroCalottina == 13}">
										<td class="text-center" style="color:#FFFFFF; background-color:#CC0000">${tbn.numeroCalottina}</td>
		</c:if>
		<c:if test="${tbn.numeroCalottina > 1 and tbn.numeroCalottina < 13}">
										<td class="text-center" style="color:#FFFFFF; background-color:#006699">${tbn.numeroCalottina}</td>
		</c:if>
										<td>${tbn.atleta.nomeCompleto}</td>
										<td class="text-center">${tbn.reti}</td>
										<td class="text-center">${tbn.falli}</td>
									</tr>
	</c:if>
</c:forEach>
								</table>
								<p class="text-center"><b>Portiere:</b> ${partita.portiereNero.atleta.nomeCompleto}<br>
								[<a href="<c:url value="/scores/${partita.idPartita}/portiere/nero/${squadraNero.idSquadra}"/>"> <span class="glyphicon glyphicon-refresh"></span> sostituisci portiere</a> ]</p>				
							</td>
						</tr>
					</table>

					<h3 class="text-center">Azioni della partita nel corrente periodo di gioco</h3>

					<table class="table table-condensed">
						<tr>
							<th class="info text-center">Periodo</th>
							<th class="info text-center">Tempo</th>
							<th class="info text-left">Azione</th>
							<th class="info text-center">Risultato</th>
							<th class="info text-left">Squadra</th>
							<th class="info text-left" colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;Giocatrice</th>
						</tr>
<c:forEach items="${azioni}" var="az">
	<c:if test="${az.tempoGioco[0] == partita.periodoGioco}">
						<tr>
							<td class="text-center">${az.tempoGioco[0]}</td>
							<td class="text-center">
		<c:if test="${az.tempoGioco[1] != '0' or az.tempoGioco[2] != '0'}">${az.tempoGioco[1]}:${az.tempoGioco[2]}</c:if>	
							</td>
							<td class="text-left">${az.tipoAzione}<c:if test="${fn:length(az.esitoAzione) > 0}">,</c:if>
							${az.esitoAzione}</td>
							<td class="text-center">${az.risultato}</td>
							<td class="text-left">${az.squadra.nomeSquadra}</td>
							<td class="text-right">
		<c:if test="${az.numeroCalottina != '0'}">${az.numeroCalottina}</c:if>				
							</td>
							<td class="text-left">${az.atleta.nomeCompleto}</td>
						</tr>
	</c:if>
</c:forEach>
						<tr>
							<td class="info" colspan="7"></td>
						</tr>
					</table>	
				</td> 
			</tr>
		</table>

	</div>
	<div class="col-2" align="right">
		<img src="<c:url value="/media/wp.jpg"></c:url>" alt="PARTITE"/>

		<ul class="nav">
			<li class="nav-item"><a href="<c:url value="/partite/${partita.idPartita}/vediDettagli/"/>" class="nav-link"> <span class="glyphicon glyphicon-file"></span> Dettaglio Partita </a></li>
			<li class="nav-item"><a href="<c:url value="/partite/${partita.idPartita}/vediReport/"/>" class="nav-link"> <span class="glyphicon glyphicon-list-alt"></span> REPORT </a></li>
			<li class="nav-item"><a href="<c:url value="/partite/${partita.idPartita}/vediBoxScore/"/>" class="nav-link"> <span class="glyphicon glyphicon-th"></span> BOXSCORE </a></li>
			<li class="nav-item"><a href="<c:url value="/partite/${partita.idPartita}/vediPlayByPlay/"/>" class="nav-link"> <span class="glyphicon glyphicon-menu-hamburger"></span> PLAY-BY-PLAY </a></li> 
		</ul>
	</div>
</div>
