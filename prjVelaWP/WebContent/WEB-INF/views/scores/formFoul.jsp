<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:url value="/scores/${partita.idPartita}/salvaFallo/${colore}" var="action_url" />
   
<h5>Serie A1 di pallanuoto femminile (stagioni 2019-20 e 2020-21)</h5>

<h1>SCOREBOARD Partita</h1>

<h3>Inserisci FALLO GRAVE ${colore} ( ${squadra.nomeSquadra} )</h3>

<c:if test="${fn:length(messaggio) > 0}"><h4 style="color:red">${messaggio}</h4></c:if>

<form:form method="POST" action="${action_url}" modelAttribute="tabellino">

<div class="row">
	<div class="col-8">

		<table class="table table-striped">
			<tr>
				<td class="control-label">Periodo, minuto e secondo di gioco</td>
				<td>${partita.periodoGioco}° TEMPO, 
					<select name="minuto">
<% for ( int i=7; i>0; i-- ) { %>
						<option value="<%= i %>"><%= i %></option>
<% } %>
						<option value="0" selected>0</option>
					</select> ' :					
					<select name="secondo">
<% for ( int i=59; i>0; i-- ) { %>
						<option value="<%= i %>"><%= i %></option>
<% } %>
						<option value="0" selected>0</option>
					</select> "
				</td>
			</tr>
			<tr>
				<td>Squadra in difesa</td>
				<td class="text-uppercase">${squadra.denominazioneSquadra}</td>
			</tr>
			<tr>		
				<td>Atleta che ha commesso il fallo</td>
				<td>
					<select name="idTabellino">
		<c:forEach items="${tabellini}" var="tb">
						<option value="${tb.idTabellino}">${tb.atleta.nomeCompleto}</option> 
		</c:forEach>
					</select>
				</td>
			</tr>
			<tr>		
				<td>Esito del Fallo</td>
				<td>
					<select name="esitoFallo">
						<option value="centro" selected>Espulsione 20 secondi al centro</option>
						<option value="perimetro">Espulsione 20 secondi dal perimetro</option>
						<option value="ripartenza">Espulsione 20 secondi in ripartenza</option>
						<option value="ostruzione">Espulsione 20 secondi per ostruzione</option>
						<option value="rigore">Fallo da rigore</option>
					</select>
				</td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" value="SUBMIT"/ class="btn btn-primary"></td>
			</tr>
		</table>

	</div>
	<div class="col-4" align="right">
		<img src="<c:url value="/media/fallo.jpg"></c:url>" alt="FALLO GRAVE"/>
	</div>
</div>

</form:form>
