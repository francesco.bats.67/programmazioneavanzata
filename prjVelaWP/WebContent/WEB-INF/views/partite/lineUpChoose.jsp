<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    
<c:url value="/partite/gestione/${partita.idPartita}/salvaFormazione" var="action_url" />

<h5>Serie A1 di pallanuoto femminile (stagioni 2019-20 e 2020-21)</h5>

<h1>Inserimento delle formazioni di una partita</h1>

<c:if test="${fn:length(messaggio) > 0}"><h4 style="color:red">${messaggio}</h4></c:if>

<table class="table table-striped">
	<tr>
		<td><b>Campionato:</b> ${partita.competizione}.
		<b>Luogo e data:</b> ${partita.piscina.nomePiscina}, ${partita.data}</td>
	</tr>
</table>

<img src="<c:url value="/media/formazioni.jpg"></c:url>" alt="FORMAZIONE"/>
		
<h3>Scegli le formazioni delle squadre</h3>

<form method="POST" action="${action_url}">

	<div class="row">
		<div class="col-6">

			<table class="table table-striped">
				<tr>		
					<th class="info text-left" colspan="2">SQUADRA BIANCA: ${squadraBianco.denominazioneSquadra}</th>
				</tr>
				<tr>		
					<td>Tecnico</td>
					<td>
						<select name="idTecnicoBianco">
<c:forEach items="${squadraBianco.tecnici}" var="tb">
							<option value="${tb.idTecnico}">${tb.nomeCompleto}</option> 
</c:forEach>
						</select>
					</td>
				</tr>
<% for (int i = 1; i < 14; i++) {  %>
				<tr>		
					<td>Calottina n. <%= i %>.</td>
					<td>
						<select name="idAtletaBianco<%= i %>">
<c:forEach items="${atletiSquadraBianco}" var="ab">
							<option value="${ab.idAtleta}">${ab.nomeCompleto}</option> 
</c:forEach>
						</select>
					</td>
				</tr>
<% } %>
			</table>
			
		</div>
		<div class="col-6">
	
			<table class="table table-striped">
				<tr>		
					<th class="info text-left" colspan="2">SQUADRA NERA: ${squadraNero.denominazioneSquadra}</th>
				</tr>
				<tr>		
					<td>Tecnico</td>
					<td>
						<select name="idTecnicoNero">
<c:forEach items="${squadraNero.tecnici}" var="tn">
							<option value="${tn.idTecnico}">${tn.nomeCompleto}</option> 
</c:forEach>
						</select>
					</td>
				</tr>
<% for (int i = 1; i < 14; i++) {  %>
				<tr>		
					<td>Calottina n. <%= i %>.</td>
					<td>
						<select name="idAtletaNero<%= i %>">
<c:forEach items="${atletiSquadraNero}" var="an">
							<option value="${an.idAtleta}">${an.nomeCompleto}</option> 
</c:forEach>
						</select>
					</td>
				</tr>
<% } %>
			</table>
			
		</div>
	</div>

	<hidden id="idPartita" name="idPartita" value="${partita.idPartita}"/>

	<div class="text-center"><input type="submit" value="CONFERMA FORMAZIONI" class="btn btn-primary btn-md" /></div>

</form>
