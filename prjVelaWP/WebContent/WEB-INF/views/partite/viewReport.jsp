<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<h5>Serie A1 di pallanuoto femminile (stagioni 2019-20 e 2020-21)</h5>

<h1>REPORT Partita</h1>

<c:if test="${fn:length(messaggio) > 0}"><h4 style="color:red">${messaggio}</h4></c:if>

<div class="row">
	<div class="col-9">

		<table class="table table-striped">
			<tr>
				<td colspan="3"><b>Campionato:</b> ${partita.competizione}.
					<b>Luogo e data:</b> ${partita.piscina.nomePiscina} - ${partita.piscina.luogoPiscina}, ${dataPartita}</td>
			</tr>
			<tr class="info">
				<th width="40%" class="text-center text-uppercase" style="font-size: 1.5em;">
					<a href="<c:url value="/squadre/${partita.squadraBianco.idSquadra}/vediDettagli/" />">${partita.squadraBianco.denominazioneSquadra}</a>
				</th>
				<th width="20%" class="text-center" style="font-size: 1.5em;">${partita.retiBianco} - ${partita.retiNero}</th>
				<th width="40%" class="text-center text-uppercase" style="font-size: 1.5em;">
					<a href="<c:url value="/squadre/${partita.squadraNero.idSquadra}/vediDettagli/" />">${partita.squadraNero.denominazioneSquadra}</a>
				</th>
			</tr>
			<tr>
				<td colspan="3" class="text-center">( parziali: ${partita.parzialiBianco[0]}-${partita.parzialiNero[0]},
					${partita.parzialiBianco[1]}-${partita.parzialiNero[1]},
					${partita.parzialiBianco[2]}-${partita.parzialiNero[2]},
					${partita.parzialiBianco[3]}-${partita.parzialiNero[3]} )</td>
			</tr>
			<tr>
				<td colspan="3" class="text-center">
					<b>Arbitri:</b>
					${partita.arbitro1.nomeCompleto} (${partita.arbitro1.provenienza}) e
					${partita.arbitro2.nomeCompleto} (${partita.arbitro2.provenienza}).
					<b>Superiorità Numeriche:</b>
					${partita.squadraBianco.nomeSquadra} ${partita.supNumBianco[0]}/${partita.supNumBianco[1]},
					${partita.squadraNero.nomeSquadra} ${partita.supNumNero[0]}/${partita.supNumNero[1]}.
				</td> 
			</tr>
		</table>

		<h4>
<c:if test="${partita.esitoPartita == 'C'}">
			<span style="color:#CC0000">PARTITA DA GIOCARE</span> - 
			[ <a href="<c:url value="/partite/gestione/${partita.idPartita}/scegliFormazione/"/>">
			<span class="glyphicon glyphicon-check"></span> SCEGLI LE FORMAZIONI</a> ]
</c:if>
<c:if test="${partita.esitoPartita == 'F'}">
			<span style="color:#CC0000">PARTITA DA GIOCARE</span> - 
			[<a href="<c:url value="/scores/${partita.idPartita}/scoreboard"/>">
			<span class="glyphicon glyphicon-edit"></span> ACQUISISCI LO SCORE DELLA PARTITA</a> ]
</c:if>
<c:if test="${partita.esitoPartita == 'G'}">
			<span style="color:#CC0000">PARTITA INIZIATA</span> - 
			[<a href="<c:url value="/scores/${partita.idPartita}/scoreboard/"/>">
			<span class="glyphicon glyphicon-edit"></span> CONTINUA LO SCORE DELLA PARTITA</a> ]
	</c:if>
</h4>

<c:if test="${partita.esitoPartita >= 'G'}">
		<table class="table table-striped">				
			<tr>
				<td>
					<table class="table table-condensed">
						<tr>
							<th colspan="2" class="info text-left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								${partita.squadraBianco.denominazioneSquadra}</th>
							<th class="info text-center">reti</td>
							<th class="info text-center">falli</td>
						</tr>			
<c:forEach items="${tabelliniBianco}" var="tbb">
	<c:if test="${tbb.numeroCalottina > 0}">
						<tr>
							<td class="text-right">${tbb.numeroCalottina}</td>
							<td>${tbb.atleta.nomeCompleto}</td>
							<td class="text-center">${tbb.reti}</td>
							<td class="text-center">${tbb.falli}</td>
						</tr>
	</c:if>
</c:forEach>
						<tr>
							<td></td>
							<td colspan="3">tecnico: ${partita.tecnicoBianco.nomeCompleto}</td>
						</tr>							
					</table>
				</td>
				<td>
					<table class="table table-condensed">
						<tr>
							<th colspan="2" class="info text-left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								${partita.squadraNero.denominazioneSquadra}</th>
							<th class="info text-center">reti</td>
							<th class="info text-center">falli</td>
						</tr>
<c:forEach items="${tabelliniNero}" var="tbn">
	<c:if test="${tbn.numeroCalottina > 0}">
						<tr>
							<td class="text-right">${tbn.numeroCalottina}</td>
							<td>${tbn.atleta.nomeCompleto}</td>
							<td class="text-center">${tbn.reti}</td>
							<td class="text-center">${tbn.falli}</td>
						</tr>
	</c:if>
</c:forEach>
						<tr>
							<td></td>
							<td colspan="3">tecnico: ${partita.tecnicoNero.nomeCompleto}</td>
						</tr>
					</table>				
				</td>
			</tr>		
			<tr>
				<td colspan="2" class="text-center">
					<b>Superiorità Numeriche:</b>
					${partita.squadraBianco.nomeSquadra} ${partita.supNumBianco[0]}/${partita.supNumBianco[1]},
					${partita.squadraNero.nomeSquadra} ${partita.supNumNero[0]}/${partita.supNumNero[1]}<br>
					<b>Arbitri:</b>
					${partita.arbitro1.nomeCompleto} (${partita.arbitro1.provenienza}) e
					${partita.arbitro2.nomeCompleto} (${partita.arbitro2.provenienza})
			</tr>
		</table>

<p>[<a href="<c:url value="/partite/${partita.idPartita}/vediDettagli/"/>">Partita</a>]
- [<a href="<c:url value="/partite/${partita.idPartita}/vediBoxScore/"/>">BOXSCORE</a>]
- [<a href="<c:url value="/partite/${partita.idPartita}/vediPlayByPlay/"/>">PLAY-BY-PLAY</a>]
<c:if test="${isScorer}">- [<a href="<c:url value="/scores/${partita.idPartita}/matchscore/"/>">ACQUISISCI SCORE</a>]</c:if></p>

</c:if>

	</div>
	<div class="col-3" align="right">
		<img src="<c:url value="/media/wp.jpg"></c:url>" alt="PARTITE"/>

		<ul class="nav">
			<li class="nav-item"><a href="<c:url value="/partite/${partita.idPartita}/vediDettagli/"/>" class="nav-link"> <span class="glyphicon glyphicon-file"></span> Dettaglio Partita </a></li>
			<li class="nav-item"><a href="#" class="nav-link-active"> <span class="glyphicon glyphicon-list-alt"></span> REPORT </a></li>
			<li class="nav-item"><a href="<c:url value="/partite/${partita.idPartita}/vediBoxScore/"/>" class="nav-link"> <span class="glyphicon glyphicon-th"></span> BOXSCORE </a></li>
			<li class="nav-item"><a href="<c:url value="/partite/${partita.idPartita}/vediPlayByPlay/"/>" class="nav-link"> <span class="glyphicon glyphicon-menu-hamburger"></span> PLAY-BY-PLAY </a></li> 
		</ul>
	</div>
</div>
