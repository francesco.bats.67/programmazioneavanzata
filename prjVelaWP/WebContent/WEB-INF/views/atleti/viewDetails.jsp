<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<h5>Serie A1 di pallanuoto femminile (stagioni 2019-20 e 2020-21)</h5>

<h1>${atleta.nomeCompleto}</h1>

<c:if test="${fn:length(messaggio) > 0}"><h4 style="color:red">${messaggio}</h4></c:if>

<div class="row">
	<div class="col-8">

<table class="table table-striped">
	<tr>
		<th colspan="2" class="info">Dati della giocatrice</th>
	</tr>
	<tr>
		<td width="30%">Codice Atleta</td>
		<td width="70%">${atleta.idAtleta}</td>
	</tr>
	<tr>
		<td>Nome</td>
		<td class="text-uppercase">${atleta.nome}</td>
	</tr>
	<tr>
		<td>Cognome</td>
		<td class="text-uppercase">${atleta.cognome}</td>
	</tr>
	<tr>
		<td>Sesso</td>
		<td>${atleta.sesso}</td>
	</tr>
	<tr>
		<td>Anno Nascita</td>
		<td>${atleta.annoNascita}</td>
	</tr>
	<tr>
		<td>Nazione</td>
		<td>${atleta.nazione}</td>
	</tr>
	<tr>
		<th colspan="2" class="info">Squadre per cui ha giocato in Serie A1 (2019-2020 e 2020-21): ${fn:length(atleta.squadre)}</th>
	</tr>
	<tr>
		<td colspan="2">
			<ul>
<c:forEach items="${atleta.squadre}" var="s">
				<li><a href="<c:url value="/squadre/${s.idSquadra}/vediDettagli/" />" class="text-uppercase">${s.denominazioneSquadra}</a>, ${s.luogoSquadra}</li>
</c:forEach>
			</ul>
		</td>
	</tr>
	<tr class="info">
		<td colspan="2">
	</tr>
</table>

	</div>
	<div class="col-4" align="right">
		<img src="<c:url value="/media/wp.jpg"></c:url>" alt="GIOCATRICI"/>
	</div>
</div>
