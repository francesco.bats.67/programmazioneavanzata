package wp.security;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * [ SECURITY WEB APPLICATION INITIALIZER ]
 * Classe che modella l'initializer sicurezza dell'applicazione, Spring scopre
 *  l'istanza durante lo scan e si configura per usare la springSecurityFilterChain
 * @author Esempio visto a lezione - UNIVPM
 * @version dicembre 2020
 */
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {

}
