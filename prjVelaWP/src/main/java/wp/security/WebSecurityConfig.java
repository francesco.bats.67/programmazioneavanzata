package wp.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.security.web.access.AccessDeniedHandler;
//import org.springframework.security.web.authentication.AuthenticationFailureHandler;
//import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import wp.services.UserService;
import wp.services.UserDetailsServiceDefault;

/**
 * [ SECURITY WEB APPLICATION INITIALIZER ]
 * Classe che modella la configurazione di sicurezza dell'applicazione
 * @author Esempio visto a lezione - UNIVPM
 * @version dicembre 2020
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	// richiama l'unico password encoder in uso
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	// richiama la classe del framework spring
	@Bean
	public UserDetailsService userDetailsService() {
		return new UserDetailsServiceDefault();
	};
	
	// --- configurazione dell'autenticazione ----------------------------------
	
	@Autowired
	public void configure(AuthenticationManagerBuilder auth) throws Exception {

		// setta lo user details service desiderato associando il password encoder da cui dipende la classe
		auth.userDetailsService(userDetailsService()).passwordEncoder(this.passwordEncoder);

	}

	// --- configurazione dell'autorizzazione ----------------------------------
	// inserisce controlli di sicurezza sugli utenti
	// spqcifica quali ruoli hanno accesso a determinate URL
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {

		// chiamata in cascata dei metodi antMatchers e permitAll per costruire il registro di regole
		// le clausole formLogin() e Logout() configurano le URL di login e di logout
		// il controllo csrf riguarda la spedizione delle form
		http.authorizeRequests()
			.antMatchers("/login").permitAll()
			.antMatchers("/").permitAll()
			.antMatchers("/atleti/gestione/**").hasAnyRole("ADMIN")
			.antMatchers("/squadre/gestione/**").hasAnyRole("ADMIN")
			.antMatchers("/partite/gestione/**").hasAnyRole("USER")
			.antMatchers("/scores/**").hasAnyRole("USER")
			.and().formLogin().loginPage("/login").defaultSuccessUrl("/")
				.failureUrl("/login?error=true").permitAll()
			.and().logout().logoutSuccessUrl("/")
				.invalidateHttpSession(true).permitAll()
//			provato per intercettare 'HTTP Status 403 - Forbidden', non funziona...
//			.and().exceptionHandling().accessDeniedPage("/accessDenied.jsp")
			.and().csrf().disable();
	}

}

