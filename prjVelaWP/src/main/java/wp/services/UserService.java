package wp.services;

import wp.model.entities.User;

/**
 * [ USER SERVICE ]
 * Interfaccia del Service utente dell'applicazione
 * @author Esempio visto a lezione - UNIVPM
 * @version dicembre 2020
 */
public interface UserService {

	public User findById(String username);

	public User create(String username, String password);

	public User update(User user);

	public void delete(String username);

}
