package wp.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import wp.model.dao.AzioneDao;
import wp.model.dao.PartitaDao;
import wp.model.dao.SquadraDao;
import wp.model.dao.TabellinoDao;
import wp.model.entities.Atleta;
import wp.model.entities.Azione;
import wp.model.entities.Partita;
import wp.model.entities.Squadra;
import wp.model.entities.Tabellino;

/**
 * [ SCORE SERVICE DEFAULT ]
 * Classe che modella il Service di default che fornisce metodi relativi allo score
 * @author Francesco Battistelli - UNIVPM
 * @version 24 agosto 2021
 */
@Transactional
@Service("SrvScore")
public class ScoreServiceDefault implements ScoreService {

	// --- dichiarazione delle dipendenze nei confronti dei DAO ----------------

	private AzioneDao azioneRepository;
	private PartitaDao partitaRepository;
//	private SquadraDao squadraRepository;
	private TabellinoDao tabellinoRepository;

	// --- metodi transazionali ------------------------------------------------

//	@Transactional(readOnly=true)
//	@Override
//	public Partita trovaDaId(Long idPartita) {
//		return this.partitaRepository.findById(idPartita);
//	}
//
//	@Transactional
//	@Override
//	public Squadra leggiSquadraBianco(Partita partita) {
//		return this.partitaRepository.findSquadraBianco(partita);
//	}
//	
//	@Transactional
//	@Override
//	public Squadra leggiSquadraNero(Partita partita) {
//		return this.partitaRepository.findSquadraNero(partita);
//	}

	@Transactional
	@Override
	public List<Tabellino> elencaTabelliniGara(Partita partita, Squadra squadra) {
		return this.tabellinoRepository.findTabelliniGara(partita, squadra);
	}
	
	@Override
	public Tabellino trovaTabellinoDaId(Long idTabellino) {
		return this.tabellinoRepository.findById(idTabellino);
	}

	@Transactional
	@Override
	public List<Azione> elencaAzioniGara(Partita partita) {
		return this.azioneRepository.findAzioniGara(partita);
	}
	
	@Transactional
	@Override
	public List<Azione> elencaUltimeAzioniGara(Partita partita) {
		return this.azioneRepository.findUltimeAzioniGara(partita);
	}

	@Transactional
	@Override
	public void scegliPortiere(Partita partita, Tabellino tabellino, String colore, int periodo, int minuto, int secondo) {
		String tipoAzione = null;
		this.tabellinoRepository.setPortiere(tabellino);
		if ( partita.getEsitoPartita().equals("F") ) {
			tipoAzione = "Portiere iniziale";
			minuto = 8;
			secondo = 0;
		} else if ( partita.getEsitoPartita().equals("G") ) {
			tipoAzione = "Sostituzione portiere";
		}
			if ( colore.equals("bianco") ) {	
				this.partitaRepository.setPortiereBianco(partita, tabellino);
			}
			if ( colore.equals("nero") ) {
				this.partitaRepository.setPortiereNero(partita, tabellino);
			}
			this.partitaRepository.update(partita);
			this.azioneRepository.create(false, tipoAzione, null, null, tabellino.getNumeroCalottina(), partita, new int[] {periodo,minuto,secondo}, tabellino.getSquadra(), tabellino.getAtleta());
			this.tabellinoRepository.update(tabellino);
	}
	
	@Transactional
	@Override
	public void aggiornaSprint(Partita partita, Tabellino tabellino, String colore, boolean supNum, String risultato, int periodo) {
		this.partitaRepository.setInizioPartita(partita);
		this.partitaRepository.setColoreAttacco(partita, colore);
		this.partitaRepository.update(partita);
		this.azioneRepository.create(supNum, "Sprint vinto", null, risultato, tabellino.getNumeroCalottina(), partita, new int[] {periodo,8,0}, tabellino.getSquadra(), tabellino.getAtleta());
		this.tabellinoRepository.update(tabellino);
	}

	@Transactional
	@Override
	public void aggiornaFallo(Partita partita, Tabellino tabellinoDifensore, String colore, String esito, int periodo, int minuto, int secondo) {
		String azioneFallo = null;
		String codiceFallo = null;
		switch (esito) {
			case "centro":
				azioneFallo = "espulsione 20\" al centro";
				codiceFallo = "EC";
				break;
			case "perimetro":
				azioneFallo = "espulsione 20\" dal perimetro";
				codiceFallo = "EV";
				break;
			case "ripartenza":
				azioneFallo = "espulsione 20\" in ripartenza";
				codiceFallo = "ER";
				break;
			case "ostruzione":
				azioneFallo = "espulsione 20\" per ostruzione";
				codiceFallo = "EO";
				break;
			case "rigore":
				azioneFallo = "tiro di rigore";
				codiceFallo = "TR";
				break;
		}
		this.azioneRepository.create(false, "Fallo grave", azioneFallo, null, tabellinoDifensore.getNumeroCalottina(), partita, new int[] {periodo,minuto,secondo}, tabellinoDifensore.getSquadra(), tabellinoDifensore.getAtleta());
		this.tabellinoRepository.addFallo(tabellinoDifensore, codiceFallo);
		this.tabellinoRepository.update(tabellinoDifensore);
	}

	@Transactional
	@Override
	public void aggiornaPersa(Partita partita, Squadra squadra, String colore, boolean supNum, String esito, int periodo) {
		String azionePersa = null;
		if (supNum) {
			partitaRepository.addSupNumNoRete(partita, colore);
		}
		switch (esito) {
			case "recupero":
				azionePersa = "recupero avversario";
				break;
			case "fuori":
				azionePersa = "palla fuori";
				break;
			case "fallo":
				azionePersa = "controfallo";
				break;
			case "30sec":
				azionePersa = "infrazione 30 secondi";
				break;
		}
		this.partitaRepository.setColoreAttacco(partita, cambiaColore(colore));
		this.partitaRepository.update(partita);
		this.azioneRepository.create(supNum, "Palla persa", azionePersa, null, 0, partita, new int[] {periodo,0,0}, squadra, null);
	}

	@Transactional
	@Override
	public void aggiornaTimeOut(Partita partita, Squadra squadra, String colore, boolean supNum, int periodo, int minuto, int secondo) {
		this.azioneRepository.create(supNum, "Time Out", null, null, 0, partita, new int[] {periodo,minuto,secondo}, squadra, null);
	}
	
	@Transactional
	@Override
	public void aggiornaTiro(Partita partita, Tabellino tabellinoAttaccante, String colore, boolean supNum, String tipo, String esito, int periodo, int minuto, int secondo) {
		String nuovoColore = null;
		String azioneTipoTiro = null;
		String azioneEsitoTiro = null;
		String codiceTiro = null;
		char esitoTiro = '0';
		String risultato = null;
		if (supNum) { tipo = "supnum"; }
		
		switch (tipo) {
			case "azione":
				azioneTipoTiro = "Tiro su azione";
				codiceTiro = "AZ";
				break;
			case "centro":
				azioneTipoTiro = "Tiro da centro boa";
				codiceTiro = "CB";
				break;
			case "libero":
				azioneTipoTiro = "Tiro libero da 6 metri";
				codiceTiro = "6M";
				break;
			case "fuga":
				azioneTipoTiro = "Tiro in controfuga";
				codiceTiro = "CF";
				break;
			case "supnum":
				azioneTipoTiro = "Tiro in sup. numerica";
				supNum = true;
				codiceTiro = "SN";
				break;
			case "rigore":
				azioneTipoTiro = "Tiro di rigore";
				codiceTiro = "RG";
				break;
		}
		
		switch (esito) {
			case "rete":
				azioneEsitoTiro = "RETE";
				esitoTiro = 'R';
				partitaRepository.addReteSegnata(partita, colore);
				if (supNum) { partitaRepository.addSupNumRete(partita, colore); }
				risultato = partita.getRisultato();
				nuovoColore = cambiaColore(colore);
				break;
			case "parato":
				azioneEsitoTiro = "parato";
				esitoTiro = 'P';
				if (supNum) { partitaRepository.addSupNumNoRete(partita, colore); }
				nuovoColore = cambiaColore(colore);
				break;
			case "paratoCorner":
				azioneEsitoTiro = "parato - corner";
				esitoTiro = 'P';
				nuovoColore = colore;
				break;
			case "paratoRimb":
				azioneEsitoTiro = "parato - rimbalzo offensivo";
				esitoTiro = 'P';
				nuovoColore = colore;
				break;
			case "legno":
				azioneEsitoTiro = "palo";
				if (supNum) { partitaRepository.addSupNumNoRete(partita, colore); }
				nuovoColore = cambiaColore(colore);
				break;
			case "legnoRimb":
				azioneEsitoTiro = "palo - rimbalzo offensivo";
				nuovoColore = colore;
				break;
			case "bloccato":
				azioneEsitoTiro = "bloccato";
				if (supNum) { partitaRepository.addSupNumNoRete(partita, colore); }
				nuovoColore = cambiaColore(colore);
				break;
			case "bloccatoRimb":
				azioneEsitoTiro = "bloccato";
				nuovoColore = colore;
				break;
			case "fuori":
				azioneEsitoTiro = "fuori";
				if (supNum) { partitaRepository.addSupNumNoRete(partita, colore); }
				nuovoColore = cambiaColore(colore);
				break;
		}
		Tabellino tabellinoPortiere = null;
		if ( colore.equals("bianco") ) {
			tabellinoPortiere = this.partitaRepository.findPortiereNero(partita);
		} else if ( colore.equals("nero") ) {
			tabellinoPortiere = this.partitaRepository.findPortiereBianco(partita);
		}
		this.partitaRepository.setColoreAttacco(partita, nuovoColore);
		this.partitaRepository.update(partita);
		this.azioneRepository.create(supNum, azioneTipoTiro, azioneEsitoTiro, risultato, tabellinoAttaccante.getNumeroCalottina(), partita, new int[] {periodo,minuto,secondo}, tabellinoAttaccante.getSquadra(), tabellinoAttaccante.getAtleta());
		this.tabellinoRepository.addTiro(tabellinoAttaccante, tabellinoPortiere, codiceTiro, esitoTiro);
		this.tabellinoRepository.update(tabellinoAttaccante);
		this.tabellinoRepository.update(tabellinoPortiere);
	}
	
	@Transactional
	@Override
	public void finisceSupNum(Partita partita, Squadra squadra, String colore) {
		this.partitaRepository.addSupNumNoRete(partita, colore);
	}
	
	@Transactional
	@Override
	public void finiscePeriodo(Partita partita, int periodo) {
		if ( periodo < 3 ) {
			this.partitaRepository.setPeriodoGioco(partita);
		} else if ( periodo == 3 ) {
			this.partitaRepository.setFinePartita(partita);
		}
		this.partitaRepository.setColoreAttacco(partita, null);
		this.partitaRepository.update(partita);
	}
	
	private String cambiaColore(String colore) {
		String altroColore = "rosso";
		if (colore.equals("bianco")) {
			altroColore = "nero";
		}
		if (colore.equals("nero")) {
			altroColore = "bianco";
		}
		return altroColore;
	}
	

	// --- setter che iniettano le dipendenze nei confronti dei DAO ------------
	
	@Autowired
	public void setAzioneRepository(AzioneDao azioneRepository) {
		this.azioneRepository = azioneRepository;
	}
	
	@Autowired
	public void setPartitaRepository(PartitaDao partitaRepository) {
		this.partitaRepository = partitaRepository;
	}

	@Autowired
	public void setTabellinoRepository(TabellinoDao tabellinoRepository) {
		this.tabellinoRepository = tabellinoRepository;
	}

}
