package wp.services;

import java.util.List;

import wp.model.entities.Atleta;
import wp.model.entities.Tabellino;

/**
 * [ ATLETA SERVICE ]
 * Interfaccia del Service che fornisce metodi relativi alle giocatrici
 * @author Francesco Battistelli - UNIVPM
 * @version 24 agosto 2021
 */
public interface AtletaService {

	public Atleta trovaDaId(String idAtleta);
	public List<Atleta> elencaTutti();

	public Atleta crea(String idAtleta, String nome, String cognome, char sesso, String nazione, String codiceFin, int annoNascita);
	public Atleta aggiorna(Atleta atleta);
	public void elimina(Atleta atleta);
	public void elimina(String idAtleta);
	
	public List<Tabellino> elencaTabelliniAtleta(Atleta atleta);

}
