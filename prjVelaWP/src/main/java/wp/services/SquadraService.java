package wp.services;

import java.util.List;

import wp.model.entities.Squadra;
import wp.model.entities.Atleta;
import wp.model.entities.Partita;

/**
 * [ SQUADRA SERVICE ]
 * Interfaccia del Service che fornisce metodi relativi alle squadre
 * @author Francesco Battistelli - UNIVPM
 * @version 24 agosto 2021
 */
public interface SquadraService {

	public List<Squadra> elencaTutti();
	public Squadra trovaDaId(String idSquadra);
	
	public Squadra aggiorna(Squadra squadra);
	
	public List<Atleta> elencaAtleti(Squadra squadra);
	public List<Atleta> elencaAtletiDisponibili(Squadra squadra);
	void dissociaAtleta(String idSquadra, String idAtleta);
	
	public List<Partita> elencaPartite(Squadra squadra);

}
