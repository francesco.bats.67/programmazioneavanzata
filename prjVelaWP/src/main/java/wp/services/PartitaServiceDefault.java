package wp.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import wp.model.dao.ArbitroDao;
import wp.model.dao.AtletaDao;
import wp.model.dao.PartitaDao;
import wp.model.dao.PiscinaDao;
import wp.model.dao.SquadraDao;
import wp.model.dao.TecnicoDao;
import wp.model.dao.AzioneDao;
import wp.model.dao.TabellinoDao;
import wp.model.entities.Partita;
import wp.model.entities.Squadra;
import wp.model.entities.Arbitro;
import wp.model.entities.Atleta;
import wp.model.entities.Azione;
import wp.model.entities.Piscina;
import wp.model.entities.Tabellino;
import wp.model.entities.Tecnico;

/**
 * [ PARTITA SERVICE DEFAULT ]
 * Classe che modella il Service di default che fornisce metodi relativi alle partite
 * @author Francesco Battistelli - UNIVPM
 * @version 24 agosto 2021
 */
@Transactional
@Service("SrvPartita")
public class PartitaServiceDefault implements PartitaService {

	// --- dichiarazione delle dipendenze nei confronti dei DAO ----------------

	private ArbitroDao arbitroRepository;
	private AtletaDao atletaRepository;
	private AzioneDao azioneRepository;
	private PartitaDao partitaRepository;
	private PiscinaDao piscinaRepository;
	private SquadraDao squadraRepository;
	private TabellinoDao tabellinoRepository;
	private TecnicoDao tecnicoRepository;

	// --- metodi transazionali ------------------------------------------------

	@Transactional(readOnly=true)
	@Override
	public List<Partita> elencaTutti() {
		return this.partitaRepository.findAll();
	}
	
	@Transactional(readOnly=true)
	@Override
	public List<Partita> elencaTuttiOrdinaRecente() {
		return this.partitaRepository.findAllOrderDesc();
	}

	@Transactional(readOnly=true)
	@Override
	public Partita trovaDaId(Long idPartita) {
		return this.partitaRepository.findById(idPartita);
	}
	
	@Transactional
	@Override
	public Squadra leggiSquadraBianco(Partita partita) {
		return this.partitaRepository.findSquadraBianco(partita);
	}

	@Transactional
	@Override
	public Squadra leggiSquadraNero(Partita partita) {
		return this.partitaRepository.findSquadraNero(partita);
	}

	public String leggiDataEstesa(Partita partita) {
		return this.partitaRepository.getDataEstesa(partita);
	}	

	@Transactional
	@Override
	public Piscina trovaPiscinaDaId(String idPiscina) {
		return this.piscinaRepository.findById(idPiscina);
	}
	
	@Transactional
	@Override
	public Arbitro trovaArbitroDaId(String idArbitro) {
		return this.arbitroRepository.findById(idArbitro);
	}

	@Transactional
	@Override
	public List<Partita> elencaGarePiscina(Piscina piscina) {
		return this.piscinaRepository.findPartite(piscina);
	}

	@Transactional
	@Override
	public List<Partita> elencaGareArbitro(Arbitro arbitro) {
		return this.arbitroRepository.findPartite(arbitro);
	}
	
	@Transactional
	@Override	
	public void insericiTecnici(Partita partita, String idTecnicoBianco, String idTecnicoNero) {
		Tecnico tecnicoBianco = this.tecnicoRepository.findById(idTecnicoBianco);
		Tecnico tecnicoNero = this.tecnicoRepository.findById(idTecnicoNero);		
		this.partitaRepository.setTecnici(partita, tecnicoBianco, tecnicoNero);
	};

	@Transactional
	@Override	
	public void insericiFormazioni(Partita partita, Squadra squadraBianco, Squadra squadraNero, String[] idAB, String[] idAN) {
		Atleta[] AB = { null, null, null, null, null, null, null, null, null, null, null, null, null };
		Atleta[] AN = { null, null, null, null, null, null, null, null, null, null, null, null, null };
		Tabellino[] TB = { null, null, null, null, null, null, null, null, null, null, null, null, null };
		Tabellino[] TN = { null, null, null, null, null, null, null, null, null, null, null, null, null };			
		// estrazione entit� Atleti
		for ( int i = 0; i<13; i++ ) {
			AB[i] = this.atletaRepository.findById(idAB[i]);
			AN[i] = this.atletaRepository.findById(idAN[i]);
		}
		// creazione tabellini della squadra
		int[] s6 = {0,0,0,0,0,0};	
		int[] s5 = {0,0,0,0,0};
		for (int i = 0; i < 13; i++) {
			TB[i] = this.tabellinoRepository.create(i+1, false, 0, 0, 0, 0, false, s6, s6, s5, null, partita, squadraBianco, AB[i]);
			TN[i] = this.tabellinoRepository.create(i+1, false, 0, 0, 0, 0, false, s6, s6, s5, null, partita, squadraNero, AN[i]);
		}
		this.partitaRepository.setDisputaPartita(partita);
		this.partitaRepository.update(partita);
	};
	
	@Transactional
	@Override
	public List<Tabellino> elencaTabelliniGara(Partita partita, Squadra squadra) {
		return this.tabellinoRepository.findTabelliniGara(partita, squadra);
	};
	
	@Transactional
	@Override
	public List<Azione> elencaAzioniGara(Partita partita) {
		return this.azioneRepository.findAzioniGara(partita);
	};

	// --- setter che iniettano le dipendenze nei confronti dei DAO ------------

	@Autowired
	public void setArbitroRepository(ArbitroDao arbitroRepository) {
		this.arbitroRepository = arbitroRepository;
	}

	@Autowired
	public void setAtletaRepository(AtletaDao atletaRepository) {
		this.atletaRepository = atletaRepository;
	}

	@Autowired
	public void setAzioneRepository(AzioneDao azioneRepository) {
		this.azioneRepository = azioneRepository;
	}
	
	@Autowired
	public void setPartitaRepository(PartitaDao partitaRepository) {
		this.partitaRepository = partitaRepository;
	}

	@Autowired
	public void setPiscinaRepository(PiscinaDao piscinaRepository) {
		this.piscinaRepository = piscinaRepository;
	}
	
	@Autowired
	public void setSquadraRepository(SquadraDao squadraRepository) {
		this.squadraRepository = squadraRepository;
	}
	
	@Autowired
	public void setTabellinoRepository(TabellinoDao tabellinoRepository) {
		this.tabellinoRepository = tabellinoRepository;
	}

	@Autowired
	public void setTecnicoRepository(TecnicoDao tecnicoRepository) {
		this.tecnicoRepository = tecnicoRepository;
	}

}

