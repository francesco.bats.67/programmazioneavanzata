package wp.services;

import java.util.Date;
import java.util.List;

import wp.model.entities.Partita;
import wp.model.entities.Squadra;
import wp.model.entities.Arbitro;
import wp.model.entities.Azione;
import wp.model.entities.Piscina;
import wp.model.entities.Tabellino;

/**
 * [ PARTITA SERVICE ]
 * Interfaccia del Service che fornisce metodi relativi alle partite
 * @author Francesco Battistelli - UNIVPM
 * @version 24 agosto 2021
 */
public interface PartitaService {

	public List<Partita> elencaTutti();
	public List<Partita> elencaTuttiOrdinaRecente();
	public Partita trovaDaId(Long idPartita);
	
	public Squadra leggiSquadraBianco(Partita partita);
	public Squadra leggiSquadraNero(Partita partita);
	public String leggiDataEstesa(Partita partita);
	
	public List<Tabellino> elencaTabelliniGara(Partita partita, Squadra squadra);
	public List<Azione> elencaAzioniGara(Partita partita);
	
	public Arbitro trovaArbitroDaId(String idArbitro);
	public Piscina trovaPiscinaDaId(String idPiscina);
	
	public void insericiTecnici(Partita partita, String idTecnicoBianco, String idTecnicoNero);
	public void insericiFormazioni(Partita partita, Squadra squadraBianco, Squadra squadraNero, String[] idAB, String[] idAN);
	
	public List<Partita> elencaGarePiscina(Piscina piscina);
	public List<Partita> elencaGareArbitro(Arbitro arbitro);


}
