package wp.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import wp.model.dao.AtletaDao;
import wp.model.dao.TabellinoDao;
import wp.model.entities.Atleta;
import wp.model.entities.Tabellino;

/**
 * [ ATLETA SERVICE DEFAULT ]
 * Classe che modella il Service di default che fornisce metodi relativi alle giocatrici
 * @author Francesco Battistelli - UNIVPM
 * @version 24 agosto 2021
 */
@Transactional
@Service("SrvAtleta")
public class AtletaServiceDefault implements AtletaService {

	// --- dichiarazione delle dipendenze nei confronti dei DAO ----------------

	private AtletaDao atletaRepository;
	private TabellinoDao tabellinoRepository;

	// --- metodi transazionali ------------------------------------------------

	@Transactional(readOnly=true)
	@Override
	public Atleta trovaDaId(String idAtleta) {
		return this.atletaRepository.findById(idAtleta);
	}

	@Transactional(readOnly=true)
	@Override
	public List<Atleta> elencaTutti() {
		return this.atletaRepository.findAll();
	}

	@Transactional
	@Override
	public Atleta crea(String idAtleta, String nome, String cognome, char sesso, String nazione, String codiceFin,
			int annoNascita) {
		return this.atletaRepository.create(idAtleta, nome, cognome, sesso, nazione, codiceFin, annoNascita);
	}

	@Transactional
	@Override
	public Atleta aggiorna(Atleta atleta) {
		return this.atletaRepository.update(atleta);
	}

	@Transactional
	@Override
	public void elimina(Atleta atleta) {
		this.atletaRepository.delete(atleta);
	}

	@Transactional
	@Override
	public void elimina(String idAtleta) {
		Atleta atleta = this.atletaRepository.findById(idAtleta);
		this.atletaRepository.delete(atleta);
	}
	
	@Transactional
	@Override
	public List<Tabellino> elencaTabelliniAtleta(Atleta atleta) {
		return this.tabellinoRepository.findTabelliniAtleta(atleta);
	}

	// --- setter che iniettano le dipendenze nei confronti dei DAO ------------

	@Autowired
	public void setAtletaRepository(AtletaDao atletaRepository) {
		this.atletaRepository = atletaRepository;
	}

	@Autowired
	public void setTabellinoRepository(TabellinoDao tabellinoRepository) {
		this.tabellinoRepository = tabellinoRepository;
	}

}