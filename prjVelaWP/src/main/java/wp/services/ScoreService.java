package wp.services;

import java.util.List;

import wp.model.entities.Atleta;
import wp.model.entities.Azione;
import wp.model.entities.Partita;
import wp.model.entities.Squadra;
import wp.model.entities.Tabellino;

/**
 * [ SCORE SERVICE ]
 * Interfaccia del Service che fornisce metodi relativi allo score
 * @author Francesco Battistelli - UNIVPM
 * @version 24 agosto 2021
 */
public interface ScoreService {

//	public Partita trovaDaId(Long idPartita);
//	public Squadra leggiSquadraBianco(Partita partita);
//	public Squadra leggiSquadraNero(Partita partita);
	
	public Tabellino trovaTabellinoDaId(Long idTabellino);
	public List<Tabellino> elencaTabelliniGara(Partita partita, Squadra squadra);
	public List<Azione> elencaAzioniGara(Partita partita);
	public List<Azione> elencaUltimeAzioniGara(Partita partita);

	public void scegliPortiere(Partita partita, Tabellino tabellino, String colore, int periodo, int minuto, int secondo);

	public void aggiornaSprint(Partita partita, Tabellino tabellino, String colore, boolean supNum, String risultato, int periodo);
	public void aggiornaFallo(Partita partita, Tabellino tabellino, String colore, String tipo, int periodo, int minuto, int secondo);
	public void aggiornaPersa(Partita partita, Squadra squadra, String colore, boolean supNum, String esito, int periodo);
	public void aggiornaTimeOut(Partita partita, Squadra squadra, String colore, boolean supNum, int periodo, int minuto, int secondo);
	public void aggiornaTiro(Partita partita, Tabellino tabellinoAttaccante, String colore, boolean supNum, String tipo, String esito, int periodo, int minuto, int secondo);
	public void finisceSupNum(Partita partita, Squadra squadra, String colore);
	public void finiscePeriodo(Partita partita, int periodo);

}
