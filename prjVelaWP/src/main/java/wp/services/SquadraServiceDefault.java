package wp.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import wp.model.dao.AtletaDao;
import wp.model.dao.SquadraDao;
import wp.model.dao.TecnicoDao;
import wp.model.entities.Atleta;
import wp.model.entities.Squadra;
import wp.model.entities.Tecnico;
import wp.model.entities.Partita;

/**
 * [ SQUADRA SERVICE DEFAULT ]
 * Classe che modella il Service di default che fornisce metodi relativi alle squadre
 * @author Francesco Battistelli - UNIVPM
 * @version 24 agosto 2021
 */
@Transactional
@Service("SquadraSRV")
public class SquadraServiceDefault implements SquadraService {

	// --- dichiarazione delle dipendenze nei confronti dei DAO ----------------

	private AtletaDao atletaRepository;
	private SquadraDao squadraRepository;

	// --- metodi transazionali ------------------------------------------------
	
	@Transactional(readOnly=true)
	@Override
	public List<Squadra> elencaTutti() {
		return this.squadraRepository.findAll();
	}

	@Transactional(readOnly=true)
	@Override
	public Squadra trovaDaId(String idSquadra) {
		return this.squadraRepository.findById(idSquadra);
	}

	@Transactional
	@Override
	public Squadra aggiorna(Squadra squadra) {
		return this.squadraRepository.update(squadra);
	}

	@Transactional
	@Override
	public List<Atleta> elencaAtleti(Squadra squadra) {
		return this.squadraRepository.findAtleti(squadra);
	}

	@Transactional
	@Override	
	public List<Atleta> elencaAtletiDisponibili(Squadra squadra) {
		return this.squadraRepository.findAtletiExtra(squadra);
	}

	@Transactional
	@Override
	public void dissociaAtleta(String idSquadra, String idAtleta) {
		Squadra squadra = this.trovaDaId(idSquadra);
		Atleta atleta = this.atletaRepository.findById(idAtleta);
		squadra.getAtleti().remove(atleta);
	}
	
	@Transactional
	@Override
	public List<Partita> elencaPartite(Squadra squadra) {
		return this.squadraRepository.findPartite(squadra);
	}

	// --- setter che iniettano le dipendenze nei confronti dei DAO ------------
	
	@Autowired
	public void setAtletaRepository(AtletaDao atletaRepository) {
		this.atletaRepository = atletaRepository;
	}

	@Autowired
	public void setSquadraRepository(SquadraDao squadraRepository) {
		this.squadraRepository = squadraRepository;
	}

}
