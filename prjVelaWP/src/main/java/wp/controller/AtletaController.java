package wp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import wp.model.entities.Atleta;
import wp.services.AtletaService;

/**
 * [ ATLETA CONTROLLER ]
 * Classe che modella il Controller delle view relative alle giocatrici di pallanuoto
 * @author Francesco Battistelli - UNIVPM
 * @version 25 agosto 2021
 */
@RequestMapping("/atleti")
@Controller
public class AtletaController {

	// --- dichiarazione delle dipendenze nei confronti dei Service ------------

	private AtletaService atletaService;
	
	// --- metodo GET mappato sulla URL /atleti/elenca -------------------------
	
	@GetMapping(value="elenca")
	public String elenca(
			@RequestParam(value = "messaggio", required=false) String messaggio,
			Model modelloUI) {		
		List<Atleta> elencoAtleti = this.atletaService.elencaTutti();
		elencoAtleti = this.atletaService.elencaTutti();
		int numeroAtleti = elencoAtleti.size();
		// aggiunge attributi alla variabile MODELLO	
		modelloUI.addAttribute("atleti", elencoAtleti);
		modelloUI.addAttribute("numeroAtleti", numeroAtleti);
		modelloUI.addAttribute("messaggio", messaggio);
		return "atleti/list";
	}

	// --- metodo GET mappato sulla URL /atleti/vediDettagli -------------------	
	
	@GetMapping(value="/{idAtleta}/vediDettagli")
	public String vediDettagli(
			@PathVariable("idAtleta") String idAtleta, 
			@RequestParam(value = "messaggio", required=false) String messaggio,
			Model modelloUI) {
		Atleta atleta = this.atletaService.trovaDaId(idAtleta);
		modelloUI.addAttribute("atleta", atleta);
		modelloUI.addAttribute("messaggio", messaggio);
		return "atleti/viewDetails";
	}

	// --- metodi GET mappato sulle URL /atleti/gestione/*** > salva -----------

	@GetMapping(value="/gestione/aggiungi")
	public String aggiungi(
			@RequestParam(value = "messaggio", required=false) String messaggio,
			Model modelloUI) {
		modelloUI.addAttribute("atleta", new Atleta());
		modelloUI.addAttribute("messaggio", messaggio);
		return "atleti/form";
	}
	
	@GetMapping(value="/gestione/{idAtleta}/modifica")
	public String modifica(
			@PathVariable("idAtleta") String idAtleta,
			@RequestParam(value = "messaggio", required=false) String messaggio,
			Model modelloUI) {
		Atleta atleta = this.atletaService.trovaDaId(new String(idAtleta));
		modelloUI.addAttribute("atleta", atleta);
		modelloUI.addAttribute("messaggio", messaggio);
		return "atleti/form";
	}
	
	@GetMapping(value="/gestione/{idAtleta}/elimina")
	public String elimina(
			@PathVariable("idAtleta") String idAtleta) {
		Atleta atleta = this.atletaService.trovaDaId(new String(idAtleta));
		this.atletaService.elimina(atleta);
		return "redirect:/atleti/elenca/";
	}
	
	@PostMapping(value = "/gestione/salva")
	public String salva(
			@ModelAttribute("Atleta") Atleta atleta,
			BindingResult br) {
		try {
			this.bonificaId(atleta.getIdAtleta());
			this.atletaService.aggiorna(atleta);
			return "redirect:/atleti/elenca";
		} catch (Exception ex) {
			return "redirect:/atleti/elenca?messaggio = " + ex.getMessage();
		}
	}
	
	// --- metodi di supporto non mappati su URL -------------------------------
	
	// verifica che l'idAtleta con contenga il carattere '/'
	public void bonificaId(String idAtleta) {
		if (idAtleta.contains("/")) {
			throw new RuntimeException("idAtleta non formato correttamente");
		}
	}

	// --- setter che iniettano le dipendenze nei confronti dei Service --------

	@Autowired
	public void setAtletaService(AtletaService atletaService) {
		this.atletaService = atletaService;
	}

}