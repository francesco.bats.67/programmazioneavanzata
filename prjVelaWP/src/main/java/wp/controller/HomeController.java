package wp.controller;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * [ HOME CONTROLLER ]
 * Classe che modella il Controller della view home
 * @author Francesco Battistelli - UNIVPM
 * @version 25 agosto 2021
 */
@Controller
public class HomeController {

	@Autowired
	String nomeApp = "VELA WATER POLO";
	
	// metodo mappato sulla URL radice dell'applicazione
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model modello) {

		System.out.println("Richiesta Home Page, locale = " + locale);
	
		Date data = new Date();
		DateFormat formattaData = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		String dataFormattata = formattaData.format(data);
			
		// data corrente del server
		modello.addAttribute("serverTime", dataFormattata);
		
		// nome dell'applicazione
		modello.addAttribute("nomeApp", nomeApp);
		
		// restituisce il nome della vista in formato stringa (gestito dal RESOLVER)
		return "home";
	}
}

