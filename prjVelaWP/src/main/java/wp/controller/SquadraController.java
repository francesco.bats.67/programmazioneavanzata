package wp.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import wp.model.entities.Atleta;
import wp.model.entities.Partita;
import wp.model.entities.Squadra;
import wp.services.AtletaService;
import wp.services.SquadraService;

/**
 * [ SQUADRA CONTROLLER ]
 * Classe che modella il Controller delle view relative alle squadre di pallanuoto
 * @author Francesco Battistelli - UNIVPM
 * @version 25 agosto 2021
 */
@RequestMapping("/squadre")
@Controller
public class SquadraController {

	// --- dichiarazione delle dipendenze nei confronti dei Service ------------

	private AtletaService atletaService;
	private SquadraService squadraService;
	
	// --- metodo GET mappato sulla URL /squadre/elenca ------------------------

	@RequestMapping(value="/elenca", method = RequestMethod.GET)
	public String elenca(
			@RequestParam(value = "messaggio", required=false) String messaggio,
			Model modelloUI) {
		List<Squadra> elencoSquadre = new ArrayList<Squadra>();
		elencoSquadre = this.squadraService.elencaTutti();
		int numeroSquadre = elencoSquadre.size();
		modelloUI.addAttribute("squadre", elencoSquadre);
		modelloUI.addAttribute("numeroSquadre", numeroSquadre);
		modelloUI.addAttribute("messaggio", messaggio);		
		return "squadre/list";
	}

	// --- metodo GET mappato sulla URL /squadre/vediDettagli ------------------

	@GetMapping(value="/{idSquadra}/vediDettagli")
	public String vediDettagli(
			@PathVariable("idSquadra") String idSquadra,
			@RequestParam(value = "messaggio", required=false) String messaggio,
			Model modelloUI) {
		Squadra squadra = this.squadraService.trovaDaId(idSquadra);
		List<Atleta> elencoAtleti = new ArrayList<Atleta>();
		List<Partita> elencoPartite = new ArrayList<Partita>();
		elencoAtleti = this.squadraService.elencaAtleti(squadra);
		elencoPartite = this.squadraService.elencaPartite(squadra);
		modelloUI.addAttribute("squadra", squadra);
		modelloUI.addAttribute("atleti", elencoAtleti);
		modelloUI.addAttribute("partite", elencoPartite);
		modelloUI.addAttribute("messaggio", messaggio);		
		return "squadre/viewDetails";
	}

	// --- metodo GET mappato sulla URL /squadre/gestione/atleta/dissocia ---------------
	
	@GetMapping(value="/gestione/{idSquadra}/atleta/{idAtleta}/dissocia")
	public String dissociaAtleta(
			@PathVariable("idSquadra") String idSquadra,
			@PathVariable("idAtleta") String idAtleta) {
		try {
			this.squadraService.dissociaAtleta(new String(idSquadra), idAtleta);
			return "redirect:/squadre/{idSquadra}/vediDettagli";
		} catch (Exception ex) {
			return "redirect:/squadre/" + idSquadra + "/atleta/scegli?errore=" + ex.getMessage();
		}
	}

	// --- metodo GET mappato sulla URL /squadre/atleta/scegli > associa -------

	@GetMapping(value="/gestione/{idSquadra}/atleta/scegli")
	public String associaAtleta(
			@PathVariable("idSquadra") String idSquadra,
			@RequestParam(value = "messaggio", required=false) String messaggio,
			Model modelloUI) {
		Squadra squadra = this.squadraService.trovaDaId(idSquadra);
		List<Atleta> atletiDisponibili = this.squadraService.elencaAtletiDisponibili(squadra);
		List<Atleta> atletiSquadra = this.squadraService.elencaAtleti(squadra);	
		modelloUI.addAttribute("squadra", squadra);
		modelloUI.addAttribute("atletiSquadra", atletiSquadra);;
		modelloUI.addAttribute("atletiDisponibili", atletiDisponibili);
		modelloUI.addAttribute("messaggio", messaggio);		
		return "squadre/linkChoose";
	}

	@PostMapping(value = "/gestione/associa")
	public String associaAtleta(
			@RequestParam("idSquadra") String idSquadra,
			@RequestParam("idAtleta") String idAtleta) {
		Atleta atleta = this.atletaService.trovaDaId(new String(idAtleta));
		Squadra squadra = this.squadraService.trovaDaId(new String(idSquadra));	
		try {
			squadra.getAtleti().add(atleta);
			atleta.getSquadre().add(squadra);
			squadra = this.squadraService.aggiorna(squadra);
			atleta = this.atletaService.aggiorna(atleta);
			return "redirect:/squadre/gestione/" + idSquadra + "/atleta/scegli";
		} catch (Exception ex) {
			return "redirect:/squadre/" + idSquadra + "/atleta/scegli?errore=" + ex.getMessage();
		}			
	}

	// --- setter che iniettano le dipendenze nei confronti dei Service --------

	@Autowired
	public void setAtletaService(AtletaService atletaService) {
		this.atletaService = atletaService;
	}
	
	@Autowired
	public void setSquadraService(SquadraService squadraService) {
		this.squadraService = squadraService;
	}

}