package wp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * [ USER DETAILS SERVICE DEFAULT ]
 * Classe che modella il Controller che gestisce la sicurezza dell'applicazione
 * @author Esempio visto a lezione - UNIVPM
 * @version dicembre 2020
 */
@Controller
public class SecurityController {

	@Autowired
	String nomeApp = "VELA WATER POLO";
	
	// --- controller su invocazione della URL pagina di login -----------------
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String loginPage(
			@RequestParam(value = "error", required = false) String errore,
			@RequestParam(value = "logout", required = false) String logout,
			Model modelloUI) {

		// inizializza il messaggio di errore
		String messaggioErrore = null;
		if (errore != null) {
			messaggioErrore = "Nome utente o password errati!";
		}
        if(logout != null) {
        	messaggioErrore = "Logout effettuato!";
        }
		// aggiunge attributi al modello per la vista
		modelloUI.addAttribute("messaggioErrore", messaggioErrore);
		modelloUI.addAttribute("nomeApp", nomeApp);
		
		// ritorna alla pagina di login
		return "login";

	}

}
