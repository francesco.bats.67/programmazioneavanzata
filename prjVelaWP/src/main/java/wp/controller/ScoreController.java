package wp.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import wp.model.entities.Azione;
import wp.model.entities.Partita;
import wp.model.entities.Squadra;
import wp.model.entities.Tabellino;
import wp.services.AtletaService;
import wp.services.PartitaService;
import wp.services.ScoreService;
import wp.services.SquadraService;

/**
 * [ SCORE CONTROLLER ]
 * Classe che modella il Controller delle view relative agli score di pallanuoto
 * @author Francesco Battistelli - UNIVPM
 * @version 29 agosto 2021
 */
@RequestMapping("/scores")
@Controller
public class ScoreController {

	// --- dichiarazione delle dipendenze nei confronti dei Service ------------

	private PartitaService partitaService;	
	private ScoreService scoreService;
	private SquadraService squadraService;

	// --- metodo GET mappato sulla URL /scores/elenca ------------------------

	@GetMapping(value="/elenca")
	public String elenca(
			@RequestParam(value = "messaggio", required=false) String messaggio,
			Model modelloUI) {
		List<Partita> partiteScore = new ArrayList<Partita>();
		partiteScore = this.partitaService.elencaTuttiOrdinaRecente();
		modelloUI.addAttribute("partiteScore", partiteScore);
		modelloUI.addAttribute("messaggio", messaggio);		
		return "scores/list";
	}

	// --- metodo GET mappato sulla URL /scores/scoreboard ---------------------
	
	@GetMapping(value="/{idPartita}/scoreboard")
	public String scoreBoard(
			@PathVariable("idPartita") Long idPartita,
			@RequestParam(value = "messaggio", required=false) String messaggio,
			Model modelloUI) {
		Partita partita = this.partitaService.trovaDaId(idPartita);
		String dataPartita = this.partitaService.leggiDataEstesa(partita);
		Squadra squadraBianco = this.partitaService.leggiSquadraBianco(partita);
		Squadra squadraNero = this.partitaService.leggiSquadraNero(partita);
		List<Azione> azioni = this.scoreService.elencaUltimeAzioniGara(partita);
		List<Tabellino> tabelliniBianco = this.scoreService.elencaTabelliniGara(partita, squadraBianco);
		List<Tabellino> tabelliniNero = this.scoreService.elencaTabelliniGara(partita, squadraNero);			
		modelloUI.addAttribute("partita", partita);
		modelloUI.addAttribute("dataPartita", dataPartita);
		modelloUI.addAttribute("squadraBianco", squadraBianco);
		modelloUI.addAttribute("squadraNero", squadraNero);
		modelloUI.addAttribute("azioni", azioni);
		modelloUI.addAttribute("tabelliniBianco", tabelliniBianco);
		modelloUI.addAttribute("tabelliniNero", tabelliniNero);
		modelloUI.addAttribute("messaggio", messaggio);
		try {	
			return "scores/viewScoreBoard";
		} catch (Exception ex) {
			return "redirect:/partite/list?messaggio = " + ex.getMessage();
		}
	}
	
	// --- metodo GET mappato sulla URL /scores/portiere > salvaPortiere -------

	@GetMapping(value="/{idPartita}/portiere/{colore}/{idSquadra}")
	public String scegliPortiere(	
			@PathVariable("colore") String colore,
			@PathVariable("idPartita") Long idPartita,
			@PathVariable("idSquadra") String idSquadra,
			@RequestParam(value = "messaggio", required=false) String messaggio,
			Model modelloUI) {
		Partita partita = this.partitaService.trovaDaId(idPartita);
		Squadra squadra = this.squadraService.trovaDaId(idSquadra);
		List<Tabellino> tabellini = this.scoreService.elencaTabelliniGara(partita, squadra);
		modelloUI.addAttribute("partita", partita);
		modelloUI.addAttribute("colore", colore);
		modelloUI.addAttribute("squadra", squadra);
		modelloUI.addAttribute("tabellini", tabellini);
		modelloUI.addAttribute("messaggio", messaggio);
		return "scores/formGoalkeeper";
	}

	@PostMapping(value = "/{idPartita}/salvaPortiere/{colore}")
	public String salvaPortiere(
			@PathVariable("idPartita") Long idPartita,
			@PathVariable("colore") String colore,
			@RequestParam("idPortiere") Long idPortiere,
			@RequestParam("minuto") int minuto,
			@RequestParam("secondo") int secondo) {
		Tabellino tabellinoPortiere = this.scoreService.trovaTabellinoDaId(idPortiere);
		Partita partita = this.partitaService.trovaDaId(idPartita);
		int periodo = partita.getPeriodoGioco();
		try {
			this.scoreService.scegliPortiere(partita, tabellinoPortiere, colore, periodo, minuto, secondo);
			return "redirect:/scores/{idPartita}/scoreboard";
		} catch (Exception ex) {
			return "redirect:/scores/{idPartita}/scoreboard?messaggio = " + ex.getMessage();
		}
	}

	// --- metodo GET mappato sulla URL /scores/sprint > salvaSprint -----------

	@GetMapping(value="/{idPartita}/sprint/{colore}/{idSquadra}")
	public String inserisciSprint(	
			@PathVariable("colore") String colore,
			@PathVariable("idPartita") Long idPartita,
			@PathVariable("idSquadra") String idSquadra,
			@RequestParam(value = "messaggio", required=false) String messaggio,
			Model modelloUI) {
		Partita partita = this.partitaService.trovaDaId(idPartita);
		Squadra squadra = this.squadraService.trovaDaId(idSquadra);
		List<Tabellino> tabellini = this.scoreService.elencaTabelliniGara(partita, squadra);
		modelloUI.addAttribute("partita", partita);
		modelloUI.addAttribute("colore", colore);
		modelloUI.addAttribute("squadra", squadra);
		modelloUI.addAttribute("tabellini", tabellini);
		modelloUI.addAttribute("messaggio", messaggio);
		return "scores/formSprint";
	}

	@PostMapping(value = "/{idPartita}/salvaSprint/{colore}")
	public String salvaSprint(
			@PathVariable("idPartita") Long idPartita,
			@PathVariable("colore") String colore,
			@RequestParam("idTabellino") Long idTabellino,
			@RequestParam("supNum") boolean supNum) {
		Tabellino tabellino = this.scoreService.trovaTabellinoDaId(idTabellino);
		Partita partita = this.partitaService.trovaDaId(idPartita);
		int periodo = partita.getPeriodoGioco();
		String risultato = partita.getRisultato();
		try {
			this.scoreService.aggiornaSprint(partita, tabellino, colore, supNum, risultato, periodo);	
			return "redirect:/scores/{idPartita}/scoreboard";
		} catch (Exception ex) {
			return "redirect:/scores/{idPartita}/scoreboard?messaggio = " + ex.getMessage();
		}
	}
	
	// --- metodo GET mappato sulla URL /scores/fallo > salvaFallo -------------

	@GetMapping(value="/{idPartita}/fallo/{colore}/{idSquadra}")
	public String inserisciFallo(	
			@PathVariable("colore") String colore,
			@PathVariable("idPartita") Long idPartita,
			@PathVariable("idSquadra") String idSquadra,
			@RequestParam(value = "messaggio", required=false) String messaggio,
			Model modelloUI) {
		Partita partita = this.partitaService.trovaDaId(idPartita);
		Squadra squadra = this.squadraService.trovaDaId(idSquadra);
		List<Tabellino> tabellini = this.scoreService.elencaTabelliniGara(partita, squadra);
		modelloUI.addAttribute("partita", partita);
		modelloUI.addAttribute("colore", colore);
		modelloUI.addAttribute("squadra", squadra);
		modelloUI.addAttribute("tabellini", tabellini);
		modelloUI.addAttribute("messaggio", messaggio);
		return "scores/formFoul";
	}

	@PostMapping(value = "/{idPartita}/salvaFallo/{colore}")
	public String salvaFallo(
			@PathVariable("idPartita") Long idPartita,
			@PathVariable("colore") String colore,
			@RequestParam("idTabellino") Long idTabellino,
			@RequestParam("esitoFallo") String esitoFallo,
			@RequestParam("minuto") int minuto,
			@RequestParam("secondo") int secondo) {
		Tabellino tabellino = this.scoreService.trovaTabellinoDaId(idTabellino);
		Partita partita = this.partitaService.trovaDaId(idPartita);
		int periodo = partita.getPeriodoGioco();
		try {
			this.scoreService.aggiornaFallo(partita, tabellino, colore, esitoFallo, periodo, minuto, secondo);
			return "redirect:/scores/{idPartita}/scoreboard";
		} catch (Exception ex) {
			return "redirect:/scores/{idPartita}/scoreboard?messaggio = " + ex.getMessage();
		}
	}
	
	// --- metodo GET mappato sulla URL /scores/tiro > salvaTiro ---------------

	@GetMapping(value="/{idPartita}/tiro/{colore}/{idSquadra}")
	public String inserisciTiro(	
			@PathVariable("colore") String colore,
			@PathVariable("idPartita") Long idPartita,
			@PathVariable("idSquadra") String idSquadra,
			@RequestParam(value = "messaggio", required=false) String messaggio,
			Model modelloUI) {
		Partita partita = this.partitaService.trovaDaId(idPartita);
		Squadra squadra = this.squadraService.trovaDaId(idSquadra);
		List<Tabellino> tabellini = this.scoreService.elencaTabelliniGara(partita, squadra);
		modelloUI.addAttribute("partita", partita);
		modelloUI.addAttribute("colore", colore);
		modelloUI.addAttribute("squadra", squadra);
		modelloUI.addAttribute("tabellini", tabellini);
		modelloUI.addAttribute("messaggio", messaggio);
		return "scores/formShot";
	}

	@PostMapping(value = "/{idPartita}/salvaTiro/{colore}")
	public String salvaTiro(
			@PathVariable("idPartita") Long idPartita,
			@PathVariable("colore") String colore,
			@RequestParam("idTabellino") Long idTabellino,
			@RequestParam("tipoTiro") String tipoTiro,
			@RequestParam("esitoTiro") String esitoTiro,
			@RequestParam("minuto") int minuto,
			@RequestParam("secondo") int secondo,
			@RequestParam("supNum") boolean supNum) {
		Partita partita = this.partitaService.trovaDaId(idPartita);
		int periodo = partita.getPeriodoGioco();
		Tabellino tabellinoAttaccante = this.scoreService.trovaTabellinoDaId(idTabellino);
		try {
			this.scoreService.aggiornaTiro(partita, tabellinoAttaccante, colore, supNum, tipoTiro, esitoTiro, periodo, minuto, secondo);
			return "redirect:/scores/{idPartita}/scoreboard";
		} catch (Exception ex) {
			return "redirect:/scores/{idPartita}/scoreboard?messaggio = " + ex.getMessage();
		}
	}

	// --- metodo GET mappato sulla URL /scores/persa > salvaPersa -------------

	@GetMapping(value="/{idPartita}/persa/{colore}/{idSquadra}")
	public String inserisciPersa(	
			@PathVariable("colore") String colore,
			@PathVariable("idPartita") Long idPartita,
			@PathVariable("idSquadra") String idSquadra,
			@RequestParam(value = "messaggio", required=false) String messaggio,
			Model modelloUI) {
		Partita partita = this.partitaService.trovaDaId(idPartita);
		Squadra squadra = this.squadraService.trovaDaId(idSquadra);
		modelloUI.addAttribute("partita", partita);
		modelloUI.addAttribute("colore", colore);
		modelloUI.addAttribute("squadra", squadra);
		modelloUI.addAttribute("messaggio", messaggio);
		return "scores/formTurnover";
	}

	@PostMapping(value = "/{idPartita}/salvaPersa/{colore}")
	public String salvaPersa(
			@PathVariable("idPartita") Long idPartita,
			@PathVariable("colore") String colore,
			@RequestParam("idSquadra") String idSquadra,
			@RequestParam("esitoPersa") String esitoPersa,
			@RequestParam("supNum") boolean supNum) {
		Partita partita = this.partitaService.trovaDaId(idPartita);
		Squadra squadra = this.squadraService.trovaDaId(idSquadra);
		int periodo = partita.getPeriodoGioco();
		try {
			this.scoreService.aggiornaPersa(partita, squadra, colore, supNum, esitoPersa, periodo);
			return "redirect:/scores/{idPartita}/scoreboard";
		} catch (Exception ex) {
			return "redirect:/scores/{idPartita}/scoreboard?messaggio = " + ex.getMessage();
		}
	}

	// --- metodo GET mappato sulla URL /scores/timeout > salvaTimeout ---------

	@GetMapping(value="/{idPartita}/timeout/{colore}/{idSquadra}")
	public String inserisciTimeOut(	
			@PathVariable("colore") String colore,
			@PathVariable("idPartita") Long idPartita,
			@PathVariable("idSquadra") String idSquadra,
			@RequestParam(value = "messaggio", required=false) String messaggio,
			Model modelloUI) {
		Partita partita = this.partitaService.trovaDaId(idPartita);
		Squadra squadra = this.squadraService.trovaDaId(idSquadra);
		modelloUI.addAttribute("partita", partita);
		modelloUI.addAttribute("colore", colore);
		modelloUI.addAttribute("squadra", squadra);
		modelloUI.addAttribute("messaggio", messaggio);
		return "scores/formTimeout";
	}

	@PostMapping(value = "/{idPartita}/salvaTimeOut/{colore}")
	public String salvaTimeOut(
			@PathVariable("idPartita") Long idPartita,
			@PathVariable("colore") String colore,
			@RequestParam("idSquadra") String idSquadra,
			@RequestParam("minuto") int minuto,
			@RequestParam("secondo") int secondo,
			@RequestParam("supNum") boolean supNum) {
		Partita partita = this.partitaService.trovaDaId(idPartita);
		Squadra squadra = this.squadraService.trovaDaId(idSquadra);
		int periodo = partita.getPeriodoGioco();
		try {
			this.scoreService.aggiornaTimeOut(partita, squadra, colore, supNum, periodo, minuto, secondo);
			return "redirect:/scores/{idPartita}/scoreboard";
		} catch (Exception ex) {
			return "redirect:/scores/{idPartita}/scoreboard?messaggio = " + ex.getMessage();
		}
	}
	
	// --- metodo GET mappato sulla URL /scores/fineSup ------------------------

	@GetMapping(value="/{idPartita}/fineSup/{colore}/{idSquadra}")
	public String fineSup(	
			@PathVariable("colore") String colore,
			@PathVariable("idPartita") Long idPartita,
			@PathVariable("idSquadra") String idSquadra) {
		Partita partita = this.partitaService.trovaDaId(idPartita);
		Squadra squadra = this.squadraService.trovaDaId(idSquadra);
		try {
			this.scoreService.finisceSupNum(partita, squadra, colore);
			return "redirect:/scores/{idPartita}/scoreboard";
		} catch (Exception ex) {
			return "redirect:/scores/{idPartita}/scoreboard?messaggio = " + ex.getMessage();
		}
	}
	
	// --- metodo GET mappato sulla URL /scores/fine ---------------------------
	
	@GetMapping(value="/{idPartita}/fine")
	public String fineSup(	
			@PathVariable("idPartita") Long idPartita,
			@RequestParam("periodoGioco") int periodo) {
		Partita partita = this.partitaService.trovaDaId(idPartita);
		try {
			this.scoreService.finiscePeriodo(partita, periodo);		
			return "redirect:/partite/{idPartita}/vediDettagli";
		} catch (Exception ex) {
			return "redirect:/scores/{idPartita}/scoreboard?messaggio = " + ex.getMessage();
		}
	}	

	// --- setter che iniettano le dipendenze nei confronti dei Service --------

	@Autowired
	public void setScoreService(ScoreService scoreService) {
		this.scoreService = scoreService;
	}

	@Autowired
	public void setPartitaService(PartitaService partitaService) {
		this.partitaService = partitaService;
	}

	@Autowired
	public void setSquadraService(SquadraService squadraService) {
		this.squadraService = squadraService;
	}
	

}
