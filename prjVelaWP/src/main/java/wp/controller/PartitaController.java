package wp.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import wp.model.entities.Arbitro;
import wp.model.entities.Atleta;
import wp.model.entities.Azione;
import wp.model.entities.Partita;
import wp.model.entities.Piscina;
import wp.model.entities.Squadra;
import wp.model.entities.Tabellino;
import wp.services.PartitaService;
import wp.services.SquadraService;

/**
 * [ PARTITA CONTROLLER ]
 * Classe che modella il Controller delle view relative alle partite di pallanuoto
 * @author Francesco Battistelli - UNIVPM
 * @version 29 agosto 2021
 */
@RequestMapping("/partite")
@Controller
public class PartitaController {

	// --- dichiarazione delle dipendenze nei confronti dei Service ------------

	private PartitaService partitaService;
	private SquadraService squadraService;
	
	// --- metodo GET mappato sulla URL /partite/elenca ------------------------

	@GetMapping(value="/elenca")
	public String elenca(
			@RequestParam(value = "messaggio", required=false) String messaggio,
			Model modelloUI) {
		List<Partita> elencoPartite = new ArrayList<Partita>();
		elencoPartite = this.partitaService.elencaTuttiOrdinaRecente();
		int numeroPartite = elencoPartite.size();
		modelloUI.addAttribute("partite", elencoPartite);
		modelloUI.addAttribute("numeroPartite", numeroPartite);
		modelloUI.addAttribute("messaggio", messaggio);		
		return "partite/list";
	}

	// --- metodo GET mappato sulla URL /partite/vediDettagli ------------------

	@GetMapping(value="/{idPartita}/vediDettagli")
	public String vediDettagli(
			@PathVariable("idPartita") Long idPartita,
			@RequestParam(value = "messaggio", required=false) String messaggio,
			Model modelloUI) {	
		Partita partita = this.partitaService.trovaDaId(idPartita);
		String dataPartita = this.partitaService.leggiDataEstesa(partita);
		modelloUI.addAttribute("partita", partita);
		modelloUI.addAttribute("dataPartita", dataPartita);
		modelloUI.addAttribute("messaggio", messaggio);
		return "partite/viewDetails";
	}

	// --- metodo GET mappato sulla URL /partite/piscina/elencaPartite ---------

	@GetMapping(value="/piscina/{idPiscina}/elencaPartite")
	public String elencaPiscina(
			@PathVariable("idPiscina") String idPiscina,
			@RequestParam(value = "messaggio", required=false) String messaggio,
			Model modelloUI) {
		Piscina piscina = this.partitaService.trovaPiscinaDaId(idPiscina);
		List<Partita> elencoPartitePiscina = new ArrayList<Partita>();
		elencoPartitePiscina = this.partitaService.elencaGarePiscina(piscina);
		modelloUI.addAttribute("partite", elencoPartitePiscina);
		modelloUI.addAttribute("piscina", piscina);
		modelloUI.addAttribute("messaggio", messaggio);
		return "partite/listPool";
	}

	// --- metodo GET mappato sulla URL /partite/arbitro/elencaPartite ---------

	@GetMapping(value="/arbitro/{idArbitro}/elencaPartite")
	public String elencaArbitro(
			@PathVariable("idArbitro") String idArbitro,
			@RequestParam(value = "messaggio", required=false) String messaggio,
			Model modelloUI) {
		Arbitro arbitro = this.partitaService.trovaArbitroDaId(idArbitro);
		List<Partita> elencoPartiteArbitro = new ArrayList<Partita>();
		elencoPartiteArbitro = this.partitaService.elencaGareArbitro(arbitro);
		modelloUI.addAttribute("partite", elencoPartiteArbitro);
		modelloUI.addAttribute("arbitro", arbitro);
		modelloUI.addAttribute("messaggio", messaggio);
		return "partite/listReferee";
	}

	// --- metodo GET mappato sulla URL /partite/vediReport --------------------

	@GetMapping(value="/{idPartita}/vediReport")
	public String vediReport(
			@PathVariable("idPartita") Long idPartita,
			@RequestParam(value = "messaggio", required=false) String messaggio,
			Model modelloUI) {
		Partita partita = this.partitaService.trovaDaId(idPartita);
		String dataPartita = this.partitaService.leggiDataEstesa(partita);
		Squadra squadraBianco = this.partitaService.leggiSquadraBianco(partita);
		Squadra squadraNero = this.partitaService.leggiSquadraNero(partita);
		List<Tabellino> tabelliniBianco = this.partitaService.elencaTabelliniGara(partita, squadraBianco);
		List<Tabellino> tabelliniNero = this.partitaService.elencaTabelliniGara(partita, squadraNero);
		modelloUI.addAttribute("partita", partita);
		modelloUI.addAttribute("dataPartita", dataPartita);
		modelloUI.addAttribute("tabelliniBianco", tabelliniBianco);
		modelloUI.addAttribute("tabelliniNero", tabelliniNero);
		modelloUI.addAttribute("messaggio", messaggio);
		return "partite/viewReport";
	}

	// --- metodo GET mappato sulla URL /partite/vediBoxSccore -----------------
	
	@GetMapping(value="/{idPartita}/vediBoxScore")
	public String vediBoxScore(
			@PathVariable("idPartita") Long idPartita, 
			@RequestParam(value = "messaggio", required=false) String messaggio,
			Model modelloUI) {	
		Partita partita = this.partitaService.trovaDaId(idPartita);
		String dataPartita = this.partitaService.leggiDataEstesa(partita);
		Squadra squadraBianco = this.partitaService.leggiSquadraBianco(partita);
		Squadra squadraNero = this.partitaService.leggiSquadraNero(partita);
		List<Tabellino> tabelliniBianco = this.partitaService.elencaTabelliniGara(partita, squadraBianco);
		List<Tabellino> tabelliniNero = this.partitaService.elencaTabelliniGara(partita, squadraNero);
		modelloUI.addAttribute("partita", partita);
		modelloUI.addAttribute("dataPartita", dataPartita);
		modelloUI.addAttribute("tabelliniBianco", tabelliniBianco);
		modelloUI.addAttribute("tabelliniNero", tabelliniNero);
		modelloUI.addAttribute("messaggio", messaggio);
		return "partite/viewBoxScore";
	}
	
	// --- metodo GET mappato sulla URL /partite/vediPlayByPlay ----------------

	@GetMapping(value="/{idPartita}/vediPlayByPlay")
	public String vediPlayByPlay(
			@PathVariable("idPartita") Long idPartita,
			@RequestParam(value = "messaggio", required=false) String messaggio,
			Model modelloUI) {
		Partita partita = this.partitaService.trovaDaId(idPartita);
		String dataPartita = this.partitaService.leggiDataEstesa(partita);
		List<Azione> azioni = this.partitaService.elencaAzioniGara(partita);
		modelloUI.addAttribute("partita", partita);
		modelloUI.addAttribute("dataPartita", dataPartita);
		modelloUI.addAttribute("azioni", azioni);
		modelloUI.addAttribute("messaggio", messaggio);
		return "partite/viewPlayByPlay";
	}

	// --- metodo GET mappato sulla URL /partite/scegliformazione > salvaFormazione ---

	@GetMapping(value="/gestione/{idPartita}/scegliFormazione")
	public String formazioniGara(
			@PathVariable("idPartita") Long idPartita,
			@RequestParam(value="messaggio", required=false) String messaggio,
			Model modelloUI) {
		Partita partita = this.partitaService.trovaDaId(idPartita);
		Squadra squadraBianco = this.partitaService.leggiSquadraBianco(partita);
		Squadra squadraNero = this.partitaService.leggiSquadraNero(partita);
		List<Atleta> atletiSquadraBianco = squadraService.elencaAtleti(squadraBianco);
		List<Atleta> atletiSquadraNero = squadraService.elencaAtleti(squadraNero);
		modelloUI.addAttribute("partita", partita);
		modelloUI.addAttribute("squadraBianco", squadraBianco);
		modelloUI.addAttribute("squadraNero", squadraNero);
		modelloUI.addAttribute("atletiSquadraBianco", atletiSquadraBianco);
		modelloUI.addAttribute("atletiSquadraNero", atletiSquadraNero);
		modelloUI.addAttribute("messaggio", messaggio);
		return "partite/lineUpChoose";
	}

	@PostMapping(value = "/gestione/{idPartita}/salvaFormazione")
	public String salvaFormazione(
			@PathVariable("idPartita") Long idPartita,
			@RequestParam("idTecnicoBianco") String idTecnicoBianco,
			@RequestParam("idTecnicoNero") String idTecnicoNero,
			@RequestParam("idAtletaBianco1") String idAB1,
			@RequestParam("idAtletaBianco2") String idAB2,
			@RequestParam("idAtletaBianco3") String idAB3,
			@RequestParam("idAtletaBianco4") String idAB4,
			@RequestParam("idAtletaBianco5") String idAB5,
			@RequestParam("idAtletaBianco6") String idAB6,
			@RequestParam("idAtletaBianco7") String idAB7,
			@RequestParam("idAtletaBianco8") String idAB8,
			@RequestParam("idAtletaBianco9") String idAB9,
			@RequestParam("idAtletaBianco10") String idAB10,
			@RequestParam("idAtletaBianco11") String idAB11,
			@RequestParam("idAtletaBianco12") String idAB12,
			@RequestParam("idAtletaBianco13") String idAB13,
			@RequestParam("idAtletaNero1") String idAN1,
			@RequestParam("idAtletaNero2") String idAN2,
			@RequestParam("idAtletaNero3") String idAN3,
			@RequestParam("idAtletaNero4") String idAN4,
			@RequestParam("idAtletaNero5") String idAN5,
			@RequestParam("idAtletaNero6") String idAN6,
			@RequestParam("idAtletaNero7") String idAN7,
			@RequestParam("idAtletaNero8") String idAN8,
			@RequestParam("idAtletaNero9") String idAN9,
			@RequestParam("idAtletaNero10") String idAN10,
			@RequestParam("idAtletaNero11") String idAN11,
			@RequestParam("idAtletaNero12") String idAN12,
			@RequestParam("idAtletaNero13") String idAN13) {
		Partita partita = this.partitaService.trovaDaId(idPartita);
		Squadra squadraBianco = this.partitaService.leggiSquadraBianco(partita);
		Squadra squadraNero = this.partitaService.leggiSquadraNero(partita);
		String[] idAB = { idAB1, idAB2, idAB3, idAB4, idAB5, idAB6, idAB7, idAB8, idAB9, idAB10, idAB11, idAB12, idAB13 };
		String[] idAN = { idAN1, idAN2, idAN3, idAN4, idAN5, idAN6, idAN7, idAN8, idAN9, idAN10, idAN11, idAN12, idAN13 };
		boolean duplicato = trovaDuplicati(idAB) || trovaDuplicati(idAN);
		try {
			if ( !(duplicato) )  { 
				this.partitaService.insericiTecnici(partita, idTecnicoBianco, idTecnicoNero);
				this.partitaService.insericiFormazioni(partita, squadraBianco, squadraNero, idAB, idAN);
				return "redirect:/partite/{idPartita}/vediDettagli";
			} else {
				String messaggioErrore = "ATTENZIONE! Presenza di giocatrici duplicate in formazione";
				return "redirect:/partite/gestione/{idPartita}/scegliFormazione?messaggio=" + messaggioErrore;
			}
		} catch (Exception ex) {
			return "redirect:/partite/gestione/{idPartita}/scegliFormazione?messaggio=" + ex.getMessage();
		}
	}

	// --- metodi di supporto non mappati su URL -------------------------------
	
	private boolean trovaDuplicati(String[] arrayStringhe) {
		boolean duplicato = false;
		String e;
		for ( int i = 0; i < arrayStringhe.length; i++ ) {
			e = arrayStringhe[i];
			for ( int j = i+1 ; j < arrayStringhe.length; j++ ) {
				if ( arrayStringhe[j].equals( e ) ) { duplicato = true; };
			}
		}
		return duplicato;
	}

	// --- setter che iniettano le dipendenze nei confronti dei Service --------

	@Autowired
	public void setPartitaService(PartitaService partitaService) {
		this.partitaService = partitaService;
	}

	@Autowired
	public void setSquadraService(SquadraService squadraService) {
		this.squadraService = squadraService;
	}

}
