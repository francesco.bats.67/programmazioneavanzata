package wp.test;

import java.text.DateFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import wp.model.dao.ArbitroDao;
import wp.model.dao.AtletaDao;
import wp.model.dao.AzioneDao;
import wp.model.dao.PartitaDao;
import wp.model.dao.PiscinaDao;
import wp.model.dao.RoleDao;
import wp.model.dao.SquadraDao;
import wp.model.dao.TecnicoDao;
import wp.model.dao.UserDetailsDao;
import wp.model.entities.Atleta;
import wp.model.entities.Partita;
import wp.model.entities.Squadra;
import wp.model.entities.Role;
import wp.model.entities.User;
import wp.test.TestDataServiceConfig;

public class Test_CaricaDati_Prova {

	public static void main(String[] args) {

		// crea oggetti che vanno ad utilizzare servizi e DAO
		
		// istanzia un contesto partendo dalla classe di configurazione per il test
		try (AnnotationConfigApplicationContext contesto =
				new AnnotationConfigApplicationContext(TestDataServiceConfig.class)) {
			
			// chiede al contesto un riferimento alla session factory
			SessionFactory sf = contesto.getBean("sessionFactory", SessionFactory.class);

			// chiede al contesto un riferimento ai DAO
			AtletaDao atletaDao = contesto.getBean("DaoAtleta", AtletaDao.class);
			PartitaDao partitaDao = contesto.getBean("DaoPartita", PartitaDao.class);
			SquadraDao squadraDao = contesto.getBean("DaoSquadra", SquadraDao.class);
			UserDetailsDao userDao = contesto.getBean("DaoUtente", UserDetailsDao.class);
			RoleDao roleDao = contesto.getBean("DaoRuolo", RoleDao.class);

			// apre una nuova sessione
			try (Session sessione = sf.openSession()) {
				// inietta in tutti i DAO la stessa nuova sessione
				atletaDao.setSession(sessione);
				partitaDao.setSession(sessione);
				squadraDao.setSession(sessione);
				userDao.setSession(sessione);
				roleDao.setSession(sessione);
				
				// TEST 1 - CARICAMENTO E AGGIORNAMENTO DATI NELLA BASE DATI
				
				sessione.beginTransaction();
				
				atletaDao.create("UCCELLA_V", "Valeria", "Uccella", 'F', "Italia", "0000000", 2001);
				atletaDao.create("IVANOVA_E", "Ivanova", "Elizaveta", 'F', "Rossija", "0000000", 2000);
				atletaDao.create("STRAPPATO_L", "Strappato", "Lisa", 'F', "Italia", "0000000", 1999);
				atletaDao.create("ALTAMURA_E", "Altamura", "Elena", 'F', "Italia", "0000000", 1998);
				atletaDao.create("DICLAUDIO_L", "Di Claudio", "Luna", 'F', "Italia", "0000000", 1997);
				atletaDao.create("PICOZZI_D", "Picozzi", "Domitilla", 'F', "Italia", "0000000", 1998);
			
				Squadra sqANV = squadraDao.create("ANV", "Vela", "Vela Nuoto", "Ancona");
				Squadra sqCTE = squadraDao.create("CTE", "Orizzonte", "L'Ekipe Orizzonte", "Catania");
				Squadra sqRMS = squadraDao.create("RMS", "Sis", "S.I.S.", "Roma");

				// crea partita di test e la lega alle squadre
				DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, Locale.ITALY);
				partitaDao.create(df.parse("20/1/2021"), "Serie A1", sqANV, sqCTE);
				partitaDao.create(df.parse("14/2/2021"), "Serie A1", sqRMS, sqANV);
				
				assert sqCTE.getPartiteTotale().size() == 0;
				assert sqRMS.getPartiteTotale().size() == 0;
				
				// con il refresh della sessione, rende persistento gli oggetti nella base dati				
				sessione.refresh(sqCTE);
				sessione.refresh(sqRMS);
				
				assert sqCTE.getPartiteTotale().size() == 1;
				assert sqRMS.getPartiteTotale().size() == 1;


				Partita g001 = partitaDao.create(df.parse("20/3/2021"), "Serie A1", sqCTE, sqCTE);
				g001.setSquadraNero(sqRMS);
				partitaDao.update(g001);
			
				// carica su tre oggetti atleta i risultati della ricerca per nome
				Atleta a1 = atletaDao.findById("ALTAMURA_E");
				Atleta a2 = atletaDao.findByName("Strappato", "Lisa");
				Atleta a3 = atletaDao.findByName("Di Claudio", "Luna");

				// effettua il commit della transazione
				sessione.getTransaction().commit();
			
				// inizia una nuova transazione
				sessione.beginTransaction();
				
				sqRMS.addAtleta(a3);
				sqRMS = squadraDao.update(sqRMS);
				
				assert(sqRMS.getAtleti().contains(a3));
				assert(a3.getSquadre().contains(sqRMS));
				
				// effettua il commit della transazione
				sessione.getTransaction().commit();
				
				// inizia una nuova transazione
				sessione.beginTransaction();
				
				sqANV.addAtleta(a1);
				sqANV.addAtleta(a2);
				sqANV = squadraDao.update(sqANV);
				
				assert sqANV.getAtleti().contains(a1) == true;
				assert sqANV.getAtleti().contains(a2) == true;
				assert a1.getSquadre().contains(sqANV) == true;
				assert a2.getSquadre().contains(sqANV) == true;

				/*	
				// rimuove le entità collegate prima di eliminare una entità
				sqRMS.getAtleti().clear();
				for (Partita p : sqRMS.getPartiteBianco()) {
					partitaDao.delete(p);
				}
				for (Partita p : sqRMS.getPartiteNero()) {
					partitaDao.delete(p);
				}

				sqRMS.getPartiteBianco().clear();
				sqRMS.getPartiteNero().clear();
				sqRMS = squadraDao.update(sqRMS);				


				squadraDao.delete(sqRMS);
				*/

				// effettua il commit della transazione
				sessione.getTransaction().commit();
					
				// TEST 2 - NAVIGAZIONE NELLA BASE DATI
				
				// inizia una nuova transazione
				sessione.beginTransaction();
			
				// carica tutte le squadre
				List<Squadra> allSquadre = squadraDao.findAll();
					
				// stampa a video tutti le squadre con le relative partite
				System.out.println("Numero delle squadre: " + allSquadre.size());
				for (Squadra squadra : allSquadre) {
					System.out.println(" - " + squadra.toString());

					Set<Partita> partiteSquadra = squadraDao.findPartiteBianco(squadra);
					System.out.println("Numero di partite contro la squadra: " + partiteSquadra.size());
					for (Partita partita : partiteSquadra) {
						System.out.println("  - " + partita.toString());			
					}
				}				

				// carica tutti gli atleti
				List<Atleta> allAtleti = atletaDao.findAll();
				
				// stampa a video tutti gli atleti con le squadre collegate
				System.out.println("Numero degli atleti: " + allAtleti.size());
				for (Atleta atleta : allAtleti) {
					System.out.println(" - " + atleta.getNomeCompleto() + " (" + atleta.getNazione() + ")");
					Set<Squadra> squadreAtleta = atleta.getSquadre();

					if (squadreAtleta == null) {
						squadreAtleta = new HashSet<Squadra>();
					}

					System.out.println("Numero delle squadre con l'atleta: " + squadreAtleta.size());
					for (Squadra squadra : squadreAtleta) {
						System.out.println("  - " + squadra.toString());
					}
				}

				sessione.getTransaction().commit();

				// TEST 3 - CREA UTENTI
				
				sessione.beginTransaction();
				
				Role usr = roleDao.create("USER");
				Role adr = roleDao.create("ADMIN");
				
				sessione.getTransaction().commit();
				
				sessione.beginTransaction();

				User us1 = userDao.create("user1", userDao.encryptPassword("user1"), true);			
				us1.addRole(usr);
				
				User us2 = userDao.create("user2", userDao.encryptPassword("user2"), true);
				us2.addRole(usr);

				User ad1 = userDao.create("admin1", userDao.encryptPassword("admin1"), true);			
				ad1.addRole(adr);
				
				userDao.update(us1);
				userDao.update(us2);
				userDao.update(ad1);

				sessione.getTransaction().commit();

			} catch (Exception exc) {
//				logger.error("Eccezione: " + e.getMessage());
				exc.printStackTrace(System.err);
			}


			
		} catch (Exception exc) {
//			logger.error("Eccezione: " + e.getMessage());
			exc.printStackTrace(System.err);
		}
	
	}

}
