package wp.test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import wp.app.DataServiceConfig;
import wp.model.dao.AtletaDao;
import wp.model.dao.AzioneDao;
import wp.model.dao.PartitaDao;
import wp.model.dao.SquadraDao;
import wp.model.dao.TabellinoDao;
import wp.model.dao.TecnicoDao;
import wp.model.entities.Atleta;
import wp.model.entities.Partita;
import wp.model.entities.Squadra;
import wp.model.entities.Tabellino;
import wp.model.entities.Tecnico;

/**
 * INIZIALIZZA BASE DATI VELAWP CARICA FORMAZIONI
 * Classe per caricare le formazioni delle partite di pallanuoto femminile Serie A1:
 *    19.  15/05/2021	TRIESTE - VELA
 *    18.  20/03/2021	VELA - FLORENTIA
 *    17.  06/03/2021	BOGLIASCO - VELA
 * @author Francesco Battistelli - UNIVPM
 * @version 30 agosto 2021
 */
public class InitDBWP_CaricaFormazioni {

	public static void main(String[] args) {

		// crea oggetti che vanno ad utilizzare servizi e DAO
		
		// istanzia un contesto partendo dalla classe di configurazione per il test
		try (AnnotationConfigApplicationContext contesto =
				new AnnotationConfigApplicationContext(DataServiceConfig.class)) {
			
			// chiede al contesto un riferimento alla session factory
			SessionFactory sf = contesto.getBean("sessionFactory", SessionFactory.class);

			// chiede al contesto un riferimento ai DAO
			AtletaDao atletaDao = contesto.getBean("DaoAtleta", AtletaDao.class);
			AzioneDao azioneDao = contesto.getBean("DaoAzione", AzioneDao.class);
			PartitaDao partitaDao = contesto.getBean("DaoPartita", PartitaDao.class);
			SquadraDao squadraDao = contesto.getBean("DaoSquadra", SquadraDao.class);
			TecnicoDao tecnicoDao = contesto.getBean("DaoTecnico", TecnicoDao.class);
			TabellinoDao tabellinoDao = contesto.getBean("DaoTabellino", TabellinoDao.class);

			// apre una nuova sessione
			try (Session sessione = sf.openSession()) {
				
				// inietta in tutti i DAO la stessa nuova sessione
				atletaDao.setSession(sessione);
				azioneDao.setSession(sessione);
				partitaDao.setSession(sessione);
				squadraDao.setSession(sessione);
				tecnicoDao.setSession(sessione);
				tabellinoDao.setSession(sessione);

				// dichiarazione e inizializzazione delle variabili

				Partita partita;
				Squadra squadraB;
				Squadra squadraN;
				Atleta[] AB = { null, null, null, null, null, null, null, null, null, null, null, null, null, null };
				Atleta[] AN = { null, null, null, null, null, null, null, null, null, null, null, null, null, null };
				Tabellino[] TB = { null, null, null, null, null, null, null, null, null, null, null, null, null, null };
				Tabellino[] TN = { null, null, null, null, null, null, null, null, null, null, null, null, null, null };			
				Tabellino portiereB;
				Tabellino portiereN;

				boolean in = false;
				float min = 0;
				int rs = 0;
				int tf = 0;
				int fp = 0;
				boolean es = false;
				int[] sRe = {0,0,0,0,0,0};	
				int[] sTi = {0,0,0,0,0,0};
				int[] sFa = {0,0,0,0,0};
				int[] sPo = {0,0,0,0,0,0};	

				// CARICAMENTO FORMAZIONI A REFERTO DELLA PARTITA N. 19 ( TRIESTE - VELA )

				sessione.beginTransaction();

				partita = partitaDao.findById(19L);
				squadraB = squadraDao.findById("TSP");
				squadraN = squadraDao.findById("ANV");
				partita.setTecnicoBianco(tecnicoDao.findById("COLAUTTI_I"));
				partita.setTecnicoNero(tecnicoDao.findById("PACE_M"));

				AB[1] = atletaDao.findById("INGANNAMOR_S");
				AB[2] = atletaDao.findById("GAGLIARDI_G");
				AB[3] = atletaDao.findById("BOERO_V");
				AB[4] = atletaDao.findById("GANT_V");
				AB[5] = atletaDao.findById("MARUSSI_G");
				AB[6] = atletaDao.findById("LONZA_F");
				AB[7] = atletaDao.findById("KLATOWSKI_G");
				AB[8] = atletaDao.findById("INGANNAMOR_E");
				AB[9] = atletaDao.findById("BETTINI_D");
				AB[10] = atletaDao.findById("RATTELLI_F");
				AB[11] = atletaDao.findById("JANKOVIC_A");
				AB[12] = atletaDao.findById("RUSSIGNAN_A");
				AB[13] = atletaDao.findById("KRASTI_G");
				
				AN[1] = atletaDao.findById("UCCELLA_V");
				AN[2] = atletaDao.findById("STRAPPATO_L");
				AN[3] = atletaDao.findById("IVANOVA_E");
				AN[4] = atletaDao.findById("RICCIO_A");
				AN[5] = atletaDao.findById("VECCHIAREL_E");
				AN[6] = atletaDao.findById("BERSACCHIA_G");
				AN[7] = atletaDao.findById("BARTOCCI_C");
				AN[8] = atletaDao.findById("OLIVIERI_M");
				AN[9] = atletaDao.findById("COLLETTA_F");
				AN[10] = atletaDao.findById("DEMATTEIS_V");
				AN[11] = atletaDao.findById("ALTAMURA_E");
				AN[12] = atletaDao.findById("QUATTRINI_E");
				AN[13] = atletaDao.findById("ANDREONI_A");
				
				// creazione tabellini della squadra
				for (int i = 1; i < 14; i++) {
					TB[i] = tabellinoDao.create(i, in, min, rs, tf, fp, es, sRe, sTi, sFa, null, partita, squadraB, AB[i]);
					TN[i] = tabellinoDao.create(i, in, min, rs, tf, fp, es, sRe, sTi, sFa, null, partita, squadraN, AN[i]);
				}
				partita.setPeriodoGioco(1);		
				portiereB = tabellinoDao.findTabellinoGaraCalottina(partita, squadraB, 1);
				portiereN = tabellinoDao.findTabellinoGaraCalottina(partita, squadraN, 1);
				partita.setPortiereBianco(portiereB);
				partita.setPortiereNero(portiereN);
				tabellinoDao.setPortiere(portiereB);
				tabellinoDao.setPortiere(portiereN);
				azioneDao.create(false, "Portiere iniziale", null, null, portiereB.getNumeroCalottina(), partita, new int[] {1,8,0}, portiereB.getSquadra(), portiereB.getAtleta());
				azioneDao.create(false, "Portiere iniziale", null, null, portiereN.getNumeroCalottina(), partita, new int[] {1,8,0}, portiereN.getSquadra(), portiereN.getAtleta());

				sessione.getTransaction().commit();

				// CARICAMENTO FORMAZIONI A REFERTO DELLA PARTITA N. 18 ( VELA - FLORENTIA )

				sessione.beginTransaction();

				partita = partitaDao.findById(18L);
				squadraB = squadraDao.findById("ANV");
				squadraN = squadraDao.findById("FLO");
				partita.setTecnicoBianco(tecnicoDao.findById("PACE_M"));
				partita.setTecnicoNero(tecnicoDao.findById("COTTI_A"));

				AB[1] = atletaDao.findById("UCCELLA_V");
				AB[2] = atletaDao.findById("STRAPPATO_L");
				AB[3] = atletaDao.findById("IVANOVA_E");
				AB[4] = atletaDao.findById("SANTINI_M");
				AB[5] = atletaDao.findById("VECCHIAREL_E");
				AB[6] = atletaDao.findById("BERSACCHIA_G");
				AB[7] = atletaDao.findById("BARTOCCI_C");
				AB[8] = atletaDao.findById("OLIVIERI_M");
				AB[9] = atletaDao.findById("COLLETTA_F");
				AB[10] = atletaDao.findById("DEMATTEIS_V");
				AB[11] = atletaDao.findById("ALTAMURA_E");
				AB[12] = atletaDao.findById("QUATTRINI_E");
				AB[13] = atletaDao.findById("ANDREONI_A");

				AN[1] = atletaDao.findById("BANCHELLI_C");
				AN[2] = atletaDao.findById("LANDI_I");
				AN[3] = atletaDao.findById("LEPORE_L");
				AN[4] = atletaDao.findById("CORDOVANI_S");
				AN[5] = atletaDao.findById("GASPARRI_G");
				AN[6] = atletaDao.findById("VITTORI_N");
				AN[7] = atletaDao.findById("NESTI_L");
				AN[8] = atletaDao.findById("FRANCINI_R");
				AN[9] = atletaDao.findById("GIACHI_G");
				AN[10] = atletaDao.findById("NENCHA_C");
				AN[11] = atletaDao.findById("MARIONI_V");
				AN[12] = atletaDao.findById("MUGNAI_B");
				AN[13] = atletaDao.findById("PEREGO_L");
				
				// creazione tabellini della squadra
				for (int i = 1; i < 14; i++) {
					TB[i] = tabellinoDao.create(i, in, min, rs, tf, fp, es, sRe, sTi, sFa, null, partita, squadraB, AB[i]);
					TN[i] = tabellinoDao.create(i, in, min, rs, tf, fp, es, sRe, sTi, sFa, null, partita, squadraN, AN[i]);
				}
				partita.setPeriodoGioco(1);		
				portiereB = tabellinoDao.findTabellinoGaraCalottina(partita, squadraB, 1);
				portiereN = tabellinoDao.findTabellinoGaraCalottina(partita, squadraN, 1);
				partita.setPortiereBianco(portiereB);
				partita.setPortiereNero(portiereN);
				tabellinoDao.setPortiere(portiereB);
				tabellinoDao.setPortiere(portiereN);
				azioneDao.create(false, "Portiere iniziale", null, null, portiereB.getNumeroCalottina(), partita, new int[] {1,8,0}, portiereB.getSquadra(), portiereB.getAtleta());
				azioneDao.create(false, "Portiere iniziale", null, null, portiereN.getNumeroCalottina(), partita, new int[] {1,8,0}, portiereN.getSquadra(), portiereN.getAtleta());

				sessione.getTransaction().commit();
				
				// CARICAMENTO FORMAZIONI A REFERTO DELLA PARTITA N. 17 ( BOGLIASCO - VELA )

				sessione.beginTransaction();

				partita = partitaDao.findById(17L);
				squadraB = squadraDao.findById("BG5");
				squadraN = squadraDao.findById("ANV");
				partita.setTecnicoBianco(tecnicoDao.findById("SINATRA_M"));
				partita.setTecnicoNero(tecnicoDao.findById("PACE_M"));

				AB[1] = atletaDao.findById("MALARA_C");
				AB[2] = atletaDao.findById("ROSTA_B");
				AB[3] = atletaDao.findById("CAVALLINI_G");
				AB[4] = atletaDao.findById("CUZZUPE_G");
				AB[5] = atletaDao.findById("MAUCERI_A");
				AB[6] = atletaDao.findById("MILLO_G");
				AB[7] = atletaDao.findById("SANTINELLI_G");
				AB[8] = atletaDao.findById("ROGONDINO_R");
				AB[9] = atletaDao.findById("PAGANELLO_M");
				AB[10] = atletaDao.findById("CARPANETO_M");
				AB[11] = atletaDao.findById("FRANCI_V");
				AB[12] = atletaDao.findById("AMEDEO_G");
				AB[13] = atletaDao.findById("SOKHNA_A");
				AN[1] = atletaDao.findById("UCCELLA_V");
				AN[2] = atletaDao.findById("STRAPPATO_L");
				AN[3] = atletaDao.findById("IVANOVA_E");
				AN[4] = atletaDao.findById("SANTINI_M");
				AN[5] = atletaDao.findById("VECCHIAREL_E");
				AN[6] = atletaDao.findById("BERSACCHIA_G");
				AN[7] = atletaDao.findById("BARTOCCI_C");
				AN[8] = atletaDao.findById("OLIVIERI_M");
				AN[9] = atletaDao.findById("COLLETTA_F");
				AN[10] = atletaDao.findById("DEMATTEIS_V");
				AN[11] = atletaDao.findById("ALTAMURA_E");
				AN[12] = atletaDao.findById("QUATTRINI_E");
				AN[13] = atletaDao.findById("ANDREONI_A");
				
				// creazione tabellini della squadra
				for (int i = 1; i < 14; i++) {
					TB[i] = tabellinoDao.create(i, in, min, rs, tf, fp, es, sRe, sTi, sFa, null, partita, squadraB, AB[i]);
					TN[i] = tabellinoDao.create(i, in, min, rs, tf, fp, es, sRe, sTi, sFa, null, partita, squadraN, AN[i]);
				}
				partita.setPeriodoGioco(1);		
				portiereB = tabellinoDao.findTabellinoGaraCalottina(partita, squadraB, 1);
				portiereN = tabellinoDao.findTabellinoGaraCalottina(partita, squadraN, 1);
				partita.setPortiereBianco(portiereB);
				partita.setPortiereNero(portiereN);
				tabellinoDao.setPortiere(portiereB);
				tabellinoDao.setPortiere(portiereN);
				azioneDao.create(false, "Portiere iniziale", null, null, portiereB.getNumeroCalottina(), partita, new int[] {1,8,0}, portiereB.getSquadra(), portiereB.getAtleta());
				azioneDao.create(false, "Portiere iniziale", null, null, portiereN.getNumeroCalottina(), partita, new int[] {1,8,0}, portiereN.getSquadra(), portiereN.getAtleta());

				sessione.getTransaction().commit();				

			} catch (Exception exc) {
				exc.printStackTrace(System.err);
			}
		
		} catch (Exception exc) {
			exc.printStackTrace(System.err);
		}
	
	}

}

