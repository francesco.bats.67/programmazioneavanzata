package wp.test;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import wp.model.dao.ArbitroDao;
import wp.model.dao.AtletaDao;
import wp.model.dao.AzioneDao;
import wp.model.dao.PartitaDao;
import wp.model.dao.PiscinaDao;
import wp.model.dao.RoleDao;
import wp.model.dao.SquadraDao;
import wp.model.dao.TabellinoDao;
import wp.model.dao.TecnicoDao;
import wp.model.dao.UserDetailsDao;
import wp.model.entities.Partita;
import wp.model.entities.Role;
import wp.model.entities.Squadra;
import wp.model.entities.User;

/**
 * INIZIALIZZA BASE DATI VELAWP
 * Classe per caricare i dati delle squadre, delle giocatrici e delle partite della
 *  Vela Nuoto nei campionati di pallanuoto femminile Serie A1 2019-20 e 2020-21
 * @author Francesco Battistelli - UNIVPM
 * @version 8 agosto 2021
 */
public class InitDBWaterPolo {

	public static void main(String[] args) {

		// istanzia un contesto partendo dalla classe di configurazione per il test
		try (AnnotationConfigApplicationContext contesto =
				new AnnotationConfigApplicationContext(TestDataServiceConfig.class)) {
			
			// chiede al contesto un riferimento alla session factory
			SessionFactory sf = contesto.getBean("sessionFactory", SessionFactory.class);

			// chiede al contesto un riferimento ai DAO
			ArbitroDao arbitroDao = contesto.getBean("DaoArbitro", ArbitroDao.class);
			AtletaDao atletaDao = contesto.getBean("DaoAtleta", AtletaDao.class);
			AzioneDao azioneDao = contesto.getBean("DaoAzione", AzioneDao.class);
			PartitaDao partitaDao = contesto.getBean("DaoPartita", PartitaDao.class);
			PiscinaDao piscinaDao = contesto.getBean("DaoPiscina", PiscinaDao.class);
			SquadraDao squadraDao = contesto.getBean("DaoSquadra", SquadraDao.class);
			TabellinoDao tabellinoDao = contesto.getBean("DaoTabellino", TabellinoDao.class);
			TecnicoDao tecnicoDao = contesto.getBean("DaoTecnico", TecnicoDao.class);
			UserDetailsDao userDao = contesto.getBean("DaoUtente", UserDetailsDao.class);
			RoleDao roleDao = contesto.getBean("DaoRuolo", RoleDao.class);

			// apre una nuova sessione
			try (Session sessione = sf.openSession()) {
				
				// inietta in tutti i DAO la stessa nuova sessione
				arbitroDao.setSession(sessione);
				atletaDao.setSession(sessione);
				azioneDao.setSession(sessione);		
				partitaDao.setSession(sessione);
				piscinaDao.setSession(sessione);
				squadraDao.setSession(sessione);		
				tabellinoDao.setSession(sessione);
				tecnicoDao.setSession(sessione);
				userDao.setSession(sessione);
				roleDao.setSession(sessione);
				
				// 00 - CARICAMENTO UTENTI E RUOLI NELLA BASE DATI

				sessione.beginTransaction();

				Role adr = roleDao.create("ADMIN");
				Role usr = roleDao.create("USER");

				User ad1 = userDao.create("admin1", userDao.encryptPassword("admin1"), true);
				User us1 = userDao.create("user1", userDao.encryptPassword("user1"), true);					
				User us2 = userDao.create("user2", userDao.encryptPassword("user2"), true);				
				ad1.addRole(adr);
				us1.addRole(usr);
				us2.addRole(usr);
				userDao.update(us1);
				userDao.update(us2);
				userDao.update(ad1);

				sessione.getTransaction().commit();

				// 01 - CARICAMENTO PISCINE NELLA BASE DATI
				
				sessione.beginTransaction();

				piscinaDao.create("Bianchi", "Polo Natatorio Bruno Bianchi", "Trieste");
				piscinaDao.create("Faustina", "Piscina Faustina Sporting", "Lodi");
				piscinaDao.create("ForoItalico", "Complesso Natatorio Foro Italico", "Roma");
				piscinaDao.create("MonteBianco", "Piscina Monte Bianco", "Verona");
				piscinaDao.create("Nannini", "Piscina Goffredo Nannini", "Firenze");
				piscinaDao.create("Nesima", "Piscina Comunale Nesima", "Catania");
				piscinaDao.create("Ostia", "Polo Natatorio di Ostia", "Ostia (RM)");		
				piscinaDao.create("Passetto", "Piscina Comunale del Passetto", "Ancona");
				piscinaDao.create("Plebiscito", "Centro Sportivo del Plebiscito", "Padova");
				piscinaDao.create("Rapallo", "Piscina Comunale Poggiolino", "Rapallo (GE)");
				piscinaDao.create("Scuderi", "Piscina Comunale Francesco Scuderi", "Catania");
				piscinaDao.create("Vassallo", "Piscina Gianni Vassallo", "Bogliasco (GE)");
				
				sessione.getTransaction().commit();
				
				// 02 - CARICAMENTO ARBITRI NELLA BASE DATI
				
				sessione.beginTransaction();

				arbitroDao.create("ALFI_S", "Stefano", "Alfi", 'M', "Napoli");
				arbitroDao.create("BARLETTA_L", "Luigi", "Barletta", 'M', "Napoli");
				arbitroDao.create("BENSAIA_P", "Paolo", "Bensaia", 'M', "Firenze");
				arbitroDao.create("BRAGHINI_F", "Federico", "Braghini", 'M', "Roma");
				arbitroDao.create("CARMIGNANI_R", "Riccardo", "Carmignani", 'M', "Messina");
				arbitroDao.create("CASTAGNOLA_L", "Luca", "Castagnola", 'M', "Roma");
				arbitroDao.create("CENTINEO_G", "Gianluca", "Centineo", 'M', "Palermo");
				arbitroDao.create("COLLANTONI_F", "Fabio", "Collantoni", 'M', "Grosseto");
				arbitroDao.create("COLOMBO_R", "Raffaele", "Colombo", 'M', "Como");
				arbitroDao.create("DANTONI_R", "Riccardo", "D'Antoni", 'M', "Siracusa");
				arbitroDao.create("ERCOLI_M", "Marco" ,"Ercoli", 'M', "Fermo");
				arbitroDao.create("FERRARI_A", "Alessia", "Ferrari", 'F', "Genova");
				arbitroDao.create("FRAUENFELD_V", "Vittorio", "Frauenfelder", 'M', "Salerno");
				arbitroDao.create("FUSCO_G", "Giuseppe", "Fusco", 'M', "Torino");
				arbitroDao.create("GUARRACINO_A", "Antonio", "Guarracino", 'M', "Napoli");
				arbitroDao.create("NAVARRA_B", "Bruno", "Navarra", 'M', "Roma");
				arbitroDao.create("NICOLOSI_G", "Giuliana", "Nicolosi", 'F', "Catania");
				arbitroDao.create("PASCUCCI_A", "Antonio", "Pascucci", 'M', "Caserta");
				arbitroDao.create("PETRONILLI_A", "Arnaldo", "Petronilli", 'M', "Roma");
				arbitroDao.create("PIANO_M", "Marco", "Piano", 'M', "Genova");
				arbitroDao.create("RICCIOTTI_F", "Fabio", "Ricciotti", 'M', "Roma");
				arbitroDao.create("ROMOLINI_F", "Francesco", "Romolini", 'M', "Firenze");
				arbitroDao.create("SCAPPINI_S", "Stefano", "Scappini", 'M', "Roma");
				arbitroDao.create("SCHIAVO_M", "Mirko", "Schiavo", 'M', "Palermo");
				arbitroDao.create("SCILLATO_D", "Daniela", "Scillato", 'F', "Roma");
				arbitroDao.create("SEVERO_A", "Alessandro", "Severo", 'M', "Roma");
				arbitroDao.create("TACCINI_C", "Cristina", "Taccini", 'F', "Torino");

				sessione.getTransaction().commit();

				// 03 - CARICAMENTO SQUADRE NELLA BASE DATI
				
				sessione.beginTransaction();

				Squadra sqANV = squadraDao.create("ANV", "Vela", "Vela Nuoto Ancona", "Ancona");
				Squadra sqBG5 = squadraDao.create("BG5", "Bogliasco", "Bogliasco 1951", "Bogliasco (GE)");
				Squadra sqCON = squadraDao.create("CON", "Como", "Como Nuoto", "Como");
				Squadra sqCTE = squadraDao.create("CTE", "Orizzonte", "L'Ekipe Orizzonte", "Catania");
				Squadra sqFLO = squadraDao.create("FLO", "Florentia", "Rari Nantes Florentia", "Firenze");
				Squadra sqMIN = squadraDao.create("MIN", "NC Milano", "Nuoto Club Milano", "Milano");
				Squadra sqPDP = squadraDao.create("PDP", "Plebiscito", "C.S. Plebiscito Padova", "Padova");
				Squadra sqRMS = squadraDao.create("RMS", "Sis Roma", "S.I.S. Roma", "Roma");
				Squadra sqRPP = squadraDao.create("RPP", "Rapallo", "Rapallo P.N.", "Rapallo (GE)");
				Squadra sqTSP = squadraDao.create("TSP", "Trieste", "Pallanuoto Trieste", "Trieste");
				Squadra sqVRC = squadraDao.create("VRC", "Css Verona", "C.S.S. Verona", "Verona");


				sessione.getTransaction().commit();
				
				// 04 - CARICAMENTO TECNICI NELLA BASE DATI
				
				sessione.beginTransaction();

				tecnicoDao.create("ANTONUCCI_L", "Luca", "Antonucci", 'M', "ITA");
				tecnicoDao.create("BINCHI_L", "Leonardo", "Binchi", 'M', "ITA");
				tecnicoDao.create("CAPANNA_M", "Marco", "Capanna", 'M', "ITA");			
				tecnicoDao.create("COLAUTTI_I", "Ilaria", "Colautti", 'F', "ITA");
				tecnicoDao.create("COTTI_A", "Aleksandra", "Cotti", 'F', "ITA");
				tecnicoDao.create("MARINELLI_U", "Ugo", "Marinelli", 'M', "ITA");
				tecnicoDao.create("MICELI_M", "Martina", "Miceli", 'F', "ITA");
				tecnicoDao.create("PACE_M", "Milko", "Pace", 'M', "ITA");
				tecnicoDao.create("POSTERIVO_S", "Stefano", "Posterivo", 'M', "ITA");
				tecnicoDao.create("SINATRA_M", "Mario", "Sinatra", 'M', "ITA");
				tecnicoDao.create("ZACCARIA_G", "Giovanni", "Zaccaria", 'M', "ITA");
				tecnicoDao.create("ZIZZA_P", "Paolo", "Zizza", 'M', "ITA");

				sessione.getTransaction().commit();

				// 05 - CARICAMENTO ATLETI NELLA BASE DATI
				
				sessione.beginTransaction();
			
				atletaDao.create("AGOSTA_E", "Elisa", "Agosta", 'F', "ITA", null, 1985);
				atletaDao.create("AIELLO_R", "Rosaria", "Aiello", 'F', "ITA", null, 1989);
				atletaDao.create("ALBASINI_A", "Anna", "Albasini", 'F', "ITA", null, 2004);
				atletaDao.create("ALOGBO_K", "Kristina", "Alogbo", 'F', "CAN", null, 1986);
				atletaDao.create("ALTAMURA_E", "Elena", "Altamura", 'F', "ITA", null, 1998);
				atletaDao.create("AMBROSINI_S", "Silvia ", "Ambrosini", 'F', "ITA", null, 2004);
				atletaDao.create("AMEDEO_G", "Giorgia Grazia", "Amedeo", 'F', "ITA", null, 2000);		
				atletaDao.create("ANDREONI_A", "Aurora", "Andreoni", 'F', "ITA", null, 2000);
				atletaDao.create("APILONGO_G", "Gaia", "Apilongo", 'F', "ITA", null, 1992);			
				atletaDao.create("AVEGNO_S", "Silvia", "Avegno", 'F', "ITA", null, 1997);
				atletaDao.create("BANCHELLI_C", "Caterina", "Banchelli", 'F', "ITA", null, 2000);
				atletaDao.create("BARBIERI_D", "Dada", "Barbieri", 'F', "ITA", null, 2006);
				atletaDao.create("BARTOCCI_C", "Chiara", "Bartocci", 'F', "ITA", null, 2004);
				atletaDao.create("BARTOLINI_G", "Giulia", "Bartolini", 'F', "ITA", null, 1990);
				atletaDao.create("BARZON_L", "Laura", "Barzon", 'F', "ITA", null, 1992);
				atletaDao.create("BEKHAZI_J", "Joelle", "Bekhazi", 'F', "CAN", null, 1987);
				atletaDao.create("BENATI_M", "Martina", "Benati", 'F', "ITA", null, 2004);
				atletaDao.create("BERSACCHIA_G", "Giulia", "Bersacchia", 'F', "ITA", null, 2000);
				atletaDao.create("BETTINI_D", "Dafne", "Bettini", 'F', "ITA", null, 2003);
				atletaDao.create("BIANCHI_A", "Aurora", "Bianchi", 'F', "ITA", null, 2002);
				atletaDao.create("BIANCO_E", "Elisa", "Bianco", 'F', "ITA", null, 2003);
				atletaDao.create("BIANCONI_R", "Roberta", "Bianconi", 'F', "ITA", null, 1989);
				atletaDao.create("BOERO_V", "Virginia", "Boero", 'F', "ITA", null, 1995);
				atletaDao.create("BORG_E", "Elena", "Borg", 'F', "ITA", null, 1999);
				atletaDao.create("BORGHETTI_J", "Jara", "Borghetti", 'F', "ITA", null, 1993);
				atletaDao.create("BOZZETTA_B", "Beatrice", "Bozzetta", 'F', "ITA", null, 2004);
				atletaDao.create("BRAGA_I", "Ines Maria", "Braga", 'F', "POR", null, 1984);
				atletaDao.create("BRANDIMART_M", "Martina", "Brandimarte", 'F', "ITA", null, 2000);
				atletaDao.create("BUJKA_B", "Barbara", "Bujka", 'F', "MAG", null, 1986);
				atletaDao.create("CABONA_B", "Benedetta", "Cabona", 'F', "ITA", null, 2003);
				atletaDao.create("CARDILLO_A", "Anna", "Cardillo", 'F', "ITA", null, 2000);
				atletaDao.create("CARREGA_C", "Carla", "Carrega", 'F', "ITA", null, 1995);
				atletaDao.create("CASABIANCA_M", "Martina", "Casabianca", 'F', "ITA", null, 2001);
				atletaDao.create("CARNESECCH_A", "Aurora", "Carnesecchi", 'F', "ITA", null, 2004);
				atletaDao.create("CAROTENUTO_G", "Giulia", "Carotenuto", 'F', "ITA", null, 1988);
				atletaDao.create("CARPANETO_M", "Marta", "Carpaneto", 'F', "ITA", null, 2004);
				atletaDao.create("CASSON_A", "Alessia", "Casson", 'F', "ITA", null, 1997);
				atletaDao.create("CASTAGNINI_E", "Eleonora", "Castagnini", 'F', "ITA", null, 2002);
				atletaDao.create("CAVALLINI_G", "Ginevra", "Cavallini", 'F', "ITA", null, 2005);
				atletaDao.create("CENTANNI_S", "Sara", "Centanni", 'F', "ITA", null, 1995);
				atletaDao.create("CERGOL_L", "Lucrezia Lys", "Cergol", 'F', "ITA", null, 2001);
				atletaDao.create("CHIAPPINI_I", "Izabella", "Chiappini", 'F', "ITA", null, 1995);
				atletaDao.create("COCCHIERE_A", "Agnese", "Cocchiere", 'F', "ITA", null, 1999);
				atletaDao.create("COCCHIERE_T", "Teresa", "Cocchiere", 'F', "ITA", null, 2001);
				atletaDao.create("COLLETTA_F", "Francesca", "Colletta", 'F', "ITA", null, 2000);
				atletaDao.create("CONDORELLI_G", "Giuseppina Aurora", "Condorelli", 'F', "ITA", null, 2001);
				atletaDao.create("CORDOVANI_S", "Sara", "Cordovani", 'F', "ITA", null, 2001);
				atletaDao.create("CRISCUOLO_C", "Claudia", "Criscuolo", 'F', "ITA", null, 1995);
				atletaDao.create("CRISCUOLO_S", "Sonia", "Criscuolo", 'F', "ITA", null, 1991);
				atletaDao.create("CROVETTO_M", "Michela", "Crovetto", 'F', "ITA", null, 2000);
				atletaDao.create("CRUDELE_G", "Giulia", "Crudele", 'F', "ITA", null, 1993);
				atletaDao.create("CUZZUPE_G", "Giulia", "Cuzzup�", 'F', "ITA", null, 1999);
				atletaDao.create("DAMICO_A", "Agnese", "D'Amico", 'F', "ITA", null, 1994);
				atletaDao.create("DARIO_S", "Sara", "Dario", 'F', "ITA", null, 1994);
				atletaDao.create("DEMARCH_E", "Emma", "De March", 'F', "ITA", null, 2005);
				atletaDao.create("DEMATTEIS_V", "Valeria", "De Matteis", 'F', "ITA", null, 1995);
				atletaDao.create("DESERTI_S", "Sofia", "Deserti", 'F', "ITA", null, 2006);
				atletaDao.create("DEVINCENTI_F", "Federica", "De Vincentiis", 'F', "ITA", null, 1998);
				atletaDao.create("DICLAUDIO_L", "Luna", "Di Claudio", 'F', "ITA", null, 1997);
				atletaDao.create("DIMARTINO_T", "Teresa", "Di Martino", 'F', "ITA", null, 1992);			
				atletaDao.create("DONADIO_G", "Giorgia", "Donadio", 'F', "ITA", null, 2005);
				atletaDao.create("EMMOLO_G", "Giulia Enrica", "Emmolo", 'F', "ITA", null, 1991);
				atletaDao.create("ESPOSITO_A", "Adele", "Esposito", 'F', "ITA", null, 1999);
				atletaDao.create("FALCONI_C", "Carola", "Falconi", 'F', "ITA", null, 1991);
				atletaDao.create("FALCONI_E", "Elena", "Falconi", 'F', "ITA", null, 2001);
				atletaDao.create("FARAGO_B", "Berta", "Farag�", 'F', "ITA", null, 2001);
				atletaDao.create("FERRETTI_M", "Michela", "Ferretti", 'F', "ITA", null, 1995);
				atletaDao.create("FIORE_G", "Giulia", "Fiore", 'F', "ITA", null, 1999);
				atletaDao.create("FORESTA_C", "Chiara", "Foresta", 'F', "ITA", null, 1998);
				atletaDao.create("FRANCI_V", "Viola", "Franci", 'F', "ITA", null, 2000);
				atletaDao.create("FRANCINI_R", "Rebecca", "Francini", 'F', "ITA", null, 1994);
				atletaDao.create("GABUSI_G", "Giorgia", "Gabusi", 'F', "ITA", null, 2003);
				atletaDao.create("GAGLIARDI_G", "Gaia", "Gagliardi", 'F', "ITA", null, 2001);
				atletaDao.create("GALARDI_G", "Giuditta", "Galardi", 'F', "ITA", null, 1995);
				atletaDao.create("GANT_V", "Veronica", "Gant", 'F', "ITA", null, 2002);
				atletaDao.create("GARIBOTTI_A", "Arianna", "Garibotti", 'F', "ITA", null, 1989);
				atletaDao.create("GASPARRI_G", "Giulia", "Gasparri", 'F', "ITA", null, 1998);
				atletaDao.create("GIACHI_G", "Gloria", "Giachi", 'F', "ITA", null, 1991);
				atletaDao.create("GIACON_C", "Carolina", "Giacon", 'F', "ITA", null, 2001);
				atletaDao.create("GITTO_U", "Ursula", "Gitto", 'F', "ITA", null, 1993);
				atletaDao.create("GIUFFRIDA_M", "Marta ", "Giuffrida", 'F', "ITA", null, 2003);
				atletaDao.create("GIUSTINI_S", "Sofia", "Giustini", 'F', "ITA", null, 2003);
				atletaDao.create("GORLERO_G", "Giulia", "Gorlero", 'F', "ITA", null, 1990);
				atletaDao.create("GOTTARDO_M", "Martina", "Gottardo", 'F', "ITA", null, 1996);
				atletaDao.create("GRAGNOLATI_A", "Arianna", "Gragnolati", 'F', "ITA", null, 1996);
				atletaDao.create("GREGORUTTI_G", "Gaia ", "Gregorutti", 'F', "ITA", null, 2003);
				atletaDao.create("GUERCI_G", "Guendalina", "Guerci", 'F', "ITA", null, 1990);
				atletaDao.create("IANNARELLI_A", "Alessia", "Iannarelli", 'F', "ITA", null, 2002);
				atletaDao.create("IMPERATRIC_M", "Marzia", "Imperatrice", 'F', "ITA", null, 2000);
				atletaDao.create("INGANNAMOR_E", "Elisa", "Ingannamorte", 'F', "ITA", null, 1999);
				atletaDao.create("INGANNAMOR_S", "Sara", "Ingannamorte", 'F', "ITA", null, 2000);
				atletaDao.create("IOANNOU_C", "Carolina", "Ioannou", 'F', "ITA", null, 1996);
				atletaDao.create("ISETTA_L", "Ludovica", "Isetta", 'F', "ITA", null, 2003);
				atletaDao.create("IVANOVA_E", "Elizaveta", "Ivanova", 'F', "RUS", null, 2000);
				atletaDao.create("JANKOVIC_A", "Aleksandra", "Jankovic", 'F', "ITA", null, 2002);
				atletaDao.create("KEMPF_P", "Polina", "Kempf", 'F', "RUS", null, 1999);
				atletaDao.create("KLATOWSKI_G", "Giorgia", "Klatowski", 'F', "ITA", null, 2003);
				atletaDao.create("KRASTI_G", "Gioia", "Krasti", 'F', "ITA", null, 1997);
				atletaDao.create("KUZINA_S", "Svetlana", "Kuzina", 'F', "RUS", null, 1993);
				atletaDao.create("LANDI_I", "Irene", "Landi", 'F', "ITA", null, 1999);
				atletaDao.create("LEONE_M", "Morena", "Leone", 'F', "ITA", null, 2004);
				atletaDao.create("LEPORE_L", "Letizia Marina", "Lepore", 'F', "ITA", null, 2002);
				atletaDao.create("LOMBELLA_G", "Giulia", "Lombella", 'F', "ITA", null, 2004);
				atletaDao.create("LONARDI_A", "Agnese", "Lonardi", 'F', "ITA", null, 2007);
				atletaDao.create("LONZA_F", "Francesca", "Lonza", 'F', "ITA", null, 2004);
				atletaDao.create("MALARA_C", "Carlotta", "Malara", 'F', "ITA", null, 1998);
				atletaDao.create("MANDELLI_A", "Alice", "Mandelli", 'F', "ITA", null, 2000);
				atletaDao.create("MANDIC_N", "Nada", "Mandic", 'F', "SRB", null, 1998);
				atletaDao.create("MANIGRASSO_G", "Giorgia", "Manigrasso", 'F', "ITA", null, 2007);
				atletaDao.create("MARCHETTI_M", "Miriam", "Marchetti", 'F', "ITA", null, 2004);
				atletaDao.create("MARCIALIS_C", "Carolina", "Marcialis", 'F', "ITA", null, 1991);
				atletaDao.create("MARIONI_V", "Vittoria", "Marioni", 'F', "ITA", null, 1993);
				atletaDao.create("MARLETTA_C", "Claudia Roberta", "Marletta", 'F', "ITA", null, 1995);
				atletaDao.create("MARUSSI_G", "Grace ", "Marussi", 'F', "ITA", null, 2004);
				atletaDao.create("MAUCERI_A", "Alessia", "Mauceri", 'F', "ITA", null, 2000);
				atletaDao.create("MEGGIATO_C", "Carlotta", "Meggiato", 'F', "ITA", null, 2000);
				atletaDao.create("MILLO_A", "Alessia", "Millo", 'F', "ITA", null, 1993);
				atletaDao.create("MORI_E", "Emanuela", "Mori", 'F', "ITA", null, 1995);
				atletaDao.create("MOTTA_S", "Silvia", "Motta", 'F', "ITA", null, 1988);
				atletaDao.create("MILLO_G", "Giulia", "Millo", 'F', "ITA", null, 1998);
				atletaDao.create("MUGNAI_B", "Bianca", "Mugnai", 'F', "ITA", null, 2000);
				atletaDao.create("NARDINI_C", "Cecilia", "Nardini", 'F', "ITA", null, 1999);
				atletaDao.create("NENCHA_C", "Carlotta", "Nencha", 'F', "ITA", null, 1997);
				atletaDao.create("NESTI_L", "Letizia", "Nesti", 'F', "ITA", null, 2002);
				atletaDao.create("NIGRO_D", "Divina Lucia", "Nigro", 'F', "ITA", null, 1997);
				atletaDao.create("OBERTI_M", "Matilde", "Oberti", 'F', "ITA", null, 2007);
				atletaDao.create("OLIVIERI_M", "Matilde", "Olivieri", 'F', "ITA", null, 2002);
				atletaDao.create("PAGANELLO_M", "Maddalena", "Paganello", 'F', "ITA", null, 2005);
				atletaDao.create("PALMIERI_V", "Valeria", "Palmieri", 'F', "ITA", null, 1993);
				atletaDao.create("PANTANI_M", "Margherita", "Pantani", 'F', "ITA", null, 2005);
				atletaDao.create("PAPI_L", "Lavinia", "Papi", 'F', "ITA", null, 2005);
				atletaDao.create("PASQUON_G", "Gaia", "Pasquon", 'F', "ITA", null, 2003);
				atletaDao.create("PEREGO_L", "Laura", "Perego", 'F', "ITA", null, 1992);
				atletaDao.create("PERNA_V", "Veronica", "Perna", 'F', "ITA", null, 2000);
				atletaDao.create("PERONI_G", "Giorgia", "Peroni", 'F', "ITA", null, 1992);
				atletaDao.create("PICOZZI_D", "Domitilla", "Picozzi", 'F', "ITA", null, 1998);
				atletaDao.create("PRANDINI_G", "Giorgia", "Prandini", 'F', "ITA", null, 1985);
				atletaDao.create("QUATTRINI_E", "Elisa", "Quattrini", 'F', "ITA", null, 1998);
				atletaDao.create("QUEIROLO_E", "Elisa", "Queirolo", 'F', "ITA", null, 1991);
				atletaDao.create("RANALLI_C", "Chiara", "Ranalli", 'F', "ITA", null, 1997);
				atletaDao.create("RATTELLI_F", "Francesca", "Rattelli", 'F', "ITA", null, 1996);
				atletaDao.create("RAYNER_J", "Johanna Olinda", "Rayner", 'F', "ITA", null, 2004);
				atletaDao.create("REPETTO_A", "Anna", "Repetto", 'F', "ITA", null, 1999);
				atletaDao.create("REPETTO_L", "Laura", "Repetto", 'F', "ITA", null, 1993);
				atletaDao.create("RICCIO_A", "Anastasia", "Riccio", 'F', "ITA", null, 2003);
				atletaDao.create("RICCIOLI_I", "Isabella", "Riccioli", 'F', "ITA", null, 2000);
				atletaDao.create("ROGONDINO_R", "Rosa", "Rogondino", 'F', "ITA", null, 1997);
				atletaDao.create("RORANDELLI_C", "Claudia", "Rorandelli", 'F', "ITA", null, 1995);
				atletaDao.create("ROSTA_B", "Bianca M.", "Rosta", 'F', "ITA", null, 2006);
				atletaDao.create("RUGORA_S", "Silvia ", "Rugora", 'F', "ITA", null, 2004);
				atletaDao.create("RUSSIGNAN_A", "Amanda", "Russignan", 'F', "ITA", null, 2001);
				atletaDao.create("SANTAPAOLA_R", "Roberta", "Santapaola", 'F', "ITA", null, 2000);
				atletaDao.create("SANTINELLI_G", "Giulia", "Santinelli", 'F', "ITA", null, 2001);
				atletaDao.create("SANTINI_M", "Marta", "Santini", 'F', "ITA", null, 2002);
				atletaDao.create("SANTINI_S", "Silvia", "Santini", 'F', "ITA", null, 2001);
				atletaDao.create("SANTORO_V", "Vittoria", "Santoro", 'F', "ITA", null, 2003);
				atletaDao.create("SAVIOLI_I", "Ilaria", "Savioli", 'F', "ITA", null, 1990);
				atletaDao.create("SAVIOLI_M", "Martina", "Savioli", 'F', "ITA", null, 1990);
				atletaDao.create("SBLATTERO_E", "Elisa", "Sblattero", 'F', "ITA", null, 2004);
				atletaDao.create("SBRUZZI_V", "Vittoria", "Sbruzzi", 'F', "ITA", null, 2004);
				atletaDao.create("SGRO_B", "Beatrice", "Sgr�", 'F', "ITA", null, 2004);
				atletaDao.create("SGRO_V", "Valentina", "Sgr�", 'F', "ITA", null, 2006);
				atletaDao.create("SINIGAGLIA_G", "Giada", "Sinigaglia", 'F', "ITA", null, 1995);
				atletaDao.create("SOKHNA_A", "Aminata Alice", "Sokhna", 'F', "ITA", null, 2005);
				atletaDao.create("SORBI_C", "Caterina", "Sorbi", 'F', "ITA", null, 1989);
				atletaDao.create("SPAMPINATO_D", "Dorotea", "Spampinato", 'F', "ITA", null, 2003);
				atletaDao.create("SPARANO_F", "Fabiana", "Sparano", 'F', "ITA", null, 1995);
				atletaDao.create("STANKOVIAN_M", "Miroslava", "Stankovianska", 'F', "SVK", null, 2000);
				atletaDao.create("STORAI_S", "Serena", "Storai", 'F', "ITA", null, 1999);
 				atletaDao.create("STRAPPATO_L", "Lisa", "Strappato", 'F', "ITA", null, 1999);
				atletaDao.create("TABANI_C", "Chiara", "Tabani", 'F', "ITA", null, 1994);
				atletaDao.create("TAMBORRINO_C", "Camilla", "Tamborrino", 'F', "ITA", null, 1998);
				atletaDao.create("TEANI_L", "Laura", "Teani", 'F', "ITA", null, 1991);
				atletaDao.create("TENBROEK_N", "Nina", "Ten Broek", 'F', "NED", null, 2001);
				atletaDao.create("TORI_C", "Chiara", "Tori", 'F', "ITA", null, 2000);
				atletaDao.create("TRONCANETT_F", "Federica", "Troncanetti", 'F', "ITA", null, 2002);
				atletaDao.create("UCCELLA_V", "Valeria", "Uccella", 'F', "ITA", null, 2001);
				atletaDao.create("VECCHIAREL_E", "Elena", "Vecchiarelli", 'F', "ITA", null, 2002);
				atletaDao.create("VIACAVA_G", "Giulia", "Viacava", 'F', "ITA", null, 1994);
				atletaDao.create("VITTORI_N", "Noura", "Vittori", 'F', "ITA", null, 1998);
				atletaDao.create("VUKOVIC_J", "Jelena", "Vukovic", 'F', "MNG", null, 1994);
				atletaDao.create("WHITELEGGE_R", "Rachel Taraina", "Whitelegge", 'F', "USA", null, 1997);
				atletaDao.create("ZADEU_G", "Giorgia", "Zadeu", 'F', "ITA", null, 2000);
				atletaDao.create("ZANDALI_C", "Carlotta", "Zandali", 'F', "ITA", null, 2002);
				atletaDao.create("ZANETTA_N", "Nicole", "Zanetta", 'F', "ITA", null, 1999);
				atletaDao.create("ZANOCCOLI_G", "Giada", "Zanoccoli", 'F', "ITA", null, 2005);

				sessione.getTransaction().commit();
			
				// 06 - ASSOCIAZIONE TECNICI ALLE SQUADRE
				
				sessione.beginTransaction();		

				sqANV.addTecnico(tecnicoDao.findById("PACE_M"));
				sqANV = squadraDao.update(sqANV);		
				sqBG5.addTecnico(tecnicoDao.findById("SINATRA_M"));
				sqBG5 = squadraDao.update(sqBG5);				
				sqCTE.addTecnico(tecnicoDao.findById("MICELI_M"));
				sqCTE = squadraDao.update(sqCTE);				
				sqFLO.addTecnico(tecnicoDao.findById("COTTI_A"));
				sqFLO = squadraDao.update(sqFLO);
				sqMIN.addTecnico(tecnicoDao.findById("BINCHI_L"));
				sqMIN = squadraDao.update(sqMIN);
				sqPDP.addTecnico(tecnicoDao.findById("POSTERIVO_S"));
				sqPDP = squadraDao.update(sqPDP);				
				sqRMS.addTecnico(tecnicoDao.findById("CAPANNA_M"));
				sqRMS = squadraDao.update(sqRMS);
				sqRPP.addTecnico(tecnicoDao.findById("ANTONUCCI_L"));
				sqRPP = squadraDao.update(sqRPP);				
				sqTSP.addTecnico(tecnicoDao.findById("COLAUTTI_I"));
				sqTSP.addTecnico(tecnicoDao.findById("MARINELLI_U"));
				sqTSP = squadraDao.update(sqTSP);				
				sqVRC.addTecnico(tecnicoDao.findById("ZACCARIA_G"));
				sqVRC.addTecnico(tecnicoDao.findById("ZIZZA_P"));
				sqVRC = squadraDao.update(sqVRC);

				sessione.getTransaction().commit();

				// 07 - ASSOCIAZIONE ATLETI ALLE SQUADRE
				
				sessione.beginTransaction();		

				sqANV.addAtleta(atletaDao.findById("ALTAMURA_E"));
				sqANV.addAtleta(atletaDao.findById("ANDREONI_A"));
				sqANV.addAtleta(atletaDao.findById("BARTOCCI_C"));
				sqANV.addAtleta(atletaDao.findById("BERSACCHIA_G"));
				sqANV.addAtleta(atletaDao.findById("BORGHETTI_J"));
				sqANV.addAtleta(atletaDao.findById("COLLETTA_F"));
				sqANV.addAtleta(atletaDao.findById("DEMATTEIS_V"));
				sqANV.addAtleta(atletaDao.findById("DIMARTINO_T"));
				sqANV.addAtleta(atletaDao.findById("FERRETTI_M"));		
				sqANV.addAtleta(atletaDao.findById("FIORE_G"));
				sqANV.addAtleta(atletaDao.findById("IVANOVA_E"));
				sqANV.addAtleta(atletaDao.findById("OLIVIERI_M"));
				sqANV.addAtleta(atletaDao.findById("QUATTRINI_E"));
				sqANV.addAtleta(atletaDao.findById("RICCIO_A"));
				sqANV.addAtleta(atletaDao.findById("SANTINI_M"));
				sqANV.addAtleta(atletaDao.findById("SANTINI_S"));
				sqANV.addAtleta(atletaDao.findById("STANKOVIAN_M"));
				sqANV.addAtleta(atletaDao.findById("STRAPPATO_L"));
				sqANV.addAtleta(atletaDao.findById("UCCELLA_V"));
				sqANV.addAtleta(atletaDao.findById("VECCHIAREL_E"));
				sqANV = squadraDao.update(sqANV);	
		
				sqBG5.addAtleta(atletaDao.findById("ALBASINI_A"));
				sqBG5.addAtleta(atletaDao.findById("AMEDEO_G"));
				sqBG5.addAtleta(atletaDao.findById("BARBIERI_D"));
				sqBG5.addAtleta(atletaDao.findById("CARPANETO_M"));
				sqBG5.addAtleta(atletaDao.findById("CAVALLINI_G"));
				sqBG5.addAtleta(atletaDao.findById("COCCHIERE_T"));
				sqBG5.addAtleta(atletaDao.findById("CRISCUOLO_C"));
				sqBG5.addAtleta(atletaDao.findById("CRISCUOLO_S"));
				sqBG5.addAtleta(atletaDao.findById("CROVETTO_M"));
				sqBG5.addAtleta(atletaDao.findById("CUZZUPE_G"));
				sqBG5.addAtleta(atletaDao.findById("DEMARCH_E"));
				sqBG5.addAtleta(atletaDao.findById("DESERTI_S"));
				sqBG5.addAtleta(atletaDao.findById("FALCONI_E"));
				sqBG5.addAtleta(atletaDao.findById("FRANCI_V"));
				sqBG5.addAtleta(atletaDao.findById("ISETTA_L"));
				sqBG5.addAtleta(atletaDao.findById("LOMBELLA_G"));
				sqBG5.addAtleta(atletaDao.findById("MALARA_C"));
				sqBG5.addAtleta(atletaDao.findById("MAUCERI_A"));
				sqBG5.addAtleta(atletaDao.findById("MILLO_G"));
				sqBG5.addAtleta(atletaDao.findById("MORI_E"));
				sqBG5.addAtleta(atletaDao.findById("OBERTI_M"));
				sqBG5.addAtleta(atletaDao.findById("PAGANELLO_M"));
				sqBG5.addAtleta(atletaDao.findById("RAYNER_J"));
				sqBG5.addAtleta(atletaDao.findById("ROGONDINO_R"));
				sqBG5.addAtleta(atletaDao.findById("ROSTA_B"));
				sqBG5.addAtleta(atletaDao.findById("SANTINELLI_G"));
				sqBG5.addAtleta(atletaDao.findById("SOKHNA_A"));
				sqBG5.addAtleta(atletaDao.findById("WHITELEGGE_R"));
				sqBG5 = squadraDao.update(sqBG5);
				
				sqCTE.addAtleta(atletaDao.findById("AIELLO_R"));
				sqCTE.addAtleta(atletaDao.findById("BARZON_L"));
				sqCTE.addAtleta(atletaDao.findById("BEKHAZI_J"));
				sqCTE.addAtleta(atletaDao.findById("CASABIANCA_M"));
				sqCTE.addAtleta(atletaDao.findById("CONDORELLI_G"));
				sqCTE.addAtleta(atletaDao.findById("EMMOLO_G"));
				sqCTE.addAtleta(atletaDao.findById("GARIBOTTI_A"));
				sqCTE.addAtleta(atletaDao.findById("GIUFFRIDA_M"));
				sqCTE.addAtleta(atletaDao.findById("GORLERO_G"));
				sqCTE.addAtleta(atletaDao.findById("IOANNOU_C"));
				sqCTE.addAtleta(atletaDao.findById("LEONE_M"));
				sqCTE.addAtleta(atletaDao.findById("MARLETTA_C"));
				sqCTE.addAtleta(atletaDao.findById("PALMIERI_V"));
				sqCTE.addAtleta(atletaDao.findById("RICCIOLI_I"));
				sqCTE.addAtleta(atletaDao.findById("SANTAPAOLA_R"));
				sqCTE.addAtleta(atletaDao.findById("SANTORO_V"));
				sqCTE.addAtleta(atletaDao.findById("SPAMPINATO_D"));
				sqCTE.addAtleta(atletaDao.findById("VIACAVA_G"));
				sqCTE.addAtleta(atletaDao.findById("VUKOVIC_J"));
				sqCTE = squadraDao.update(sqCTE);
				
				sqFLO.addAtleta(atletaDao.findById("AMEDEO_G"));
				sqFLO.addAtleta(atletaDao.findById("BANCHELLI_C"));
				sqFLO.addAtleta(atletaDao.findById("CARNESECCH_A"));
				sqFLO.addAtleta(atletaDao.findById("CORDOVANI_S"));
				sqFLO.addAtleta(atletaDao.findById("FARAGO_B"));
				sqFLO.addAtleta(atletaDao.findById("FRANCINI_R"));
				sqFLO.addAtleta(atletaDao.findById("GASPARRI_G"));
				sqFLO.addAtleta(atletaDao.findById("GIACHI_G"));
				sqFLO.addAtleta(atletaDao.findById("GUERCI_G"));
				sqFLO.addAtleta(atletaDao.findById("LANDI_I"));
				sqFLO.addAtleta(atletaDao.findById("LEPORE_L"));
				sqFLO.addAtleta(atletaDao.findById("MANDELLI_A"));
				sqFLO.addAtleta(atletaDao.findById("MARIONI_V"));
				sqFLO.addAtleta(atletaDao.findById("MUGNAI_B"));
				sqFLO.addAtleta(atletaDao.findById("NENCHA_C"));
				sqFLO.addAtleta(atletaDao.findById("NESTI_L"));
				sqFLO.addAtleta(atletaDao.findById("PANTANI_M"));
				sqFLO.addAtleta(atletaDao.findById("PEREGO_L"));
				sqFLO.addAtleta(atletaDao.findById("RORANDELLI_C"));
				sqFLO.addAtleta(atletaDao.findById("SORBI_C"));
				sqFLO.addAtleta(atletaDao.findById("TENBROEK_N"));
				sqFLO.addAtleta(atletaDao.findById("VITTORI_N"));
				sqFLO = squadraDao.update(sqFLO);
	
				sqMIN.addAtleta(atletaDao.findById("APILONGO_G"));
				sqMIN.addAtleta(atletaDao.findById("BIANCONI_R"));
				sqMIN.addAtleta(atletaDao.findById("CARREGA_C"));
				sqMIN.addAtleta(atletaDao.findById("CRUDELE_G"));
				sqMIN.addAtleta(atletaDao.findById("DEVINCENTI_F"));
				sqMIN.addAtleta(atletaDao.findById("GRAGNOLATI_A"));
				sqMIN.addAtleta(atletaDao.findById("IMPERATRIC_M"));
				sqMIN.addAtleta(atletaDao.findById("MANDIC_N"));
				sqMIN.addAtleta(atletaDao.findById("REPETTO_A"));
				sqMIN.addAtleta(atletaDao.findById("REPETTO_L"));
				sqMIN.addAtleta(atletaDao.findById("TAMBORRINO_C"));
				sqMIN.addAtleta(atletaDao.findById("VUKOVIC_J"));
				sqMIN.addAtleta(atletaDao.findById("ZANDALI_C"));
				sqMIN = squadraDao.update(sqMIN);	
	
				sqPDP.addAtleta(atletaDao.findById("AGOSTA_E"));
				sqPDP.addAtleta(atletaDao.findById("CARDILLO_A"));
				sqPDP.addAtleta(atletaDao.findById("CASSON_A"));
				sqPDP.addAtleta(atletaDao.findById("CENTANNI_S"));
				sqPDP.addAtleta(atletaDao.findById("COCCHIERE_A"));
				sqPDP.addAtleta(atletaDao.findById("DARIO_S"));
				sqPDP.addAtleta(atletaDao.findById("GIACON_C"));
				sqPDP.addAtleta(atletaDao.findById("GOTTARDO_M"));
				sqPDP.addAtleta(atletaDao.findById("MEGGIATO_C"));
				sqPDP.addAtleta(atletaDao.findById("MILLO_A"));
				sqPDP.addAtleta(atletaDao.findById("QUEIROLO_E"));
				sqPDP.addAtleta(atletaDao.findById("RANALLI_C"));
				sqPDP.addAtleta(atletaDao.findById("SAVIOLI_I"));
				sqPDP.addAtleta(atletaDao.findById("SAVIOLI_M"));
				sqPDP.addAtleta(atletaDao.findById("TEANI_L"));
				sqPDP = squadraDao.update(sqPDP);
				
				sqRMS.addAtleta(atletaDao.findById("AMBROSINI_S"));
				sqRMS.addAtleta(atletaDao.findById("AVEGNO_S"));
				sqRMS.addAtleta(atletaDao.findById("BIANCHI_A"));
				sqRMS.addAtleta(atletaDao.findById("BRANDIMART_M"));
				sqRMS.addAtleta(atletaDao.findById("CHIAPPINI_I"));
				sqRMS.addAtleta(atletaDao.findById("DICLAUDIO_L"));
				sqRMS.addAtleta(atletaDao.findById("GALARDI_G"));
				sqRMS.addAtleta(atletaDao.findById("GIUSTINI_S"));
				sqRMS.addAtleta(atletaDao.findById("IANNARELLI_A"));
				sqRMS.addAtleta(atletaDao.findById("MOTTA_S"));
				sqRMS.addAtleta(atletaDao.findById("NARDINI_C"));
				sqRMS.addAtleta(atletaDao.findById("PAPI_L"));
				sqRMS.addAtleta(atletaDao.findById("PICOZZI_D"));
				sqRMS.addAtleta(atletaDao.findById("RUGORA_S"));
				sqRMS.addAtleta(atletaDao.findById("SINIGAGLIA_G"));
				sqRMS.addAtleta(atletaDao.findById("SPARANO_F"));
				sqRMS.addAtleta(atletaDao.findById("STORAI_S"));
				sqRMS.addAtleta(atletaDao.findById("TABANI_C"));
				sqRMS.addAtleta(atletaDao.findById("TORI_C"));
				sqRMS.addAtleta(atletaDao.findById("TRONCANETT_F"));
				sqRMS = squadraDao.update(sqRMS);

				sqRPP.addAtleta(atletaDao.findById("BIANCO_E"));
				sqRPP.addAtleta(atletaDao.findById("BUJKA_B"));
				sqRPP.addAtleta(atletaDao.findById("CABONA_B"));
				sqRPP.addAtleta(atletaDao.findById("COLLETTA_F"));
				sqRPP.addAtleta(atletaDao.findById("DAMICO_A"));
				sqRPP.addAtleta(atletaDao.findById("FALCONI_C"));
				sqRPP.addAtleta(atletaDao.findById("FORESTA_C"));
				sqRPP.addAtleta(atletaDao.findById("GAGLIARDI_G"));
				sqRPP.addAtleta(atletaDao.findById("GITTO_U"));
				sqRPP.addAtleta(atletaDao.findById("GIUSTINI_S"));
				sqRPP.addAtleta(atletaDao.findById("KUZINA_S"));
				sqRPP.addAtleta(atletaDao.findById("MARCIALIS_C"));
				sqRPP.addAtleta(atletaDao.findById("ZANETTA_N"));
				sqRPP = squadraDao.update(sqRPP);
				
				sqTSP.addAtleta(atletaDao.findById("BENATI_M"));
				sqTSP.addAtleta(atletaDao.findById("BETTINI_D"));
				sqTSP.addAtleta(atletaDao.findById("BOERO_V"));
				sqTSP.addAtleta(atletaDao.findById("BOZZETTA_B"));
				sqTSP.addAtleta(atletaDao.findById("CERGOL_L"));
				sqTSP.addAtleta(atletaDao.findById("GAGLIARDI_G"));
				sqTSP.addAtleta(atletaDao.findById("GANT_V"));
				sqTSP.addAtleta(atletaDao.findById("GREGORUTTI_G"));
				sqTSP.addAtleta(atletaDao.findById("INGANNAMOR_E"));
				sqTSP.addAtleta(atletaDao.findById("INGANNAMOR_S"));
				sqTSP.addAtleta(atletaDao.findById("JANKOVIC_A"));
				sqTSP.addAtleta(atletaDao.findById("KEMPF_P"));
				sqTSP.addAtleta(atletaDao.findById("KLATOWSKI_G"));
				sqTSP.addAtleta(atletaDao.findById("KRASTI_G"));
				sqTSP.addAtleta(atletaDao.findById("LONZA_F"));
				sqTSP.addAtleta(atletaDao.findById("MARUSSI_G"));
				sqTSP.addAtleta(atletaDao.findById("PASQUON_G"));
				sqTSP.addAtleta(atletaDao.findById("RATTELLI_F"));
				sqTSP.addAtleta(atletaDao.findById("RUSSIGNAN_A"));
				sqTSP.addAtleta(atletaDao.findById("SBLATTERO_E"));
				sqTSP.addAtleta(atletaDao.findById("ZADEU_G"));
				sqTSP = squadraDao.update(sqTSP);
				
				sqVRC.addAtleta(atletaDao.findById("ALOGBO_K"));
				sqVRC.addAtleta(atletaDao.findById("BARTOLINI_G"));
				sqVRC.addAtleta(atletaDao.findById("BIANCONI_R"));
				sqVRC.addAtleta(atletaDao.findById("BORG_E"));
				sqVRC.addAtleta(atletaDao.findById("BRAGA_I"));
				sqVRC.addAtleta(atletaDao.findById("CAROTENUTO_G"));
				sqVRC.addAtleta(atletaDao.findById("CASTAGNINI_E"));
				sqVRC.addAtleta(atletaDao.findById("DONADIO_G"));
				sqVRC.addAtleta(atletaDao.findById("ESPOSITO_A"));
				sqVRC.addAtleta(atletaDao.findById("GABUSI_G"));
				sqVRC.addAtleta(atletaDao.findById("GRAGNOLATI_A"));
				sqVRC.addAtleta(atletaDao.findById("LONARDI_A"));
				sqVRC.addAtleta(atletaDao.findById("MANIGRASSO_G"));
				sqVRC.addAtleta(atletaDao.findById("MARCHETTI_M"));
				sqVRC.addAtleta(atletaDao.findById("MARCIALIS_C"));
				sqVRC.addAtleta(atletaDao.findById("NIGRO_D"));
				sqVRC.addAtleta(atletaDao.findById("PERNA_V"));
				sqVRC.addAtleta(atletaDao.findById("PERONI_G"));
				sqVRC.addAtleta(atletaDao.findById("PRANDINI_G"));
				sqVRC.addAtleta(atletaDao.findById("SBRUZZI_V"));
				sqVRC.addAtleta(atletaDao.findById("SGRO_B"));
				sqVRC.addAtleta(atletaDao.findById("SGRO_V"));
				sqVRC.addAtleta(atletaDao.findById("ZANETTA_N"));
				sqVRC.addAtleta(atletaDao.findById("ZANOCCOLI_G"));
				sqVRC = squadraDao.update(sqVRC);

				sessione.getTransaction().commit();

				// 08 - CARICAMENTO PARTITE NELLA BASE DATI
				
				sessione.beginTransaction();				
				
				DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, Locale.ITALY);

				// formattazione date di calendario del campionato di serie A1

				Date s20giornata01 = df.parse("5/10/2019");
				Date s20giornata02 = df.parse("17/10/2019");
				Date s20giornata03 = df.parse("19/10/2019");
				Date s20giornata04 = df.parse("23/10/2019");
				Date s20giornata05 = df.parse("2/11/2019");
				Date s20giornata06 = df.parse("9/11/2019");
				Date s20giornata07 = df.parse("16/11/2019");
				Date s20giornata08 = df.parse("23/11/2019");
				Date s20giornata09 = df.parse("1/2/2020");
				Date s20giornata10 = df.parse("8/2/2020");

				Date s21giornata01 = df.parse("10/10/2020");
				Date s21giornata02 = df.parse("17/10/2020");
				Date s21giornata03 = df.parse("24/10/2020");
				Date s21giornata04 = df.parse("2/2/2021");
				Date s21giornata05 = df.parse("13/2/2021");
				Date s21giornata06 = df.parse("20/2/2021");
				Date s21giornata07 = df.parse("6/3/2021");
				Date s21giornata08 = df.parse("20/3/2021");
				Date s21giornata09 = df.parse("15/5/2021");
				Date s21giornata10 = df.parse("17/4/2021");
				Date s21giornata11 = df.parse("24/4/2021");
				Date s21giornata12 = df.parse("8/5/2021");

				// creazione delle partite della Vela Nuoto Ancona

				Partita s20partita01 = partitaDao.create(s20giornata01, "Serie A1 2019-20, girone", sqANV, sqFLO);
				Partita s20partita02 = partitaDao.create(s20giornata02, "Serie A1 2019-20, girone", sqVRC, sqANV);
				Partita s20partita03 = partitaDao.create(s20giornata03, "Serie A1 2019-20, girone", sqPDP, sqANV);
				Partita s20partita04 = partitaDao.create(s20giornata04, "Serie A1 2019-20, girone", sqANV, sqRMS);
				Partita s20partita05 = partitaDao.create(s20giornata05, "Serie A1 2019-20, girone", sqCTE, sqANV);
				Partita s20partita06 = partitaDao.create(s20giornata06, "Serie A1 2019-20, girone", sqANV, sqBG5);
				Partita s20partita07 = partitaDao.create(s20giornata07, "Serie A1 2019-20, girone", sqMIN, sqANV);
				Partita s20partita08 = partitaDao.create(s20giornata08, "Serie A1 2019-20, girone", sqANV, sqTSP);
				Partita s20partita09 = partitaDao.create(s20giornata09, "Serie A1 2019-20, girone", sqRPP, sqANV);
				Partita s20partita10 = partitaDao.create(s20giornata10, "Serie A1 2019-20, girone", sqFLO, sqANV);

				Partita s21partita01 = partitaDao.create(s21giornata01, "Serie A1 2020-21, girone B", sqCTE, sqANV);
				Partita s21partita02 = partitaDao.create(s21giornata02, "Serie A1 2020-21, girone B", sqANV, sqRMS);
				Partita s21partita03 = partitaDao.create(s21giornata03, "Serie A1 2020-21, girone B", sqANV, sqFLO);
				Partita s21partita04 = partitaDao.create(s21giornata04, "Serie A1 2020-21, girone B", sqANV, sqCTE);
				Partita s21partita05 = partitaDao.create(s21giornata05, "Serie A1 2020-21, girone B", sqRMS, sqANV);
				Partita s21partita06 = partitaDao.create(s21giornata06, "Serie A1 2020-21, girone B", sqFLO, sqANV);
				Partita s21partita07 = partitaDao.create(s21giornata07, "Serie A1 2020-21, final round", sqBG5, sqANV);
				Partita s21partita08 = partitaDao.create(s21giornata08, "Serie A1 2020-21, final round", sqANV, sqFLO);
				Partita s21partita09 = partitaDao.create(s21giornata09, "Serie A1 2020-21, final round", sqTSP, sqANV);
				Partita s21partita10 = partitaDao.create(s21giornata10, "Serie A1 2020-21, final round", sqANV, sqBG5);
				Partita s21partita11 = partitaDao.create(s21giornata11, "Serie A1 2020-21, final round", sqFLO, sqANV);
				Partita s21partita12 = partitaDao.create(s21giornata12, "Serie A1 2020-21, final round", sqANV, sqTSP);
				
				// piscine in cui si disputano le partite della Vela Nuoto Ancona

				s20partita01.setPiscina(piscinaDao.findById("Passetto"));
				s20partita02.setPiscina(piscinaDao.findById("MonteBianco"));
				s20partita03.setPiscina(piscinaDao.findById("Plebiscito"));
				s20partita04.setPiscina(piscinaDao.findById("Passetto"));
				s20partita05.setPiscina(piscinaDao.findById("Nesima"));
				s20partita06.setPiscina(piscinaDao.findById("Passetto"));
				s20partita07.setPiscina(piscinaDao.findById("Faustina"));
				s20partita08.setPiscina(piscinaDao.findById("Passetto"));
				s20partita09.setPiscina(piscinaDao.findById("Rapallo"));
				s20partita10.setPiscina(piscinaDao.findById("Nannini"));

				s21partita01.setPiscina(piscinaDao.findById("Scuderi"));
				s21partita02.setPiscina(piscinaDao.findById("Passetto"));
				s21partita03.setPiscina(piscinaDao.findById("Passetto"));
				s21partita04.setPiscina(piscinaDao.findById("Passetto"));
				s21partita05.setPiscina(piscinaDao.findById("ForoItalico"));
				s21partita06.setPiscina(piscinaDao.findById("Nannini"));
				s21partita07.setPiscina(piscinaDao.findById("Vassallo"));
				s21partita08.setPiscina(piscinaDao.findById("Passetto"));
				s21partita09.setPiscina(piscinaDao.findById("Bianchi"));
				s21partita10.setPiscina(piscinaDao.findById("Passetto"));
				s21partita11.setPiscina(piscinaDao.findById("Nannini"));
				s21partita12.setPiscina(piscinaDao.findById("Passetto"));
				
				// arbitri designati per le partite della Vela Nuoto Ancona

				s20partita01.setArbitro1(arbitroDao.findById("CASTAGNOLA_L"));
				s20partita01.setArbitro2(arbitroDao.findById("GUARRACINO_A"));
				s20partita02.setArbitro1(arbitroDao.findById("PIANO_M"));
				s20partita02.setArbitro2(arbitroDao.findById("FERRARI_A"));
				s20partita03.setArbitro1(arbitroDao.findById("FRAUENFELD_V"));
				s20partita03.setArbitro2(arbitroDao.findById("COLOMBO_R"));
				s20partita04.setArbitro1(arbitroDao.findById("CASTAGNOLA_L"));
				s20partita04.setArbitro2(arbitroDao.findById("PASCUCCI_A"));
				s20partita05.setArbitro1(arbitroDao.findById("SCHIAVO_M"));
				s20partita05.setArbitro2(arbitroDao.findById("BRAGHINI_F"));
				s20partita06.setArbitro1(arbitroDao.findById("SEVERO_A"));
				s20partita06.setArbitro2(arbitroDao.findById("RICCIOTTI_F"));
				s20partita07.setArbitro1(arbitroDao.findById("SCHIAVO_M"));
				s20partita07.setArbitro2(arbitroDao.findById("NICOLOSI_G"));
				s20partita08.setArbitro1(arbitroDao.findById("BRAGHINI_F"));
				s20partita08.setArbitro2(arbitroDao.findById("NAVARRA_B"));
				s20partita09.setArbitro1(arbitroDao.findById("BRAGHINI_F"));
				s20partita09.setArbitro2(arbitroDao.findById("ROMOLINI_F"));
				s20partita10.setArbitro1(arbitroDao.findById("CENTINEO_G"));
				s20partita10.setArbitro2(arbitroDao.findById("SCHIAVO_M"));

				s21partita01.setArbitro1(arbitroDao.findById("CARMIGNANI_R"));
				s21partita01.setArbitro2(arbitroDao.findById("DANTONI_R"));
				s21partita02.setArbitro1(arbitroDao.findById("CASTAGNOLA_L"));
				s21partita02.setArbitro2(arbitroDao.findById("ERCOLI_M"));
				s21partita03.setArbitro1(arbitroDao.findById("NICOLOSI_G"));
				s21partita03.setArbitro2(arbitroDao.findById("PETRONILLI_A"));
				s21partita04.setArbitro1(arbitroDao.findById("NAVARRA_B"));
				s21partita04.setArbitro2(arbitroDao.findById("BARLETTA_L"));
				s21partita05.setArbitro1(arbitroDao.findById("PETRONILLI_A"));
				s21partita05.setArbitro2(arbitroDao.findById("NICOLOSI_G"));
				s21partita06.setArbitro1(arbitroDao.findById("SEVERO_A"));
				s21partita06.setArbitro2(arbitroDao.findById("RICCIOTTI_F"));
				s21partita07.setArbitro1(arbitroDao.findById("TACCINI_C"));
				s21partita07.setArbitro2(arbitroDao.findById("FUSCO_G"));
				s21partita08.setArbitro1(arbitroDao.findById("SCAPPINI_S"));
				s21partita08.setArbitro2(arbitroDao.findById("BRAGHINI_F"));
				s21partita09.setArbitro1(arbitroDao.findById("FUSCO_G"));
				s21partita09.setArbitro2(arbitroDao.findById("BENSAIA_P"));
				s21partita10.setArbitro1(arbitroDao.findById("RICCIOTTI_F"));
				s21partita10.setArbitro2(arbitroDao.findById("SCILLATO_D"));
				s21partita11.setArbitro1(arbitroDao.findById("COLLANTONI_F"));
				s21partita11.setArbitro2(arbitroDao.findById("SCAPPINI_S"));
				s21partita12.setArbitro1(arbitroDao.findById("BARLETTA_L"));
				s21partita12.setArbitro2(arbitroDao.findById("ALFI_S"));

				// risultati nelle partite della Vela Nuoto Ancona

				s20partita01.setEsitoPartita("T");
				s20partita01.setRetiBianco(5);	s20partita01.setRetiNero(7);
				s20partita02.setEsitoPartita("T");
				s20partita02.setRetiBianco(10);	s20partita02.setRetiNero(3);
				s20partita03.setEsitoPartita("T");
				s20partita03.setRetiBianco(21);	s20partita03.setRetiNero(5);
				s20partita04.setEsitoPartita("T");
				s20partita04.setRetiBianco(5);	s20partita04.setRetiNero(14);
				s20partita05.setEsitoPartita("T");
				s20partita05.setRetiBianco(21);	s20partita05.setRetiNero(1);
				s20partita06.setEsitoPartita("T");
				s20partita06.setRetiBianco(10);	s20partita06.setRetiNero(11);
				s20partita07.setEsitoPartita("T");
				s20partita07.setRetiBianco(20);	s20partita07.setRetiNero(14);
				s20partita08.setEsitoPartita("T");
				s20partita08.setRetiBianco(8);	s20partita08.setRetiNero(9);
				s20partita09.setEsitoPartita("T");
				s20partita09.setRetiBianco(21);	s20partita09.setRetiNero(7);
				s20partita10.setEsitoPartita("T");
				s20partita10.setRetiBianco(11);	s20partita10.setRetiNero(8);

				s21partita01.setEsitoPartita("T");
				s21partita01.setRetiBianco(26);	s21partita01.setRetiNero(9);
				s21partita01.setParzialiBianco(new int[] {4,6,7,9});
				s21partita01.setParzialiNero(new int[] {3,2,1,3});
				s21partita01.setSupNumBianco(new int[] {9,10});
				s21partita01.setSupNumNero(new int[] {3,5});

				s21partita02.setEsitoPartita("T");
				s21partita02.setRetiBianco(9);
				s21partita02.setRetiNero(16);
				s21partita02.setParzialiBianco(new int[] {2,3,2,2});
				s21partita02.setParzialiNero(new int[] {5,2,5,4});
				s21partita02.setSupNumBianco(new int[] {0,5});
				s21partita02.setSupNumNero(new int[] {5,11});

				s21partita03.setEsitoPartita("T");
				s21partita03.setRetiBianco(9);
				s21partita03.setRetiNero(12);
				s21partita03.setParzialiBianco(new int[] {4,1,2,2});
				s21partita03.setParzialiNero(new int[] {1,5,4,2});
				s21partita03.setSupNumBianco(new int[] {4,8});
				s21partita03.setSupNumNero(new int[] {2,7});

				s21partita04.setEsitoPartita("T");
				s21partita04.setRetiBianco(3);
				s21partita04.setRetiNero(27);
				s21partita04.setParzialiBianco(new int[] {0,0,2,1});
				s21partita04.setParzialiNero(new int[] {8,7,6,6});
				s21partita04.setSupNumBianco(new int[] {0,2});
				s21partita04.setSupNumNero(new int[] {4,9});

				s21partita05.setEsitoPartita("T");
				s21partita05.setRetiBianco(16);
				s21partita05.setRetiNero(4);
				s21partita05.setParzialiBianco(new int[] {2,6,3,5});
				s21partita05.setParzialiNero(new int[] {0,1,1,2});
				s21partita05.setSupNumBianco(new int[] {4,11});
				s21partita05.setSupNumNero(new int[] {0,6});

				s21partita06.setEsitoPartita("T");
				s21partita06.setRetiBianco(4);
				s21partita06.setRetiNero(6);
				s21partita06.setParzialiBianco(new int[] {0,2,1,1});
				s21partita06.setParzialiNero(new int[] {0,2,1,3});
				s21partita06.setSupNumBianco(new int[] {2,7});
				s21partita06.setSupNumNero(new int[] {1,3});

				s21partita07.setEsitoPartita("F");
				s21partita07.setRetiBianco(0);
				s21partita07.setRetiNero(0);
				s21partita07.setParzialiBianco(new int[] {0,0,0,0});
				s21partita07.setParzialiNero(new int[] {0,0,0,0});
				s21partita07.setSupNumBianco(new int[] {0,0});
				s21partita07.setSupNumNero(new int[] {0,0});
				
				s21partita08.setEsitoPartita("F");
				s21partita08.setRetiBianco(0);
				s21partita08.setRetiNero(0);
				s21partita08.setParzialiBianco(new int[] {0,0,0,0});
				s21partita08.setParzialiNero(new int[] {0,0,0,0});
				s21partita08.setSupNumBianco(new int[] {0,0});
				s21partita08.setSupNumNero(new int[] {0,0});
				
				s21partita09.setEsitoPartita("F");
				s21partita09.setRetiBianco(0);
				s21partita09.setRetiNero(0);
				s21partita09.setParzialiBianco(new int[] {0,0,0,0});
				s21partita09.setParzialiNero(new int[] {0,0,0,0});
				s21partita09.setSupNumBianco(new int[] {0,0});
				s21partita09.setSupNumNero(new int[] {0,0});
				
				s21partita10.setEsitoPartita("C");
				s21partita11.setEsitoPartita("C");
				s21partita12.setEsitoPartita("C");

				sessione.getTransaction().commit();

			} catch (Exception exc) {
				exc.printStackTrace(System.err);
			}
		
		} catch (Exception exc) {
			exc.printStackTrace(System.err);
		}
	
	}

}

