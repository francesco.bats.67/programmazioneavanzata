package wp.test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import wp.app.DataServiceConfig;
import wp.model.dao.AtletaDao;
import wp.model.dao.AzioneDao;
import wp.model.dao.PartitaDao;
import wp.model.dao.SquadraDao;
import wp.model.dao.TabellinoDao;
import wp.model.dao.TecnicoDao;
import wp.model.entities.Atleta;
import wp.model.entities.Partita;
import wp.model.entities.Squadra;
import wp.model.entities.Tabellino;
import wp.model.entities.Tecnico;

/**
 * INIZIALIZZA BASE DATI VELAWP CARICA SCORE
 * Classe per caricare i tabellini delle partite di pallanuoto femminile Serie A1:
 *    16.  20/02/2021	FLORENTIA - VELA                   4 -  6   (TABELLINO COMPLETO E AZIONI)
 *    15.  13/02/2021	SIS ROMA - VELA                   16 -  4   (SOLO REPORT)
 *    14.  02/02/2021	VELA - EKIPE ORIZZONTE CATANIA	   3 - 27   (SOLO REPORT)
 *    13.  05/12/2020	VELA - FLORENTIA                   9 - 12   (SOLO REPORT)
 *    12.  17/10/2020	VELA - SIS ROMA                    9 - 16   (SOLO REPORT)
 *    11.  10/10/2020	EKIPE ORIZZONTE CATANIA - VELA    26 -  9   (SOLO REPORT)
 * @author Francesco Battistelli - UNIVPM
 * @version 30 agosto 2021
 */
public class InitDBWP_CaricaScore {

	public static void main(String[] args) {

		// crea oggetti che vanno ad utilizzare servizi e DAO
		
		// istanzia un contesto partendo dalla classe di configurazione per il test
		try (AnnotationConfigApplicationContext contesto =
				new AnnotationConfigApplicationContext(DataServiceConfig.class)) {
			
			// chiede al contesto un riferimento alla session factory
			SessionFactory sf = contesto.getBean("sessionFactory", SessionFactory.class);

			// chiede al contesto un riferimento ai DAO
			AtletaDao atletaDao = contesto.getBean("DaoAtleta", AtletaDao.class);
			AzioneDao azioneDao = contesto.getBean("DaoAzione", AzioneDao.class);
			PartitaDao partitaDao = contesto.getBean("DaoPartita", PartitaDao.class);
			SquadraDao squadraDao = contesto.getBean("DaoSquadra", SquadraDao.class);
			TecnicoDao tecnicoDao = contesto.getBean("DaoTecnico", TecnicoDao.class);
			TabellinoDao tabellinoDao = contesto.getBean("DaoTabellino", TabellinoDao.class);

			// apre una nuova sessione
			try (Session sessione = sf.openSession()) {
				
				// inietta in tutti i DAO la stessa nuova sessione
				atletaDao.setSession(sessione);
				azioneDao.setSession(sessione);
				partitaDao.setSession(sessione);
				squadraDao.setSession(sessione);
				tecnicoDao.setSession(sessione);
				tabellinoDao.setSession(sessione);

				// dichiarazione e inizializzazione delle variabili

				Partita partita;
				Squadra SB;
				Squadra SN;
				Atleta[] AB = { null, null, null, null, null, null, null, null, null, null, null, null, null, null };
				Atleta[] AN = { null, null, null, null, null, null, null, null, null, null, null, null, null, null };
				Tabellino[] TB = { null, null, null, null, null, null, null, null, null, null, null, null, null, null };
				Tabellino[] TN = { null, null, null, null, null, null, null, null, null, null, null, null, null, null };

				boolean in = false;
				float min = 0;
				int rs = 0;
				int tf = 0;
				int fp = 0;
				boolean es = false;
				int[] sRe = {0,0,0,0,0,0};	
				int[] sTi = {0,0,0,0,0,0};
				int[] sFa = {0,0,0,0,0};
				int[] sPo = {0,0,0,0,0,0};		

				// CARICAMENTO TABELLINI COMPLETI E AZIONI DELLA PARTITA N. 16 ( FLORENTIA - VELA )

				sessione.beginTransaction();

				partita = partitaDao.findById(16L);
				SB = squadraDao.findById("FLO");
				SN = squadraDao.findById("ANV");
				partita.setTecnicoBianco(tecnicoDao.findById("COTTI_A"));
				partita.setTecnicoNero(tecnicoDao.findById("PACE_M"));

				AB[1] = atletaDao.findById("BANCHELLI_C");
				AB[2] = atletaDao.findById("LANDI_I");
				AB[3] = atletaDao.findById("LEPORE_L");
				AB[4] = atletaDao.findById("CORDOVANI_S");
				AB[5] = atletaDao.findById("GASPARRI_G");
				AB[6] = atletaDao.findById("VITTORI_N");
				AB[7] = atletaDao.findById("NESTI_L");
				AB[8] = atletaDao.findById("FRANCINI_R");
				AB[9] = atletaDao.findById("GIACHI_G");
				AB[10] = atletaDao.findById("NENCHA_C");
				AB[11] = atletaDao.findById("MARIONI_V");
				AB[12] = atletaDao.findById("MUGNAI_B");
				AB[13] = atletaDao.findById("PEREGO_L");

				AN[1] = atletaDao.findById("UCCELLA_V");
				AN[2] = atletaDao.findById("STRAPPATO_L");
				AN[3] = atletaDao.findById("IVANOVA_E");
				AN[4] = atletaDao.findById("SANTINI_M");
				AN[5] = atletaDao.findById("VECCHIAREL_E");
				AN[6] = atletaDao.findById("BERSACCHIA_G");
				AN[7] = atletaDao.findById("BARTOCCI_C");
				AN[8] = atletaDao.findById("OLIVIERI_M");
				AN[9] = atletaDao.findById("COLLETTA_F");
				AN[10] = atletaDao.findById("DEMATTEIS_V");
				AN[11] = atletaDao.findById("ALTAMURA_E");
				AN[12] = atletaDao.findById("QUATTRINI_E");
				AN[13] = atletaDao.findById("ANDREONI_A");

				// creazione tabellini della squadra
				
				TB[0]  = tabellinoDao.create( 0, in,   224,  4, 33,  4, true, new int[] {0,2,2,0,0,0}, new int[] {0,7,24,0,0,2}, new int[] {2,1,0,0,1}, new int[] {14,0,0,20,1,1}, partita, SB, null);
				TB[1]  = tabellinoDao.create( 1, true,  32,  1,  5, fp, es, new int[] {0,0,1,0,0,0}, new int[] {0,0,5,0,0,0}, sFa, new int[] {14,0,0,20,1,1}, partita, SB, AB[1]);
				TB[2]  = tabellinoDao.create( 2, true, min, rs, tf,  3, true, sRe, sTi, new int[] {1,1,0,0,1}, null, partita, SB, AB[2]);
				TB[3]  = tabellinoDao.create( 3, in,   min, rs,  2, fp, es, new int[] {0,0,0,0,0,0}, new int[] {0,1,1,0,0,0}, sFa, null, partita, SB, AB[3]);
				TB[4]  = tabellinoDao.create( 4, true, min, rs,  1, fp, es, new int[] {0,0,0,0,0,0}, new int[] {0,0,0,0,0,1}, sFa, null, partita, SB, AB[4]);
				TB[5]  = tabellinoDao.create( 5, in,   min,  1,  1, fp, es, new int[] {0,1,0,0,0,0}, new int[] {0,1,0,0,0,0}, sFa, null, partita, SB, AB[5]);
				TB[6]  = tabellinoDao.create( 6, in,   min,  1,  2, fp, es, new int[] {0,0,1,0,0,0}, new int[] {0,0,2,0,0,0}, sFa, null, partita, SB, AB[6]);
				TB[7]  = tabellinoDao.create( 7, true, min, rs,  6, fp, es, new int[] {0,0,0,0,0,0}, new int[] {0,0,5,0,0,1}, sFa, null, partita, SB, AB[7]);
				TB[8]  = tabellinoDao.create( 8, true, min,  1,  5,  1, es, new int[] {0,1,0,0,0,0}, new int[] {0,2,3,0,0,0}, new int[] {1,0,0,0,0}, null, partita, SB, AB[8]);
				TB[9]  = tabellinoDao.create( 9, true, min, rs,  5, fp, es, new int[] {0,0,0,0,0,0}, new int[] {0,3,2,0,0,0}, sFa, null, partita, SB, AB[9]);
				TB[10] = tabellinoDao.create(10, true, min, rs,  4, fp, es, new int[] {0,0,0,0,0,0}, new int[] {0,0,4,0,0,0}, sFa, null, partita, SB, AB[10]);
				TB[11] = tabellinoDao.create(11, in,   min, rs,  2, fp, es, new int[] {0,0,0,0,0,0}, new int[] {0,0,2,0,0,0}, sFa, null, partita, SB, AB[11]);
				TB[12] = tabellinoDao.create(12, in,   min, rs, tf, fp, es, sRe, sTi, sFa, null, partita, SB, AB[12]);
				TB[13] = tabellinoDao.create(13, in,   min, rs, tf, fp, es, sRe, sTi, sFa, null, partita, SB, AB[13]);
								
				TN[0]  = tabellinoDao.create( 0, in,   224,  6, 26,  7, es, new int[] {1,1,2,1,0,1}, new int[] {1,2,17,5,0,1}, new int[] {2,2,3,0,0}, new int[] {12,0,3,16,0,5}, partita, SN, null);
				TN[1]  = tabellinoDao.create( 1, true,  32, rs,  1, fp, es, new int[] {0,0,0,0,0,0}, new int[] {0,0,1,0,0,0}, sFa, new int[] {12,0,3,16,0,5}, partita, SN, AN[1]);
				TN[2]  = tabellinoDao.create( 2, true,  30, rs,  3,  2, es, new int[] {0,0,0,0,0,0}, new int[] {0,0,3,0,0,0}, new int[] {2,0,0,0,0}, null, partita, SN, AN[2]);
				TN[3]  = tabellinoDao.create( 3, true,  24,  3,  7,  2, es, new int[] {1,1,1,0,0,0}, new int[] {1,1,5,0,0,0}, new int[] {0,1,1,0,0}, null, partita, SN, AN[3]);
				TN[4]  = tabellinoDao.create( 4, in,    14, rs, tf, fp, es, sRe, sTi, sFa, null, partita, SN, AN[4]);
				TN[5]  = tabellinoDao.create( 5, in,    10, rs, tf, fp, es, sRe, sTi, sFa, null, partita, SN, AN[5]);
				TN[6]  = tabellinoDao.create( 6, in,     5, rs, tf, fp, es, sRe, sTi, sFa, null, partita, SN, AN[6]);
				TN[7]  = tabellinoDao.create( 7, in,     2, rs, tf, fp, es, sRe, sTi, sFa, null, partita, SN, AN[7]);
				TN[8]  = tabellinoDao.create( 8, in,    11, rs, tf,  1, es, sRe, sTi, new int[] {0,0,1,0,0}, null, partita, SN, AN[8]);
				TN[9]  = tabellinoDao.create( 9, true,  21, rs,  1, fp, es, new int[] {0,0,0,0,0,0}, new int[] {0,0,0,1,0,0}, sFa, null, partita, SN, AN[9]);
				TN[10] = tabellinoDao.create(10, true,  18,  2,  3,  1, es, new int[] {0,0,1,0,0,1}, new int[] {0,0,2,0,0,1}, new int[] {0,0,1,0,0}, null, partita, SN, AN[10]);
				TN[11] = tabellinoDao.create(11, true,  25, rs,  4,  1, es, new int[] {0,0,0,0,0,0}, new int[] {0,0,4,0,0,0}, new int[] {0,1,0,0,0}, null, partita, SN, AN[11]);
				TN[12] = tabellinoDao.create(12, true,  32,  1,  7, fp, es, new int[] {0,0,0,1,0,0}, new int[] {0,1,2,4,0,0}, sFa, null, partita, SN, AN[12]);
				TN[13] = tabellinoDao.create(13, in,   min, rs, tf, fp, es, sRe, sTi, sFa, null, partita, SN, AN[13]);

				sessione.getTransaction().commit();

				// caricamento azioni della partita

				int[] VB = { 1, 2, 4, 7, 8, 9, 10 };	int[] VN = { 1, 2, 3, 9, 10, 11, 12 };

				sessione.beginTransaction();
	
				// azioni primo periodo	
				azioneDao.create (false, "Portiere iniziale", null, null, VB[0], partita, new int[] {1,8,0}, SB, AB[VB[0]]);
				azioneDao.create (false, "Portiere iniziale", null, null, VN[0], partita, new int[] {1,8,0}, SN, AN[VN[0]]);
				azioneDao.create (false, "Sprint vinto", null, "0-0", 11, partita, new int[] {1,8,0}, SN, AN[11]);
				azioneDao.create (false, "Palla Persa", "infrazione 30 secondi", null, 0, partita, new int[] {1,0,0}, SN, null);
				azioneDao.create (false, "Tiro Azione", "parato", null, 8, partita, new int[] {1,0,0}, SB, AB[8]);
				azioneDao.create (false, "Fallo espulsione", "perimetro", null, 2, partita, new int[] {1,6,47}, SB, AB[2]);	
				azioneDao.create (true, "Palla persa", "infrazione", null, 0, partita, new int[] {1,0,0}, SN, null);
				azioneDao.create (false, "Tiro Azione", "fuori", null, 7, partita, new int[] {1,0,0}, SB, AB[7]);
				azioneDao.create (false, "Palla persa", "recupero avversario", null, 0, partita, new int[] {1,0,0}, SN, null);
				azioneDao.create (false, "Palla persa", "recupero avversario", null, 0, partita, new int[] {1,0,0}, SB, null);
				azioneDao.create (false, "Tiro Controfuga", "parato corner", null, 3, partita, new int[] {1,0,0}, SN, AN[3]);
				azioneDao.create (false, "Tiro Azione", "fuori", null, 3, partita, new int[] {1,0,0}, SN, AN[3]);
				azioneDao.create (false, "Palla persa", "controfallo", null, 0, partita, new int[] {1,0,0},SB, null);
				azioneDao.create (false, "Tiro Centroboa", "parato", null, 12, partita, new int[] {1,0,0}, SN, AN[12]);
				azioneDao.create (false, "Palla persa", "recupero avversario", null, 0, partita, new int[] {1,0,0}, SB, null);
				azioneDao.create (false, "Palla persa", "controfallo", null, 0, partita, new int[] {1,0,0},SN, null);
				azioneDao.create (false, "Tiro Azione", "fuori", null, 10, partita, new int[] {1,0,0}, SB, AB[10]);
				azioneDao.create (false, "Palla persa", "recupero avversario", null, 0, partita, new int[] {1,0,0}, SN, null);
				azioneDao.create (false, "Tiro Azione", "fuori", null, 10, partita, new int[] {1,0,0}, SB, AB[10]);
				azioneDao.create (false, "Tiro Azione", "parato", null, 2, partita, new int[] {1,0,0}, SN, AN[2]);
				azioneDao.create (false, "Tiro Azione", "legno", null, 9, partita, new int[] {1,0,0}, SB, AB[9]);
				azioneDao.create (false, "Palla persa", "controfallo", null, 0, partita, new int[] {1,0,0},SN, null);
				azioneDao.create (false, "Palla persa", "recupero avversario", null, 0, partita, new int[] {1,0,0}, SB, null);
				azioneDao.create (false, "Tiro Azione", "parato", null, 2, partita, new int[] {1,0,0}, SN, AN[2]);
				azioneDao.create (false, "Tiro Controfuga", "parato", null, 10, partita, new int[] {1,0,0}, SB, AB[10]);
				azioneDao.create (false, "Palla persa", "recupero avversario", null, 0, partita, new int[] {1,0,0}, SN, null);
				azioneDao.create (false, "Tiro Azione", "fuori", null, 8, partita, new int[] {1,0,0}, SB, AB[8]);
				azioneDao.create (false, "Palla Persa", "infrazione 30 secondi", null, 0, partita, new int[] {1,0,0}, SN, null);
				azioneDao.create (false, "Palla persa", "recupero avversario", null, 0, partita, new int[] {1,0,0}, SB, null);
				azioneDao.create (false, "Tiro Azione", "parato", null, 12, partita, new int[] {1,0,0}, SN, AN[12]);
				azioneDao.create (false, "Tiro Azione", "fuori", null, 1, partita, new int[] {1,0,0}, SB, AB[1]);

				// azioni secondo periodo
				azioneDao.create (false, "Portiere iniziale", null, null, VB[0], partita, new int[] {2,8,0}, SB, AB[VB[0]]);
				azioneDao.create (false, "Portiere iniziale", null, null, VN[0], partita, new int[] {2,8,0}, SN, AN[VN[0]]);
				azioneDao.create (false, "Sprint vinto", null, "0-0", 11, partita, new int[] {2,8,0}, SB, AB[11]);
				azioneDao.create (false, "Palla persa", "infrazione", null, 0, partita, new int[] {2,0,0}, SB, null);
				azioneDao.create (false, "Fallo espulsione", "centro", null, 2, partita, new int[] {2,7,13}, SB, AB[2]);	
				azioneDao.create (true, "Tiro Super.Numerica", "RETE", "0-1", 3, partita, new int[] {2,6,57}, SN, AN[3]);
				azioneDao.create (false, "Palla persa", "recupero avversario", null, 0, partita, new int[] {2,0,0}, SB, null);
				azioneDao.create (false, "Fallo espulsione", "centro", null, 8, partita, new int[] {2,6,16}, SB, AB[8]);	
				azioneDao.create (true, "Tiro Super.Numerica", "fuori", null, 12, partita, new int[] {2,0,0}, SN, AN[12]);
				azioneDao.create (false, "Fallo espulsione", "perimetro", null, 11, partita, new int[] {2,5,47}, SN, AN[11]);	
				azioneDao.create (true, "Tiro Super.Numerica", "parato", null, 9, partita, new int[] {2,0,0}, SB, AB[9]);
				azioneDao.create (true, "Rimbalzo offensivo", null, null, 0, partita, new int[] {2,0,0}, SB, null);
				azioneDao.create (true, "Tiro Super.Numerica", "RETE", "1-1", 5, partita, new int[] {2,5,28}, SB, AB[5]);
				azioneDao.create (false, "Tiro Azione", "parato", null, 12, partita, new int[] {2,0,0}, SN, AN[12]);
				azioneDao.create (false, "Tiro Controfuga", "bloccato", null, 7, partita, new int[] {2,0,0}, SB, AB[7]);
				azioneDao.create (false, "Palla persa", "controfallo", null, 0, partita, new int[] {2,0,0}, SN, null);
				azioneDao.create (false, "Tiro Controfuga", "parato corner", null, 4, partita, new int[] {2,0,0}, SB, AB[4]);	
				azioneDao.create (false, "Tiro Azione", "parato", null, 11, partita, new int[] {2,0,0}, SB, AB[11]);
				azioneDao.create (false, "Tiro Azione", "bloccato", null, 2, partita, new int[] {2,0,0}, SN, AN[2]);	
				azioneDao.create (false, "Tiro Azione", "legno", null, 9, partita, new int[] {2,0,0}, SB, AB[9]);
				azioneDao.create (false, "Palla persa", "palla fuori", null, 0, partita, new int[] {2,0,0}, SN, null);
				azioneDao.create (false, "Tiro Azione", "RETE", "2-1", 6, partita, new int[] {2,2,26}, SB, AB[6]);
				azioneDao.create (false, "Palla persa", "recupero avversario", null, 0, partita, new int[] {2,0,0}, SN, null);
				azioneDao.create (false, "Tiro Azione", "parato", null, 7, partita, new int[] {2,0,0}, SB, AB[7]);
				azioneDao.create (false, "Palla persa", "recupero avversario", null, 0, partita, new int[] {2,0,0}, SN, null);
				azioneDao.create (false, "Tiro Azione", "legno", null, 10, partita, new int[] {2,0,0}, SB, AB[10]);
				azioneDao.create (false, "Palla persa", "recupero avversario", null, 0, partita, new int[] {2,0,0}, SN, null);
				azioneDao.create (false, "Tiro Azione", "legno", null, 8, partita, new int[] {2,0,0}, SB, AB[8]);
				azioneDao.create (true, "Rimbalzo offensivo", null, null, 0, partita, new int[] {2,0,0}, SB, null);
				azioneDao.create (true, "Palla persa", "controfallo", null, 0, partita, new int[] {2,0,0}, SB, null);
				azioneDao.create (false, "Fallo rigore", null, null, 2, partita, new int[] {2,0,9}, SB, AB[2]);
				azioneDao.create (false, "Tiro Rigore", "RETE", "2-2", 3, partita, new int[] {2,0,9}, SN, AN[3]);
				azioneDao.create (false, "Fine tempo", null, null, 0, partita, new int[] {2,0,0}, SB, null);

				// azioni terzo periodo
				azioneDao.create (false, "Portiere iniziale", null, null, VB[0], partita, new int[] {3,8,0}, SB, AB[VB[0]]);
				azioneDao.create (false, "Portiere iniziale", null, null, VN[0], partita, new int[] {3,8,0}, SN, AN[VN[0]]);
				azioneDao.create (false, "Sprint vinto", null, "2-2", 9, partita, new int[] {3,8,0}, SB, AB[9]);
				azioneDao.create (false, "Tiro Azione", "fuori", null, 12, partita, new int[] {3,0,0}, SB, AB[12]);	
				azioneDao.create (false, "Tiro Controfuga", "parato", null, 3, partita, new int[] {3,0,0}, SN, AN[3]);	
				azioneDao.create (false, "Palla persa", "controfallo", null, 0, partita, new int[] {3,0,0}, SB, null);
				azioneDao.create (false, "Tiro Azione", "parato", null, 11, partita, new int[] {3,0,0}, SN, AN[11]);
				azioneDao.create (false, "Tiro Azione", "fuori", null, 10, partita, new int[] {3,0,0}, SB, AB[10]);
				azioneDao.create (false, "Palla persa", "recupero avversario", null, 0, partita, new int[] {3,0,0}, SN, null);
				azioneDao.create (false, "Palla persa", "recupero avversario", null, 0, partita, new int[] {3,0,0}, SB, null);
				azioneDao.create (false, "Tiro Azione", "bloccato", null, 3, partita, new int[] {3,0,0}, SN, AN[3]);
				azioneDao.create (true, "Rimbalzo offensivo", null, null, 5, partita, new int[] {3,0,0}, SN, AN[5]);
				azioneDao.create (false, "Palla persa", "Recupero avversario", null, 0, partita, new int[] {3,0,0}, SN, null);
				azioneDao.create (false, "Fallo espulsione", "ripartenza", null, 3, partita, new int[] {3,4,43}, SN, AN[3]);	
				azioneDao.create (false, "TimeOut", null, null, 0, partita, new int[] {3,4,43}, SB, null);	
				azioneDao.create (true, "Tiro Super.Numerica", "parato", null, 9, partita, new int[] {3,0,0}, SB, AB[9]);
				azioneDao.create (false, "Palla persa", "recupero avversario", null, 0, partita, new int[] {3,0,0}, SN, null);
				azioneDao.create (false, "Tiro Azione", "fuori", null, 7, partita, new int[] {3,0,0}, SB, AB[7]);
				azioneDao.create (false, "Tiro Centroboa", "RETE", "2-3", 12, partita, new int[] {3,3,15}, SN, AN[12]);
				azioneDao.create (false, "Fallo espulsione", "centro", null, 2, partita, new int[] {3,2,52}, SN, AN[2]);		
				azioneDao.create (true, "Tiro Super.Numerica", "bloccato", null, 9, partita, new int[] {3,0,0}, SB, AB[9]);
				azioneDao.create (false, "Tiro Azione", "bloccato", null, 10, partita, new int[] {3,0,0}, SN, AN[10]);
				azioneDao.create (false, "Fallo espulsione", "ripartenza", null, 10, partita, new int[] {3,2,4}, SN, AN[10]);		
				azioneDao.create (true, "Tiro Super.Numerica", "RETE","3-3", 8, partita, new int[] {3,1,40}, SB, AB[8]);
				azioneDao.create (false, "Tiro Centroboa", "parato", null, 12, partita, new int[] {3,0,0}, SN, AN[12]);
				azioneDao.create (false, "Tiro Azione", "fuori", null, 7, partita, new int[] {3,0,0}, SB, AB[7]);
				azioneDao.create (false, "Palla persa", "recupero avversario", null, 0, partita, new int[] {3,0,0}, SN, null);
				azioneDao.create (false, "Tiro Azione", "parato", null, 6, partita, new int[] {3,0,0}, SB, AB[6]);
				azioneDao.create (false, "Tiro 6 Metri", "fuori", null, 11, partita, new int[] {3,0,0}, SN, AN[11]);
				azioneDao.create (false, "Tiro Azione", "parato", null, 1, partita, new int[] {3,0,0}, SB, AB[1]);	

				// azioni quarto periodo
				azioneDao.create (false, "Portiere iniziale", null, null, VB[0], partita, new int[] {4,8,0}, SB, AB[VB[0]]);
				azioneDao.create (false, "Portiere iniziale", null, null, VN[0], partita, new int[] {4,8,0}, SN, AN[VN[0]]);
				azioneDao.create (false, "Sprint vinto", null,  "3-3", 11, partita, new int[] {4,8,0}, SN, AN[11]);
				azioneDao.create (false, "Tiro Centroboa", "parato", null, 9, partita, new int[] {4,0,0}, SN, AN[9]);
				azioneDao.create (false, "Palla persa", "recupero avversario", null, 0, partita, new int[] {4,0,0}, SB, null);
				azioneDao.create (false, "Palla persa", "recupero avversario", null, 0, partita, new int[] {4,0,0}, SB, null);
				azioneDao.create (false, "Palla persa", "controfallo", null, 0, partita, new int[] {4,0,0}, SN, null);
				azioneDao.create (false, "Palla persa", "recupero avversario", null, 0, partita, new int[] {4,0,0}, SB, null);
				azioneDao.create (false, "Tiro Controfuga", "RETE", "3-5", 10, partita, new int[] {4,6,5}, SN, AN[10]);
				azioneDao.create (false, "Fallo espulsione", "centro", null, 2, partita, new int[] {4,5,47}, SN, AN[2]);	
				azioneDao.create (true, "Tiro Super.Numerica", "parato", null, 8, partita, new int[] {4,0,0}, SB, AB[8]);
				azioneDao.create (false, "Tiro Azione", "parato corner", null, 11, partita, new int[] {4,0,0}, SN, AN[11]);
				azioneDao.create (false, "Palla persa", "recupero avversario", null, 0, partita, new int[] {4,0,0}, SN, null);
				azioneDao.create (false, "Tiro Azione", "parato corner", null, 3, partita, new int[] {4,0,0}, SB, AB[3]);
				azioneDao.create (false, "Palla persa", "recupero avversario", null, 0, partita, new int[] {4,0,0}, SB, null);
				azioneDao.create (false, "Tiro Azione", "RETE", "3-6", 3, partita, new int[] {4,3,50}, SN, AN[3]);
				azioneDao.create (false, "Fallo espulsione", "perimetro", null, 3, partita, new int[] {4,3,44}, SN, AN[3]);	
				azioneDao.create (true, "Palla persa", "infrazione 30 secondi", null, 0, partita, new int[] {4,0,0}, SB, null);
				azioneDao.create (false, "Palla persa", "recupero avversario", null, 0, partita, new int[] {4,0,0}, SN, null);
				azioneDao.create (false, "Fallo espulsione", "ripartenza", null, 8, partita, new int[] {4,2,49}, SN, AN[8]);	
				azioneDao.create (false, "TimeOut", null, null, 0, partita, new int[] {4,2,49}, SB, null);	
				azioneDao.create (true, "Tiro Super.Numerica", "fuori", null, 3, partita, new int[] {4,0,0}, SB, AB[3]);	
				azioneDao.create (false, "Tiro Centroboa", "parato", null, 12, partita, new int[] {4,0,0}, SN, AN[12]);
				azioneDao.create (false, "Tiro Azione", "RETE", "4-6", 1, partita, new int[] {4,1,45}, SB, AB[1]);
				azioneDao.create (false, "Tiro Azione", "parato", null, 11, partita, new int[] {4,0,0}, SN, AN[11]);
				azioneDao.create (false, "Tiro Azione", "legno", null, 1, partita, new int[] {4,0,0}, SB, AB[1]);
				azioneDao.create (false, "Tiro Azione", "bloccato", null, 11, partita, new int[] {4,0,0}, SN, AN[11]);	
				azioneDao.create (false, "Tiro Azione", "parato", null, 1, partita, new int[] {4,0,0}, SB, AB[1]);
				azioneDao.create (false, "TimeOut", null, null, 0, partita, new int[] {4,0,14}, SN, null);	
				azioneDao.create (false, "Tiro Azione", "parato", null, 1, partita, new int[] {4,0,0}, SN, AN[1]);

				sessione.getTransaction().commit();

				// CARICAMENTO TABELLINI COMPLETI E AZIONI DELLA PARTITA N. 15 ( SIS ROMA - VELA )

				sessione.beginTransaction();

				partita = partitaDao.findById(15L);
				SB = squadraDao.findById("RMS");
				SN = squadraDao.findById("ANV");
				partita.setTecnicoBianco(tecnicoDao.findById("CAPANNA_M"));
				partita.setTecnicoNero(tecnicoDao.findById("PACE_M"));

				// creazione tabellini della squadra
				
				TB[0]  = tabellinoDao.create( 0,16, 9, partita, SB, null);
				TB[1]  = tabellinoDao.create( 1, 0, 0, partita, SB, atletaDao.findById("SPARANO_F"));
				TB[2]  = tabellinoDao.create( 2, 2, 1, partita, SB, atletaDao.findById("TABANI_C"));
				TB[3]  = tabellinoDao.create( 3, 1, 0, partita, SB, atletaDao.findById("GALARDI_G"));
				TB[4]  = tabellinoDao.create( 4, 1, 1, partita, SB, atletaDao.findById("AVEGNO_S"));
				TB[5]  = tabellinoDao.create( 5, 3, 1, partita, SB, atletaDao.findById("GIUSTINI_S"));
				TB[6]  = tabellinoDao.create( 6, 2, 0, partita, SB, atletaDao.findById("IANNARELLI_A"));
				TB[7]  = tabellinoDao.create( 7, 3, 1, partita, SB, atletaDao.findById("PICOZZI_D"));
				TB[8]  = tabellinoDao.create( 8, 0, 0, partita, SB, atletaDao.findById("PAPI_L"));
				TB[9]  = tabellinoDao.create( 9, 1, 0, partita, SB, atletaDao.findById("NARDINI_C"));
				TB[10] = tabellinoDao.create(10, 2, 2, partita, SB, atletaDao.findById("DICLAUDIO_L"));
				TB[11] = tabellinoDao.create(11, 1, 0, partita, SB, atletaDao.findById("STORAI_S"));
				TB[12] = tabellinoDao.create(12, 0, 2, partita, SB, atletaDao.findById("BIANCHI_A"));
				TB[13] = tabellinoDao.create(13, 0, 1, partita, SB, atletaDao.findById("BRANDIMART_M"));
								
				TN[0]  = tabellinoDao.create( 0, 4,11, partita, SN, null);
				TN[1]  = tabellinoDao.create( 1, 0, 0, partita, SN, atletaDao.findById("UCCELLA_V"));
				TN[2]  = tabellinoDao.create( 2, 1, 3, partita, SN, atletaDao.findById("STRAPPATO_L"));
				TN[3]  = tabellinoDao.create( 3, 2, 0, partita, SN, atletaDao.findById("IVANOVA_E"));
				TN[4]  = tabellinoDao.create( 4, 0, 0, partita, SN, atletaDao.findById("SANTINI_M"));
				TN[5]  = tabellinoDao.create( 5, 0, 0, partita, SN, atletaDao.findById("VECCHIAREL_E"));
				TN[6]  = tabellinoDao.create( 6, 0, 0, partita, SN, atletaDao.findById("BERSACCHIA_G"));
				TN[7]  = tabellinoDao.create( 7, 0, 1, partita, SN, atletaDao.findById("BARTOCCI_C"));
				TN[8]  = tabellinoDao.create( 8, 0, 2, partita, SN, atletaDao.findById("OLIVIERI_M"));
				TN[9]  = tabellinoDao.create( 9, 0, 1, partita, SN, atletaDao.findById("COLLETTA_F"));
				TN[10] = tabellinoDao.create(10, 0, 1, partita, SN, atletaDao.findById("DEMATTEIS_V"));
				TN[11] = tabellinoDao.create(11, 1, 1, partita, SN, atletaDao.findById("ALTAMURA_E"));
				TN[12] = tabellinoDao.create(12, 0, 2, partita, SN, atletaDao.findById("QUATTRINI_E"));
				TN[13] = tabellinoDao.create(13, 0, 0, partita, SN, atletaDao.findById("ANDREONI_A"));
					
				sessione.getTransaction().commit();

				// CARICAMENTO TABELLINI COMPLETI E AZIONI DELLA PARTITA N. 14 ( VELA - EKIPE ORIZZONTE CATANIA )

				sessione.beginTransaction();

				partita = partitaDao.findById(14L);
				SB = squadraDao.findById("ANV");
				SN = squadraDao.findById("CTE");
				partita.setTecnicoBianco(tecnicoDao.findById("PACE_M"));
				partita.setTecnicoNero(tecnicoDao.findById("MICELI_M"));

				// creazione tabellini della squadra

				TB[0]  = tabellinoDao.create( 0, 3,11, partita, SB, null);
				TB[1]  = tabellinoDao.create( 1, 0, 0, partita, SB, atletaDao.findById("UCCELLA_V"));
				TB[2]  = tabellinoDao.create( 2, 1, 2, partita, SB, atletaDao.findById("STRAPPATO_L"));
				TB[3]  = tabellinoDao.create( 3, 1, 2, partita, SB, atletaDao.findById("IVANOVA_E"));
				TB[4]  = tabellinoDao.create( 4, 0, 0, partita, SB, atletaDao.findById("RICCIO_A"));
				TB[5]  = tabellinoDao.create( 5, 0, 0, partita, SB, atletaDao.findById("VECCHIAREL_E"));
				TB[6]  = tabellinoDao.create( 6, 0, 2, partita, SB, atletaDao.findById("BERSACCHIA_G"));
				TB[7]  = tabellinoDao.create( 7, 0, 2, partita, SB, atletaDao.findById("BARTOCCI_C"));
				TB[8]  = tabellinoDao.create( 8, 0, 0, partita, SB, atletaDao.findById("OLIVIERI_M"));
				TB[9]  = tabellinoDao.create( 9, 0, 1, partita, SB, atletaDao.findById("COLLETTA_F"));
				TB[10] = tabellinoDao.create(10, 0, 1, partita, SB, atletaDao.findById("DEMATTEIS_V"));
				TB[11] = tabellinoDao.create(11, 1, 0, partita, SB, atletaDao.findById("ALTAMURA_E"));
				TB[12] = tabellinoDao.create(12, 0, 1, partita, SB, atletaDao.findById("QUATTRINI_E"));
				TB[13] = tabellinoDao.create(13, 0, 0, partita, SB, atletaDao.findById("ANDREONI_A"));
				
				TN[0]  = tabellinoDao.create( 0,27, 2, partita, SN, null);
				TN[1]  = tabellinoDao.create( 1, 0, 0, partita, SN, atletaDao.findById("GORLERO_G"));
				TN[2]  = tabellinoDao.create( 2, 2, 0, partita, SN, atletaDao.findById("IOANNOU_C"));
				TN[3]  = tabellinoDao.create( 3, 1, 0, partita, SN, atletaDao.findById("GARIBOTTI_A"));
				TN[4]  = tabellinoDao.create( 4, 0, 0, partita, SN, atletaDao.findById("VIACAVA_G"));
				TN[5]  = tabellinoDao.create( 5, 5, 0, partita, SN, atletaDao.findById("AIELLO_R"));
				TN[6]  = tabellinoDao.create( 6, 3, 0, partita, SN, atletaDao.findById("BARZON_L"));
				TN[7]  = tabellinoDao.create( 7, 4, 0, partita, SN, atletaDao.findById("PALMIERI_V"));
				TN[8]  = tabellinoDao.create( 8, 5, 1, partita, SN, atletaDao.findById("MARLETTA_C"));
				TN[9]  = tabellinoDao.create( 9, 3, 0, partita, SN, atletaDao.findById("EMMOLO_G"));
				TN[10] = tabellinoDao.create(10, 2, 0, partita, SN, atletaDao.findById("VUKOVIC_J"));
				TN[11] = tabellinoDao.create(11, 1, 0, partita, SN, atletaDao.findById("RICCIOLI_I"));
				TN[12] = tabellinoDao.create(12, 1, 1, partita, SN, atletaDao.findById("SPAMPINATO_D"));
				TN[13] = tabellinoDao.create(13, 0, 0, partita, SN, atletaDao.findById("CONDORELLI_G"));

				sessione.getTransaction().commit();

				// CARICAMENTO TABELLINI COMPLETI E AZIONI DELLA PARTITA N. 13 ( VELA - FLORENTIA )

				sessione.beginTransaction();

				partita = partitaDao.findById(13L);
				SB = squadraDao.findById("ANV");
				SN = squadraDao.findById("FLO");
				partita.setTecnicoBianco(tecnicoDao.findById("PACE_M"));
				partita.setTecnicoNero(tecnicoDao.findById("COTTI_A"));

				// creazione tabellini della squadra
				
				TB[0]  = tabellinoDao.create( 0, 9, 0, partita, SB, null);
				TB[1]  = tabellinoDao.create( 1, 2, 0, partita, SB, atletaDao.findById("UCCELLA_V"));
				TB[2]  = tabellinoDao.create( 2, 2, 2, partita, SB, atletaDao.findById("STRAPPATO_L"));
				TB[3]  = tabellinoDao.create( 3, 2, 3, partita, SB, atletaDao.findById("IVANOVA_E"));
				TB[4]  = tabellinoDao.create( 4, 0, 2, partita, SB, atletaDao.findById("RICCIO_A"));
				TB[5]  = tabellinoDao.create( 5, 0, 1, partita, SB, atletaDao.findById("FERRETTI_M"));
				TB[6]  = tabellinoDao.create( 6, 0, 0, partita, SB, atletaDao.findById("BERSACCHIA_G"));
				TB[7]  = tabellinoDao.create( 7, 0, 0, partita, SB, atletaDao.findById("BARTOCCI_C"));
				TB[8]  = tabellinoDao.create( 8, 0, 0, partita, SB, atletaDao.findById("OLIVIERI_M"));
				TB[9]  = tabellinoDao.create( 9, 1, 1, partita, SB, atletaDao.findById("COLLETTA_F"));
				TB[10] = tabellinoDao.create(10, 0, 0, partita, SB, atletaDao.findById("VECCHIAREL_E"));
				TB[11] = tabellinoDao.create(11, 1, 0, partita, SB, atletaDao.findById("ALTAMURA_E"));
				TB[12] = tabellinoDao.create(12, 1, 2, partita, SB, atletaDao.findById("QUATTRINI_E"));
				TB[13] = tabellinoDao.create(13, 0, 0, partita, SB, atletaDao.findById("ANDREONI_A"));

				TN[0]  = tabellinoDao.create( 0,12, 8, partita, SN, null);
				TN[1]  = tabellinoDao.create( 1, 0, 0, partita, SN, atletaDao.findById("BANCHELLI_C"));
				TN[2]  = tabellinoDao.create( 2, 1, 2, partita, SN, atletaDao.findById("LANDI_I"));
				TN[3]  = tabellinoDao.create( 3, 0, 0, partita, SN, atletaDao.findById("LEPORE_L"));
				TN[4]  = tabellinoDao.create( 4, 1, 1, partita, SN, atletaDao.findById("CORDOVANI_S"));
				TN[5]  = tabellinoDao.create( 5, 1, 0, partita, SN, atletaDao.findById("GASPARRI_G"));
				TN[6]  = tabellinoDao.create( 6, 1, 0, partita, SN, atletaDao.findById("VITTORI_N"));
				TN[7]  = tabellinoDao.create( 7, 4, 0, partita, SN, atletaDao.findById("NESTI_L"));
				TN[8]  = tabellinoDao.create( 8, 1, 2, partita, SN, atletaDao.findById("FRANCINI_R"));
				TN[9]  = tabellinoDao.create( 9, 1, 1, partita, SN, atletaDao.findById("GIACHI_G"));
				TN[10] = tabellinoDao.create(10, 0, 0, partita, SN, atletaDao.findById("NENCHA_C"));
				TN[11] = tabellinoDao.create(11, 2, 2, partita, SN, atletaDao.findById("MARIONI_V"));
				TN[12] = tabellinoDao.create(12, 0, 0, partita, SN, atletaDao.findById("MUGNAI_B"));
				TN[13] = tabellinoDao.create(13, 0, 0, partita, SN, atletaDao.findById("PEREGO_L"));

				sessione.getTransaction().commit();

				// CARICAMENTO TABELLINI COMPLETI E AZIONI DELLA PARTITA N. 12 ( VELA - SIS ROMA )

				sessione.beginTransaction();

				partita = partitaDao.findById(12L);
				SB = squadraDao.findById("ANV");
				SN = squadraDao.findById("RMS");
				partita.setTecnicoBianco(tecnicoDao.findById("PACE_M"));
				partita.setTecnicoNero(tecnicoDao.findById("CAPANNA_M"));

				// creazione tabellini della squadra

				TB[0]  = tabellinoDao.create( 0, 9,12, partita, SB, null);
				TB[1]  = tabellinoDao.create( 1, 0, 0, partita, SB, atletaDao.findById("UCCELLA_V"));
				TB[2]  = tabellinoDao.create( 2, 0, 2, partita, SB, atletaDao.findById("STRAPPATO_L"));
				TB[3]  = tabellinoDao.create( 3, 3, 1, partita, SB, atletaDao.findById("IVANOVA_E"));
				TB[4]  = tabellinoDao.create( 4, 1, 1, partita, SB, atletaDao.findById("RICCIO_A"));
				TB[5]  = tabellinoDao.create( 5, 0, 0, partita, SB, atletaDao.findById("FERRETTI_M"));
				TB[6]  = tabellinoDao.create( 6, 0, 2, partita, SB, atletaDao.findById("BERSACCHIA_G"));
				TB[7]  = tabellinoDao.create( 7, 0, 0, partita, SB, atletaDao.findById("BARTOCCI_C"));
				TB[8]  = tabellinoDao.create( 8, 0, 0, partita, SB, atletaDao.findById("OLIVIERI_M"));
				TB[9]  = tabellinoDao.create( 9, 2, 2, partita, SB, atletaDao.findById("COLLETTA_F"));
				TB[10] = tabellinoDao.create(10, 0, 1, partita, SB, atletaDao.findById("DEMATTEIS_V"));
				TB[11] = tabellinoDao.create(11, 2, 2, partita, SB, atletaDao.findById("ALTAMURA_E"));
				TB[12] = tabellinoDao.create(12, 1, 1, partita, SB, atletaDao.findById("QUATTRINI_E"));
				TB[13] = tabellinoDao.create(13, 0, 0, partita, SB, atletaDao.findById("ANDREONI_A"));

				TN[0]  = tabellinoDao.create( 0,16, 8, partita, SN, null);
				TN[1]  = tabellinoDao.create( 1, 0, 0, partita, SN, atletaDao.findById("SPARANO_F"));
				TN[2]  = tabellinoDao.create( 2, 0, 0, partita, SN, atletaDao.findById("RUGORA_S"));
				TN[3]  = tabellinoDao.create( 3, 0, 0, partita, SN, atletaDao.findById("AMBROSINI_S"));
				TN[4]  = tabellinoDao.create( 4, 3, 0, partita, SN, atletaDao.findById("AVEGNO_S"));
				TN[5]  = tabellinoDao.create( 5, 4, 2, partita, SN, atletaDao.findById("GIUSTINI_S"));
				TN[6]  = tabellinoDao.create( 6, 0, 1, partita, SN, atletaDao.findById("TRONCANETT_F"));
				TN[7]  = tabellinoDao.create( 7, 5, 0, partita, SN, atletaDao.findById("PICOZZI_D"));
				TN[8]  = tabellinoDao.create( 8, 2, 0, partita, SN, atletaDao.findById("SINIGAGLIA_G"));
				TN[9]  = tabellinoDao.create( 9, 0, 2, partita, SN, atletaDao.findById("NARDINI_C"));
				TN[10] = tabellinoDao.create(10, 0, 1, partita, SN, atletaDao.findById("DICLAUDIO_L"));
				TN[11] = tabellinoDao.create(11, 2, 1, partita, SN, atletaDao.findById("STORAI_S"));
				TN[12] = tabellinoDao.create(12, 0, 1, partita, SN, atletaDao.findById("BIANCHI_A"));
				TN[13] = tabellinoDao.create(13, 0, 0, partita, SN, atletaDao.findById("BRANDIMART_M"));

				sessione.getTransaction().commit();
		
				// CARICAMENTO TABELLINI COMPLETI E AZIONI DELLA PARTITA N. 11 ( EKIPE ORIZZONTE CATANIA - VELA )

				sessione.beginTransaction();

				partita = partitaDao.findById(11L);
				SB = squadraDao.findById("CTE");
				SN = squadraDao.findById("ANV");
				partita.setTecnicoBianco(tecnicoDao.findById("MICELI_M"));
				partita.setTecnicoNero(tecnicoDao.findById("PACE_M"));

				// creazione tabellini della squadra

				TB[0]  = tabellinoDao.create( 0,26, 8, partita, SB, null);
				TB[1]  = tabellinoDao.create( 1, 0, 0, partita, SB, atletaDao.findById("GORLERO_G"));
				TB[2]  = tabellinoDao.create( 2, 2, 0, partita, SB, atletaDao.findById("IOANNOU_C"));
				TB[3]  = tabellinoDao.create( 3, 1, 2, partita, SB, atletaDao.findById("SPAMPINATO_D"));
				TB[4]  = tabellinoDao.create( 4, 4, 1, partita, SB, atletaDao.findById("VIACAVA_G"));
				TB[5]  = tabellinoDao.create( 5, 4, 0, partita, SB, atletaDao.findById("AIELLO_R"));
				TB[6]  = tabellinoDao.create( 6, 6, 0, partita, SB, atletaDao.findById("BARZON_L"));
				TB[7]  = tabellinoDao.create( 7, 2, 0, partita, SB, atletaDao.findById("GIUFFRIDA_M"));
				TB[8]  = tabellinoDao.create( 8, 4, 2, partita, SB, atletaDao.findById("MARLETTA_C"));
				TB[9]  = tabellinoDao.create( 9, 2, 1, partita, SB, atletaDao.findById("EMMOLO_G"));
				TB[10] = tabellinoDao.create(10, 1, 1, partita, SB, atletaDao.findById("VUKOVIC_J"));
				TB[11] = tabellinoDao.create(11, 0, 0, partita, SB, atletaDao.findById("RICCIOLI_I"));
				TB[12] = tabellinoDao.create(12, 0, 1, partita, SB, atletaDao.findById("LEONE_M"));
				TB[13] = tabellinoDao.create(13, 0, 0, partita, SB, atletaDao.findById("CONDORELLI_G"));
								
				TN[0]  = tabellinoDao.create( 0, 9,12, partita, SN, null);
				TN[1]  = tabellinoDao.create( 1, 0, 0, partita, SN, atletaDao.findById("UCCELLA_V"));
				TN[2]  = tabellinoDao.create( 2, 1, 2, partita, SN, atletaDao.findById("STRAPPATO_L"));
				TN[3]  = tabellinoDao.create( 3, 1, 1, partita, SN, atletaDao.findById("IVANOVA_E"));
				TN[6]  = tabellinoDao.create( 6, 0, 0, partita, SN, atletaDao.findById("BERSACCHIA_G"));
				TN[7]  = tabellinoDao.create( 7, 0, 0, partita, SN, atletaDao.findById("BARTOCCI_C"));
				TN[8]  = tabellinoDao.create( 8, 0, 2, partita, SN, atletaDao.findById("OLIVIERI_M"));
				TN[9]  = tabellinoDao.create( 9, 4, 2, partita, SN, atletaDao.findById("COLLETTA_F"));
				TN[10] = tabellinoDao.create(10, 1, 1, partita, SN, atletaDao.findById("DEMATTEIS_V"));
				TN[11] = tabellinoDao.create(11, 2, 2, partita, SN, atletaDao.findById("ALTAMURA_E"));
				TN[12] = tabellinoDao.create(12, 0, 2, partita, SN, atletaDao.findById("QUATTRINI_E"));
				TN[13] = tabellinoDao.create(13, 0, 0, partita, SN, atletaDao.findById("ANDREONI_A"));

				sessione.getTransaction().commit();

			} catch (Exception exc) {
				exc.printStackTrace(System.err);
			}

		} catch (Exception exc) {
			exc.printStackTrace(System.err);
		}
	}
}

