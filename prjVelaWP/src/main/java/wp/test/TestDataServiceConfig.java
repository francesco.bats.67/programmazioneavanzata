package wp.test;

import java.util.Properties;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import wp.app.DataServiceConfig;

/**
 * TEST DATA SERVICE CONFIG
 * Classe per customizzare la DataServiceConfig per i test di caricamento dati
 * @author Francesco Battistelli - UNIVPM
 * @version 1 agosto 2021
 */
@Configuration
@ComponentScan(basePackages = { "wp.model" })
@EnableTransactionManagement
public class TestDataServiceConfig extends DataServiceConfig {

	@Bean
	@Override
	protected Properties hibernateProperties() {
		Properties hibernateProp = super.hibernateProperties();
		hibernateProp.put("javax.persistence.schema-generation.database.action", "drop-and-create");
		return hibernateProp;
	}

}
