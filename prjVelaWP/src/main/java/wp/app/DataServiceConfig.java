package wp.app;

import java.io.IOException;
import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * [ DATA SERVICE CONFIG ]
 * Classe per la configurazione vista a lezione
 * @version dicembre 2020
 */
@Configuration
@ComponentScan(basePackages= {"wp.model"})
@EnableTransactionManagement
public class DataServiceConfig {

	@Bean
	public DataSource dataSource() {
		try {
			// stabilisce una connessione al data base e restituisce la data source
			DriverManagerDataSource ds = new DriverManagerDataSource();
			ds.setDriverClassName(com.mysql.cj.jdbc.Driver.class.getName());
			ds.setUrl("jdbc:mysql://localhost:3306/waterpolo_db?createDatabaseIfNotExist=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false");
			ds.setUsername("root");
			ds.setPassword("bats");
			return ds;
 		} catch (Exception e) {
//			logger.error("DataSource bean cannot be created!!", e);
 			return null;	
		}
	}

	// codificatore delle password (modalit� del package spring framework che effettua la cifratura delle password)
	// permette di creare password criptate anche senza controller
	@Bean
	public PasswordEncoder passwordEncoder() {
	    return new BCryptPasswordEncoder();
	}

	// bean per le propriet� hibernate
	@Bean
	protected Properties hibernateProperties() {
		Properties hibernateProp = new Properties();
		hibernateProp.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		hibernateProp.put("hibernate.format_sql", true);
		hibernateProp.put("hibernate.use_sql_comments", true);
		hibernateProp.put("hibernate.show_sql", true);
		hibernateProp.put("hibernate.max_fetch_depth", 3);
		hibernateProp.put("hibernate.jdbc.batch_size", 10);
		hibernateProp.put("hibernate.jdbc.fetch_size", 50);

		// importante, altrimenti si aspetta il DB gi� "strutturato"
		hibernateProp.put("javax.persistence.schema-generation.database.action", "none");
		
		return hibernateProp;
	}

	// bean per le session factory

	@Bean
	@Autowired
	public SessionFactory sessionFactory(DataSource dataSource, Properties hibernateProperties) throws IOException {
		LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
		sessionFactoryBean = new LocalSessionFactoryBean();
		sessionFactoryBean.setDataSource(dataSource);
		sessionFactoryBean.setPackagesToScan("wp.model");
		sessionFactoryBean.setHibernateProperties(hibernateProperties);
		sessionFactoryBean.afterPropertiesSet();
		return sessionFactoryBean.getObject();
	}

	// bean per il transaction manager
	// il transaction manager prende come argomento il session factory
	// con questo costruisce un hibernate transaction manager che invoca il bean al momento opportuno 
	@Bean
	public PlatformTransactionManager transactionManager(SessionFactory sessionFactory) throws IOException {
		return new HibernateTransactionManager(sessionFactory);
	}

}
