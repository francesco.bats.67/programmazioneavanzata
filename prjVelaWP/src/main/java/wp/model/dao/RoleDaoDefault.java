package wp.model.dao;

import org.springframework.stereotype.Repository;
//import org.springframework.transaction.annotation.Transactional;
import wp.model.entities.Role;

/**
 * [ ROLE DETAILS DAO DEFAULT ]
 * Classe che modella il Data Object Access di default ruolo utente dell'applicazione
 * @author Esempio visto a lezione - UNIVPM
 * @version dicembre 2020
 */
@Repository("DaoRuolo")
public class RoleDaoDefault extends DaoDefault implements RoleDao {

	@Override
	public Role create(String name) {
		Role r = new Role();
		r.setName(name);
		this.getSession().save(r);
		return r;
	}

	@Override
	public Role update(Role role) {
		return (Role)this.getSession().merge(role);
	}

	@Override
	public void delete(Role role) {
		this.getSession().delete(role);
	}

}
