package wp.model.dao;

//import java.time.LocalDate;
import org.hibernate.Session;
import wp.model.entities.User;

/**
 * [ USER DETAILS DAO ]
 * Interfaccia del Data Object Access utente dell'applicazione
 * @author Esempio visto a lezione - UNIVPM
 * @version dicembre 2020
 */
public interface UserDetailsDao {

	Session getSession();
	public void setSession(Session session);

	// operazioni di ricerca
	User findUserByUsername(String username);
	
	// operazioni CRUD
	User create(String username, String password, boolean isEnabled);	
	User update(User user);	
	void delete(User user);

	// altri metodi
	public String encryptPassword(String password);

}
