package wp.model.dao;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import wp.model.entities.Atleta;
import wp.model.entities.Azione;
import wp.model.entities.Partita;
import wp.model.entities.Squadra;
import wp.model.entities.Tecnico;

/**
 * [ SQUADRA DAO DEFAULT ]
 * Classe che modella il Data Object Access squadra di pallanuoto
 * @author Francesco Battistelli - UNIVPM
 * @version 8 agosto 2021
 */
@Repository("DaoSquadra")
public class SquadraDaoDefault extends DaoDefault implements SquadraDao {

	// --- operazioni di ricerca  ----------------------------------------------
	
	@Override
	public List<Squadra> findAll() {
		return getSession().createQuery("FROM Squadra s", Squadra.class).getResultList();
	}

	@Override
	public Squadra findById(String id) {
		return getSession().find(Squadra.class, id);
	}

	@Override
	public List<Partita> findPartite(Squadra squadra) {
		return getSession().createQuery("FROM Partita p WHERE p.squadraBianco = :squadra OR p.squadraNero = :squadra", Partita.class).setParameter("squadra",squadra).getResultList();
	}

	@Override
	public Set<Partita> findPartiteBianco(Squadra squadra) {
		Query query = this.getSession().createQuery("FROM Partita p JOIN FETCH p.squadraBianco WHERE p.squadraBianco = :squadra", Partita.class);
		return new HashSet<Partita>(query.setParameter("squadra", squadra).getResultList());
	}

	@Override
	public Set<Partita> findPartiteNero(Squadra squadra) {
		Query query = this.getSession().createQuery("FROM Partita p JOIN FETCH p.squadraNero WHERE p.squadraNero = :squadra", Partita.class);
		return new HashSet<Partita>(query.setParameter("squadra", squadra).getResultList());
	}
	
	@Override
	public List<Atleta> findAtleti(Squadra squadra) {
		return this.getSession().createQuery("SELECT DISTINCT a FROM Atleta a JOIN FETCH a.squadre s WHERE s = :squadra ORDER BY a.idAtleta", Atleta.class).setParameter("squadra", squadra).getResultList();
	}

	@Override
	public List<Atleta> findAtletiExtra(Squadra squadra) {
		return this.getSession().createQuery("SELECT DISTINCT a FROM Atleta a LEFT JOIN FETCH a.squadre s WHERE s = null OR NOT s = :squadra ORDER BY a.idAtleta", Atleta.class).setParameter("squadra", squadra).getResultList();
	}

	// --- operazioni Create Read Update Delete --------------------------------

	@Override
	public Squadra create(String id, String nome, String denominazione, String luogo) {
		Squadra squadra = new Squadra();
		squadra.setIdSquadra(id);
		squadra.setNomeSquadra(nome);
		squadra.setDenominazioneSquadra(denominazione);
		squadra.setLuogoSquadra(luogo);
		this.getSession().save(squadra);
		return squadra;
	}

	@Override
	public Squadra update(Squadra squadra) {
		Squadra squadraMerged = (Squadra)this.getSession().merge(squadra);
		return squadraMerged;
	}

	@Override
	public void delete(Squadra squadra) {
		this.getSession().delete(squadra);
	}

}
