package wp.model.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import wp.model.entities.Arbitro;
import wp.model.entities.Atleta;
import wp.model.entities.Partita;

/**
 * [ ARBITRO DAO DEFAULT ]
 * Classe che modella il Data Object Access arbitro di pallanuoto
 * @author Francesco Battistelli - UNIVPM
 * @version 8 agosto 2021
 */
@Repository("DaoArbitro")
public class ArbitroDaoDefault extends DaoDefault implements ArbitroDao {

	// --- operazioni di ricerca  ----------------------------------------------

	@Override
	public List<Arbitro> findAll() {
		return getSession().createQuery("FROM Arbitro ar", Arbitro.class).getResultList();
	}

	@Override
	public Arbitro findById(String id) {
		return getSession().find(Arbitro.class, id);
	}

	@Override
	public Arbitro findByName(String nome, String cognome) {
		return getSession().createQuery("FROM Arbitro ar WHERE ar.nome = :nome AND ar.cognome = :cognome", Arbitro.class).setParameter("nome",nome).setParameter("cognome",cognome).getSingleResult();
	}

	@Override
	public List<Partita> findPartite(Arbitro arbitro) {
		return getSession().createQuery("FROM Partita p WHERE p.arbitro1 = :arbitro OR p.arbitro2 = :arbitro", Partita.class).setParameter("arbitro",arbitro).getResultList();
	}

	// --- operazioni Create Read Update Delete --------------------------------

	@Override
	public Arbitro create(String id, String nome, String cognome, char sesso, String provenienza) {
		Arbitro arbitro = new Arbitro();
		arbitro.setIdArbitro(id);
		arbitro.setNome(nome);
		arbitro.setCognome(cognome);
		arbitro.setSesso(sesso);
		arbitro.setProvenienza(provenienza);
		this.getSession().save(arbitro);
		return arbitro;
	}

	@Override
	public Arbitro update(Arbitro arbitro) {
		return (Arbitro)this.getSession().merge(arbitro);
	}

	@Override
	public void delete(Arbitro arbitro) {
		this.getSession().delete(arbitro);
	}
		
}

