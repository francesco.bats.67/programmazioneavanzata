package wp.model.dao;

import java.util.List;

import org.hibernate.Session;

import wp.model.entities.Atleta;
import wp.model.entities.Partita;
import wp.model.entities.Squadra;
import wp.model.entities.Tabellino;

/**
 * [ TABELLINO DAO ]
 * Interfaccia del Data Object Access tabellino di pallanuoto
 * @author Francesco Battistelli - UNIVPM
 * @version 8 agosto 2021
 */
public interface TabellinoDao {

	public Session getSession();
	public void setSession(Session session);

	// operazioni di ricerca
	public List<Tabellino> findAll();
	public Tabellino findById(Long id);
	public List<Tabellino> findTabelliniGara(Partita partita, Squadra squadra);
	public Tabellino findTabellinoGaraCalottina(Partita partita, Squadra squadra, int calottina);
	public List<Tabellino> findTabelliniAtleta(Atleta atleta);

	// operazioni CRUD	
	public Tabellino create(int numeroCalottina, boolean iniziale, float minuti, int reti, int tiri, int falli, boolean esclusione,
			     int[] statTiro, int[] statVarie, int[] statFalli, int[] statPortiere,
			     Partita partita, Squadra squadra, Atleta atleta);
	public Tabellino create(int numeroCalottina, int reti, int falli, Partita partita, Squadra squadra, Atleta atleta);
	public Tabellino create(int numeroCalottina, Partita partita, Squadra squadra, Atleta atleta);
	public Tabellino update(Tabellino tabellino);	
	public void delete(Tabellino tabellino);

	// metodi di impostazione attributi
	public void setPortiere(Tabellino tabellino);	
	public void addFallo(Tabellino difensore, String codiceFallo);
	public void addTiro(Tabellino attaccante, Tabellino portiere, String codiceTiro, char esitoTiro);

}

