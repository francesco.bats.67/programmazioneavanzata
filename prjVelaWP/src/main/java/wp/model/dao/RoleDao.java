package wp.model.dao;

import org.hibernate.Session;
import wp.model.entities.Role;

/**
 * [ ROLE DAO ]
 * Interfaccia del Data Object Access ruolo utente dell'applicazione
 * @author Esempio visto a lezione - UNIVPM
 * @version dicembre 2020
 */
public interface RoleDao {
	
	Session getSession();
	public void setSession(Session session);

	// operazioni CRUD
	Role create(String name);
	Role update(Role role);	
	void delete(Role role);

}
