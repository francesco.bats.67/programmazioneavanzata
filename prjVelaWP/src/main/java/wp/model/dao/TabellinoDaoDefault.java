package wp.model.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import wp.model.entities.Atleta;
import wp.model.entities.Partita;
import wp.model.entities.Squadra;
import wp.model.entities.Tabellino;

/**
 * [ TABELLINO DAO DEFAULT ]
 * Classe che modella il Data Object Access tabellino di pallanuoto
 * @author Francesco Battistelli - UNIVPM
 * @version 8 agosto 2021
 */
@Repository("DaoTabellino")
public class TabellinoDaoDefault extends DaoDefault implements TabellinoDao {

	// --- operazioni di ricerca  ----------------------------------------------
	
	@Override
	public List<Tabellino> findAll() {
		return getSession().createQuery("FROM Tabellino tb", Tabellino.class).getResultList();
	}
	
	@Override
	public Tabellino findById(Long id) {
		return getSession().find(Tabellino.class, id);
	}
	
	@Override
	public List<Tabellino> findTabelliniGara(Partita partita, Squadra squadra) {
		return this.getSession().createQuery("FROM Tabellino tb WHERE tb.partita = :partita AND tb.squadra = :squadra", Tabellino.class).setParameter("partita",partita).setParameter("squadra",squadra).getResultList();
	}
	
	@Override
	public Tabellino findTabellinoGaraCalottina(Partita partita, Squadra squadra, int calottina) {
		return this.getSession().createQuery("FROM Tabellino tb WHERE tb.partita = :partita AND tb.squadra = :squadra AND tb.numeroCalottina = :calottina", Tabellino.class).setParameter("partita",partita).setParameter("squadra",squadra).setParameter("calottina",calottina).getSingleResult();
	}
	
	@Override
	public List<Tabellino> findTabelliniAtleta(Atleta atleta) {
		return this.getSession().createQuery("FROM Tabellino tb WHERE tb.atleta = :atleta", Tabellino.class).setParameter("Atleta",atleta).getResultList();
	}

	// --- operazioni Create Read Update Delete --------------------------------

	@Override
	public Tabellino create(int numeroCalottina, boolean iniziale, float minuti,
			 int reti, int tiri, int falli, boolean portiere,
		     int[] statReti, int[] statTiri, int[] statFalli, int[] statPorta,
		     Partita partita, Squadra squadra, Atleta atleta) {
		Tabellino tabellino = new Tabellino();
		tabellino.setNumeroCalottina(numeroCalottina);
		tabellino.setIniziale(iniziale);
		tabellino.setMinuti(minuti);
		tabellino.setReti(reti);
		tabellino.setTiri(tiri);
		tabellino.setFalli(falli);
		tabellino.setPortiere(portiere);
		tabellino.setStatReti(statReti);
		tabellino.setStatTiri(statTiri);
		tabellino.setStatFalli(statFalli);
		tabellino.setStatPorta(statPorta);
		tabellino.setPartita(partita);
		tabellino.setSquadra(squadra);
		tabellino.setAtleta(atleta);
		this.getSession().save(tabellino);
		return tabellino;
	}
	
	public Tabellino create(int numeroCalottina, int reti, int falli, Partita partita, Squadra squadra, Atleta atleta) {
		Tabellino tabellino = new Tabellino();
		tabellino.setNumeroCalottina(numeroCalottina);
//		tabellino.setIniziale(iniziale);
//		tabellino.setMinuti(minuti);
		tabellino.setReti(reti);
//		tabellino.setTiri(tiri);
		tabellino.setFalli(falli);
//		tabellino.setPortiere(portiere);
//		tabellino.setStatReti(statReti);
//		tabellino.setStatTiri(statTiri);
//		tabellino.setStatFalli(statFalli);
//		tabellino.setStatPorta(statPorta);
		tabellino.setPartita(partita);
		tabellino.setSquadra(squadra);
		tabellino.setAtleta(atleta);
		this.getSession().save(tabellino);
		return tabellino;
	}
	
	public Tabellino create(int numeroCalottina, Partita partita, Squadra squadra, Atleta atleta) {
		Tabellino tabellino = new Tabellino();
		tabellino.setNumeroCalottina(numeroCalottina);
//		tabellino.setIniziale(iniziale);
//		tabellino.setMinuti(minuti);
//		tabellino.setReti(reti);
//		tabellino.setTiri(tiri);
//		tabellino.setFalli(falli);
//		tabellino.setPortiere(portiere);
//		tabellino.setStatReti(statReti);
//		tabellino.setStatTiri(statTiri);
//		tabellino.setStatFalli(statFalli);
//		tabellino.setStatPorta(statPorta);
		tabellino.setPartita(partita);
		tabellino.setSquadra(squadra);
		tabellino.setAtleta(atleta);
		this.getSession().save(tabellino);
		return tabellino;
	}

	@Override
	public Tabellino update(Tabellino tabellino) {
		return (Tabellino)this.getSession().merge(tabellino);
	}

	@Override
	public void delete(Tabellino tabellino) {
		this.getSession().delete(tabellino);
	}

	// --- metodi di impostazione attributi ------------------------------------

	@Override
	public void setPortiere(Tabellino tabellino) {
		if ( tabellino.isPortiere() == false ) {
			tabellino.setStatPorta(new int[] {0,0,0,0,0,0});
			tabellino.setPortiere(true);
		}
	}

	@Override
	public void addFallo(Tabellino difensore, String codiceFallo) {
		int indiceTipo = 9;
		switch (codiceFallo) {
			case "EC":	indiceTipo = 0;		break;
			case "EV": 	indiceTipo = 1;		break;
			case "ER":	indiceTipo = 2;		break;
			case "EO":	indiceTipo = 3;		break;
			case "TR":	indiceTipo = 4;		break;
		}		
		difensore.addFallo(indiceTipo);
		this.update(difensore);
	}
	
	@Override
	public void addTiro(Tabellino attaccante, Tabellino portiere, String codiceTiro, char esitoTiro) {
		int indiceTiro = 9;
		switch (codiceTiro) {
			case "RG":	indiceTiro = 0;		break;
			case "SN":	indiceTiro = 1;		break;
			case "AZ":	indiceTiro = 2;		break;
			case "CB":	indiceTiro = 3;		break;
			case "6M":	indiceTiro = 4;		break;
			case "CF":	indiceTiro = 5;		break;
		}
		if ( esitoTiro == 'R' ) {
			attaccante.addReteSegnata(indiceTiro);
			portiere.addReteSubita(indiceTiro);
		} else {
			attaccante.addTiroSbagliato(indiceTiro);
			if ( esitoTiro == 'P' ) {
				portiere.addTiroParato(indiceTiro);
			}
		}
	}

}


