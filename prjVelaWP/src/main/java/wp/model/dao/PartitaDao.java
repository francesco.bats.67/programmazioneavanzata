package wp.model.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Session;

import wp.model.entities.Partita;
import wp.model.entities.Squadra;
import wp.model.entities.Azione;
import wp.model.entities.Tabellino;
import wp.model.entities.Tecnico;

/**
 * [ PARTITA DAO ]
 * Interfaccia del Data Object Access partita di pallanuoto femminile
 * @author Francesco Battistelli - UNIVPM
 * @version 30 luglio 2021
 */
public interface PartitaDao {
	
	public Session getSession();
	public void setSession(Session session);

	// operazioni di ricerca e lettura
	public List<Partita> findAll();
	public List<Partita> findAllOrderDesc();
	public Partita findById(Long id);
	public Squadra findSquadraBianco(Partita partita);
	public Squadra findSquadraNero(Partita partita);
	public Tabellino findPortiereBianco(Partita partita);
	public Tabellino findPortiereNero(Partita partita);
	public String getDataEstesa(Partita partita);

	// operazioni CRUD
	public Partita create(Date data, String competizione, Squadra squadraB, Squadra squadraN);
	public Partita update(Partita partita);
	void delete(Partita partita);
	
	// metodi di impostazione attributi
	public void setFormazioniPartita(Partita partita);
	public void setTecnici(Partita partita, Tecnico tecnicoBianco, Tecnico tecnicoNero);
	public void setPortiereBianco(Partita partita, Tabellino tabellino);
	public void setPortiereNero(Partita partita, Tabellino tabellino);
	public void setDisputaPartita(Partita partita);
	public void setInizioPartita(Partita partita);
	public void setPeriodoGioco(Partita partita);
	public void setFinePartita(Partita partita);
	public void setColoreAttacco(Partita partita, String attacco);
	public void addReteSegnata(Partita partita, String colore);
	public void addSupNumRete(Partita partita, String colore);
	public void addSupNumNoRete(Partita partita, String colore);
	public void addAzione(Partita partita, Azione azione);

}
