package wp.model.dao;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import wp.model.entities.Partita;
import wp.model.entities.Squadra;
import wp.model.entities.Azione;
import wp.model.entities.Tabellino;
import wp.model.entities.Tecnico;

/**
 * [ PARTITA DAO DEFAULT ]
 * Classe che modella il Data Object Access partita di pallanuoto
 * @author Francesco Battistelli - UNIVPM
 * @version 8 agosto 2021
 */
@Repository("DaoPartita")
public class PartitaDaoDefault extends DaoDefault implements PartitaDao {

	// --- operazioni di ricerca e lettura dati---------------------------------
	
	@Override
	public List<Partita> findAll() {
		return getSession().createQuery("FROM Partita p", Partita.class).getResultList();
	}
	
	@Override
	public List<Partita> findAllOrderDesc() {
		return getSession().createQuery("FROM Partita p ORDER BY idPartita DESC", Partita.class).getResultList();
	}

	@Override
	public Partita findById(Long id) {
		return getSession().find(Partita.class, id);
	}
	
	@Override
	public Squadra findSquadraBianco(Partita partita) {
		return partita.getSquadraBianco();
	}

	@Override
	public Squadra findSquadraNero(Partita partita) {
		return partita.getSquadraNero();
	}
	
	@Override
	public Tabellino findPortiereBianco(Partita partita) {
		return partita.getPortiereBianco();
//		this.getSession().merge(partita);
	}
	
	@Override
	public Tabellino findPortiereNero(Partita partita) {
		return partita.getPortiereNero();
//		this.getSession().merge(partita);
	}
	
	@Override
	public String getDataEstesa(Partita partita) {
		return partita.getDataEstesa();
//		this.getSession().merge(partita);
	}

	// --- operazioni Create Read Update Delete --------------------------------

	@Override
	public Partita create(Date data, String competizione, Squadra squadraB, Squadra squadraN) {
		Partita partita = new Partita();
		partita.setData(data);
		partita.setCompetizione(competizione);
		partita.setSquadraBianco(squadraB);
		partita.setSquadraNero(squadraN);
		this.getSession().save(partita);
		return partita;
	}

	@Override
	public Partita update(Partita partita) {
		return (Partita)this.getSession().merge(partita);
	}

	@Override
	public void delete(Partita partita) {
		this.getSession().delete(partita);
	}

	// --- metodi di impostazione attributi ------------------------------------
	
	@Override
	public void setFormazioniPartita(Partita partita) {
		partita.setEsitoPartita("F");
//		this.getSession().merge(partita);
	}
	
	@Override
	public void setTecnici(Partita partita, Tecnico tecnicoBianco, Tecnico tecnicoNero) {
		partita.setTecnicoBianco(tecnicoBianco);
		partita.setTecnicoNero(tecnicoNero);
	};

	@Override
	public void setPortiereBianco(Partita partita, Tabellino tabellino) {
		partita.setPortiereBianco(tabellino);
//		this.getSession().merge(partita);
	}
	
	@Override
	public void setPortiereNero(Partita partita, Tabellino tabellino) {
		partita.setPortiereNero(tabellino);
//		this.getSession().merge(partita);
	}

	@Override
	public void setDisputaPartita(Partita partita) {
		partita.setParzialiBianco(new int[] {0,0,0,0});
		partita.setParzialiNero(new int[] {0,0,0,0});
		partita.setEsitoPartita("F");
//		this.getSession().merge(partita);
	}

	@Override
	public void setInizioPartita(Partita partita) {
		partita.setEsitoPartita("G");
//		this.getSession().merge(partita);
	}
	
	@Override
	public void setPeriodoGioco(Partita partita) {
		int periodoFinito = partita.getPeriodoGioco();
		partita.setPeriodoGioco( periodoFinito + 1 );
//		this.getSession().merge(partita);
	}	

	@Override
	public void setFinePartita(Partita partita) {
		partita.setEsitoPartita("T");
		partita.setPeriodoGioco(0);
//		this.getSession().merge(partita);
	}

	@Override
	public void setColoreAttacco(Partita partita, String attacco) {
		partita.setAttacco(attacco);
	}

	@Override
	public void addReteSegnata(Partita partita, String colore) {
		if (colore.equals("bianco")) {
			partita.addReteBianco();
		} else if (colore.equals("nero")) {
			partita.addReteNero();
		}
		this.getSession().merge(partita);
	}
	
	@Override
	public void addSupNumRete(Partita partita, String colore) {
		if (colore.equals("bianco")) {
			partita.addSupNumReteBianco();
		} else if (colore.equals("nero")) {
			partita.addSupNumReteNero();
		}
		this.getSession().merge(partita);
	}
	
	@Override
	public void addSupNumNoRete(Partita partita, String colore) {
		if (colore.equals("bianco")) {
			partita.addSupNumNoReteBianco();
		} else if (colore.equals("nero")) {
			partita.addSupNumNoReteNero();
		}
		this.getSession().merge(partita);
	}
	
	@Override
	public void addAzione(Partita partita, Azione azione) {
		partita.addAzione(azione);
		this.getSession().merge(partita);
	}

}
