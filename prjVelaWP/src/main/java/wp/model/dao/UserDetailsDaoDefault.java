package wp.model.dao;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;
//import org.springframework.transaction.annotation.Transactional;
import wp.model.entities.User;

/**
 * [ USER DETAILS DAO DEFAULT ]
 * Classe che modella il Data Object Access di default utente dell'applicazione
 * @author Esempio visto a lezione - UNIVPM
 * @version dicembre 2020
 */
@Repository("DaoUtente")
public class UserDetailsDaoDefault extends DaoDefault implements UserDetailsDao {

	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Override
	public User findUserByUsername(String username) {
		return this.getSession().get(User.class, username);
	}

	@Override
	public User create(String username, String password, boolean isEnabled) {
		User u = new User();
		u.setUsername(username);
		u.setPassword(password);
		u.setEnabled(isEnabled);
		this.getSession().save(u);
		return u;
	}

	@Override
	public User update(User user) {
		return (User)this.getSession().merge(user);
	}

	@Override
	public void delete(User user) {
		this.getSession().delete(user);
		
	}

	@Override
	public String encryptPassword(String password) {
		return this.passwordEncoder.encode(password);
	}

}