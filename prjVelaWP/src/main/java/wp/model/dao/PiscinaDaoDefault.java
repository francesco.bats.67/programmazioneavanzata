package wp.model.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import wp.model.entities.Arbitro;
import wp.model.entities.Atleta;
import wp.model.entities.Partita;
import wp.model.entities.Piscina;
import wp.model.entities.Squadra;

/**
 * [ PISCINA DAO DEFAULT ]
 * Classe che modella il Data Object Access piscina di pallanuoto
 * @author Francesco Battistelli - UNIVPM
 * @version 8 agosto 2021
 */
@Repository("DaoPiscina")
public class PiscinaDaoDefault extends DaoDefault implements PiscinaDao {

	// --- operazioni di ricerca  ----------------------------------------------

	@Override
	public List<Piscina> findAll() {
		return getSession().createQuery("FROM Piscina ps", Piscina.class).getResultList();
	}

	@Override
	public Piscina findById(String id) {
		return getSession().find(Piscina.class, id);
	}

	@Override
	public List<Partita> findPartite(Piscina piscina) {
		return getSession().createQuery("FROM Partita p WHERE p.piscina = :piscina", Partita.class).setParameter("piscina",piscina).getResultList();
	}

	// --- operazioni Create Read Update Delete --------------------------------

	@Override
	public Piscina create(String id, String nome, String luogo) {
		Piscina piscina = new Piscina();
		piscina.setIdPiscina(id);
		piscina.setNomePiscina(nome);
		piscina.setLuogoPiscina(luogo);
		this.getSession().save(piscina);
		return piscina;
	}

	@Override
	public Piscina update(Piscina piscina) {
		return (Piscina)this.getSession().merge(piscina);
	}

	@Override
	public void delete(Piscina piscina) {
		this.getSession().delete(piscina);
	}

}
