package wp.model.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import wp.model.entities.Atleta;
import wp.model.entities.Tecnico;

/**
 * [ TECNICO DAO DEFAULT ]
 * Classe che modella il Access Data Object tecnico di pallanuoto
 * @author Francesco Battistelli - UNIVPM
 * @version 8 agosto 2021
 */
@Repository("DaoTecnico")
public class TecnicoDaoDefault extends DaoDefault implements TecnicoDao {

	// --- operazioni di ricerca  ----------------------------------------------
	
	@Override
	public List<Tecnico> findAll() {
		return getSession().createQuery("FROM Tecnico t", Tecnico.class).getResultList();
	}

	@Override
	public Tecnico findById(String id) {
		return getSession().find(Tecnico.class, id);
	}

	@Override
	public Tecnico findByName(String nome, String cognome) {
		return getSession().createQuery("FROM Tecnico t WHERE t.nome = :nome AND t.cognome = :cognome", Tecnico.class).setParameter("nome",nome).setParameter("cognome",cognome).getSingleResult();
	}

	// --- operazioni Create Read Update Delete --------------------------------

	@Override
	public Tecnico create(String id, String nome, String cognome, char sesso, String nazione) {
		Tecnico tecnico = new Tecnico();
		tecnico.setIdTecnico(id);
		tecnico.setNome(nome);
		tecnico.setCognome(cognome);
		tecnico.setSesso(sesso);
		tecnico.setNazione(nazione);
		this.getSession().save(tecnico);
		return tecnico;
	}

	@Override
	public Tecnico update(Tecnico tecnico) {
		return (Tecnico)this.getSession().merge(tecnico);
	}

	@Override
	public void delete(Tecnico tecnico) {
		this.getSession().delete(tecnico);
	}
	
}

