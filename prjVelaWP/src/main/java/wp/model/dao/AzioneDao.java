package wp.model.dao;

import java.util.List;

import org.hibernate.Session;

import wp.model.entities.Atleta;
import wp.model.entities.Azione;
import wp.model.entities.Partita;
import wp.model.entities.Squadra;

/**
 * [ AZIONE DAO ]
 * Interfaccia del Data Object Access azione di gioco di un partita di pallanuoto femminile
 * @author Francesco Battistelli - UNIVPM
 * @version 8 agosto 2021
 */
public interface AzioneDao {

	public Session getSession();
    public void setSession(Session session);

	// operazioni di ricerca
    public List<Azione> findAll();
    public Azione findById(Long id);
    public List<Azione> findAzioniGara(Partita partita);
    public List<Azione> findUltimeAzioniGara(Partita partita);

	// operazioni CRUD
	Azione create(boolean supNum, String tipo, String esito, String risultato, int calottina,
	              Partita partita, int[] tempoGioco, Squadra squadra, Atleta atleta);
	Azione update(Azione azione);
	void delete(Azione azione);

}
