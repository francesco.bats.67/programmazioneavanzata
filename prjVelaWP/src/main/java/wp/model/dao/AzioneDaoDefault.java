package wp.model.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import wp.model.entities.Atleta;
import wp.model.entities.Azione;
import wp.model.entities.Partita;
import wp.model.entities.Squadra;

/**
 * [ AZIONE DAO DEFAULT ]
 * Classe che modella il Data Object Access azione di pallanuoto
 * @author Francesco Battistelli - UNIVPM
 * @version 8 agosto 2021
 */
@Repository("DaoAzione")
public class AzioneDaoDefault extends DaoDefault implements AzioneDao {

	// --- operazioni di ricerca  ----------------------------------------------

	@Override
	public List<Azione> findAll() {
		return getSession().createQuery("FROM Azione az", Azione.class).getResultList();
	}

	@Override
	public Azione findById(Long id) {
		return getSession().find(Azione.class, id);
	}

	@Override
	public List<Azione> findAzioniGara(Partita partita) {
		return this.getSession().createQuery("FROM Azione az WHERE az.partita = :partita", Azione.class).setParameter("partita",partita).getResultList();
	}
	
	@Override
	public List<Azione> findUltimeAzioniGara(Partita partita) {
		return this.getSession().createQuery("SELECT az FROM Azione az WHERE az.partita = :partita ORDER BY az.idAzione DESC", Azione.class).setParameter("partita",partita).getResultList();
	}

	// --- operazioni Create Read Update Delete --------------------------------

	@Override
	public Azione create(boolean supNum, String tipo, String esito, String risultato, int calottina,
			Partita partita, int[] tempoGioco, Squadra squadra, Atleta atleta) {
		Azione azione = new Azione();
		azione.setSupNum(supNum);
		azione.setTipoAzione(tipo);
		azione.setEsitoAzione(esito);
		azione.setRisultato(risultato);
		azione.setNumeroCalottina(calottina);
		azione.setTempoGioco(tempoGioco);
		azione.setPartita(partita);
		azione.setSquadra(squadra);
		azione.setAtleta(atleta);
		this.getSession().save(azione);
		return azione;
	}

	@Override
	public Azione update(Azione azione) {
		return (Azione)this.getSession().merge(azione);
	}

	@Override
	public void delete(Azione azione) {
		this.getSession().delete(azione);
	}

}
