package wp.model.dao;

import java.util.List;
import java.util.Set;

import org.hibernate.Session;

import wp.model.entities.Atleta;
import wp.model.entities.Azione;
import wp.model.entities.Partita;
import wp.model.entities.Squadra;
import wp.model.entities.Tecnico;

/**
 * [ SQUADRA DAO ]
 * Interfaccia del Data Object Access societ� di pallanuoto femminile
 * @author Francesco Battistelli - UNIVPM
 * @version 8 agosto 2021
 */
public interface SquadraDao {

	public Session getSession();
	public void setSession(Session session);
	
	// operazioni di ricerca	
	public List<Squadra> findAll();	
	public Squadra findById(String id);
	public List<Partita> findPartite(Squadra squadra);
	public Set<Partita> findPartiteBianco(Squadra squadra);
	public Set<Partita> findPartiteNero(Squadra squadra);
	public List<Atleta> findAtleti(Squadra squadra);
	public List<Atleta> findAtletiExtra(Squadra squadra);
	
	// operazioni CRUD
	Squadra create(String id, String nome, String denominazione, String luogo);
	Squadra update(Squadra squadra);
	void delete(Squadra squadra);

}
