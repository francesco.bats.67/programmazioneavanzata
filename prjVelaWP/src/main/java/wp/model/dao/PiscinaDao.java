package wp.model.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Session;

import wp.model.entities.Piscina;
import wp.model.entities.Partita;

/**
 * [ PISCINA DAO ]
 * Interfaccia del Data Object Access piscina dove si disputa una partita di pallanuoto
 * @author Francesco Battistelli - UNIVPM
 * @version 8 agosto 2021
 */
public interface PiscinaDao {

	public Session getSession();
	public void setSession(Session session);

	// operazioni di ricerca
	public List<Piscina> findAll();
	public Piscina findById(String id);
	public List<Partita> findPartite(Piscina piscina);
	
	// operazioni CRUD
	public Piscina create(String id, String nome, String luogo);	
	public Piscina update(Piscina piscina);	
	public void delete(Piscina piscina);

}
