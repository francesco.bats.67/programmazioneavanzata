package wp.model.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import wp.model.entities.Atleta;

/**
 * [ ATLETA DAO DEFAULT ]
 * Classe che modella il Data Object Access atleta di pallanuoto
 * @author Francesco Battistelli - UNIVPM
 * @version 8 agosto 2021
 */
@Repository("DaoAtleta")
public class AtletaDaoDefault extends DaoDefault implements AtletaDao {

	// --- operazioni di ricerca  ----------------------------------------------

	@Override
	public List<Atleta> findAll() {
		return getSession().createQuery("FROM Atleta a", Atleta.class).getResultList();
	}

	@Override
	public Atleta findById(String id) {
		return getSession().find(Atleta.class, id);
	}

	@Override
	public Atleta findByName(String nome, String cognome) {
		return getSession().createQuery("FROM Atleta a WHERE a.nome = :nome AND a.cognome = :cognome", Atleta.class).setParameter("nome",nome).setParameter("cognome",cognome).getSingleResult();
	}

	// --- operazioni Create Read Update Delete --------------------------------

	@Override
	public Atleta create(String id, String nome, String cognome, char sesso, String nazione, String codiceFin, int annoNascita) {
		Atleta atleta = new Atleta();
		atleta.setIdAtleta(id);
		atleta.setNome(nome);
		atleta.setCognome(cognome);
		atleta.setSesso(sesso);
		atleta.setNazione(nazione);
		atleta.setAnnoNascita(annoNascita);
		this.getSession().save(atleta);
		return atleta;
	}

	@Override
	public Atleta update(Atleta atleta) {
		return (Atleta)this.getSession().merge(atleta);
	}
	
	@Override
	public void delete(Atleta atleta) {
		this.getSession().delete(atleta);
	}

}

