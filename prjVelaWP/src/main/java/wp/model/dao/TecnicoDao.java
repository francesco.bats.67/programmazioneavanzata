package wp.model.dao;

import java.util.List;

import org.hibernate.Session;

import wp.model.entities.Arbitro;
import wp.model.entities.Tecnico;

/**
 * [ TECNICO DAO ]
 * Interfaccia del Data Object Access allenatore di pallanuoto
 * @author Francesco Battistelli - UNIVPM
 * @version 8 agosto 2021
 */
public interface TecnicoDao {

	public Session getSession();
	public void setSession(Session session);
	
	// operazioni di ricerca
	public List<Tecnico> findAll();	
	public Tecnico findById(String id);	
	public Tecnico findByName(String nome, String cognome);

	// operazioni CRUD
	public Tecnico create(String id, String nome, String cognome, char sesso, String nazione);
	public Tecnico update(Tecnico tecnico);
	public void delete(Tecnico tecnico);

}
