package wp.model.dao;

import javax.annotation.Resource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 * [ DAO DEFAULT ]
 * Classe astratta che condivide metodi ottenuti dalla Session Factory alle classi Repository 
 * @author Esempio visto a lezione - UNIVPM
 * @version dicembre 2020
 */
public abstract class DaoDefault {

	private SessionFactory sessionFactory;
	private Session session;

	// ritorna un riferimento alla session factory
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	// effettua una iniezione delle dipendenze
	// inizializzata con la bean sessionFactory definita nel file di configurazione
	@Resource(name = "sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void setSession(Session session) {
		this.session = session; 
	}

	// permette di lavorare con una sessione condivisa con gli altri Dao
	// restituisce la sessione esistente, se non esiste viene generata una nuova sessione
	public Session getSession() {

		Session session = this.session;

		if (session == null) {
			session = this.sessionFactory.getCurrentSession();
		}
		return session;
	}

}
