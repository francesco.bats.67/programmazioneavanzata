package wp.model.dao;

import java.util.List;

import org.hibernate.Session;

import wp.model.entities.Arbitro;
import wp.model.entities.Partita;

/**
 * [ ARBITRO DAO ]
 * Interfaccia del Data Object Access arbitro di pallanuoto
 * @author Francesco Battistelli - UNIVPM
 * @version 8 agosto 2021
 */
public interface ArbitroDao {

	public Session getSession();
	public void setSession(Session session);
	
	// operazioni di ricerca
	public List<Arbitro> findAll();	
	public Arbitro findById(String id);	
	public Arbitro findByName(String nome, String cognome);
	public List<Partita> findPartite(Arbitro arbitro);
	
	// operazioni CRUD
	public Arbitro create(String id, String nome, String cognome, char sesso, String provenienza);
	public Arbitro update(Arbitro arbitro);
	public void delete(Arbitro arbitro);
	
}
