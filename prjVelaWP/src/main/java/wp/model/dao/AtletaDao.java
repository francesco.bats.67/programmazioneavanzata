package wp.model.dao;

import java.util.List;

import org.hibernate.Session;

import wp.model.entities.Atleta;

/**
 * [ ATLETA DAO ]
 * Interfaccia del Data Object Access giocatrice di pallanuoto
 * @author Francesco Battistelli - UNIVPM
 * @version 8 agosto 2021
 */
public interface AtletaDao {
	
	public Session getSession();
	public void setSession(Session session);

	// operazioni di ricerca
	public List<Atleta> findAll();	
	public Atleta findById(String id);	
	public Atleta findByName(String nome, String cognome);

	// operazioni CRUD
	public Atleta create(String id, String nome, String cognome, char sesso, String nazione, String codiceFin, int annoNascita);	
	public Atleta update(Atleta atleta);
	public void delete(Atleta atleta);

}
