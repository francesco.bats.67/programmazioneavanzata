package wp.model;

import javax.persistence.MappedSuperclass;

/**
 * [ PERSONA ]
 * Classe che modella la superclasse PERSONA
 * @author Francesco Battistelli - UNIVPM
 * @version 27 luglio 2021
 */
@MappedSuperclass
public abstract class Persona {
	
	// --- attributi dell'entit�-----------------------------------------------
	
	private String nome;
	private String cognome;
	private char sesso;
	private String nazione;

	// --- metodi GETTER -------------------------------------------------------
	
	public String getNome() {
		return nome;
	}
	
	public String getCognome() {
		return cognome;
	}

	public char getSesso() {
		return sesso;
	}

	public String getNazione() {
		return nazione;
	}

	// --- metodi SETTER -------------------------------------------------------
	
	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public void setSesso(char sesso) {
		this.sesso = sesso;
	}

	public void setNazione(String nazione) {
		this.nazione = nazione;
	}

}
