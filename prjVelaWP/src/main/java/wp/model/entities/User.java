package wp.model.entities;

//import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
//import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
//import javax.persistence.OneToMany;
import javax.persistence.Table;
//import wp.model.entities.Role;
import javax.persistence.JoinColumn;

/**
 * [ USER ]
 * Classe che modella l'entit� persistente utente dell'applicazione
 * @author Esempio visto a lezione - UNIVPM
 * @version dicembre 2020
 */
@Entity
@Table(name="user")
public class User {

	// attributi mappati su Base Dati

	  @Id
	  @Column(name = "USERNAME")
	  private String username;

	  @Column(name = "PASSWORD", nullable = false)
	  private String password;

	  @Column(name = "ENABLED", nullable = false)
	  private boolean enabled;

	  @ManyToMany
	  @JoinTable( 
	      name = "user_role", 
	      joinColumns = @JoinColumn(
	        name = "username", referencedColumnName = "username"), 
	      inverseJoinColumns = @JoinColumn(
	        name = "role_id", referencedColumnName = "id")) 
	  private Set<Role> roles = new HashSet<Role>();
	  
	  public String getUsername() {
		  return this.username;
	  }
	  
	  public void setUsername(String username) {
		  this.username = username;
	  }
	  
	  public String getPassword() {
		  return this.password;
	  }
	  
	  public void setPassword(String password) {
		  this.password = password;
	  }
	  
	  public boolean isEnabled() {
		  return this.enabled;
	  }
	  
	  public void setEnabled(boolean enabled) {
		  this.enabled = enabled;
	  } 
	  
	  public Set<Role> roles () {
		  return this.roles;
	  }

	  public void addRole(Role role) {
		  if (this.roles == null) {
			  this.roles = new HashSet<Role>();
		  }
		  this.roles.add(role);
	  }

	  public Set<Role> getRoles() {
		  return this.roles;
	  }
	  
	  public void setRoles(Set<Role> roles) {
		  this.roles = roles;
	  }
	  
}
