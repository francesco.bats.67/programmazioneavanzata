package wp.model.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

/**
 * [ SQUADRA ]
 * Classe che modella l'entit� persistente societ� di pallanuoto femminile
 * @author Francesco Battistelli - UNIVPM
 * @version 30 luglio 2021
 */
@Entity
@Table(name="squadra")
public class Squadra implements Serializable {

	// --- attributi dell'entit�-----------------------------------------------
	
	private String idSquadra;
	private String nomeSquadra;
	private String denominazioneSquadra;
	private String luogoSquadra;
	private int versione;
	// insieme di atleti collegati alla squadra
	private Set<Atleta> atleti = new HashSet<Atleta>();
	// insieme di tecnici collegati alla squadra
	private Set<Tecnico> tecnici = new HashSet<Tecnico>();
	// insieme di partite collegate alla squadra
	private Set<Partita> partiteBianco = new HashSet<Partita>();
	private Set<Partita> partiteNero = new HashSet<Partita>();
	// insieme di azioni collegate alla squadra
	private Set<Azione> azioni = new HashSet<Azione>();
	// insieme di tabellini collegati alla squadra
	private Set<Tabellino> tabellini = new HashSet<Tabellino>();
	
	// --- metodi GETTER mappati su Base Dati ----------------------------------

	@Id
	@Column(name = "ID")
	public String getIdSquadra() {
		return this.idSquadra;
	}
	
	@Column(name = "NOME")
	public String getNomeSquadra() {
		return this.nomeSquadra;
	}
	
	@Column(name = "DENOMINAZIONE")
	public String getDenominazioneSquadra() {
		return this.denominazioneSquadra;
	}

	@Column(name = "LUOGO")
	public String getLuogoSquadra() {
		return this.luogoSquadra;
	}

	// --- metodi GETTER mappati su versioning ---------------------------------

	@Version
	public int getVersione() {
		return this.versione;
	}

	// --- metodi SETTER -------------------------------------------------------

	public void setIdSquadra(String idSquadra) {
		this.idSquadra = idSquadra;
	}

	public void setNomeSquadra(String nomeSquadra) {
		this.nomeSquadra = nomeSquadra;
	}

	public void setDenominazioneSquadra(String denominazioneSquadra) {
		this.denominazioneSquadra = denominazioneSquadra;
	}

	public void setLuogoSquadra(String luogoSquadra) {
		this.luogoSquadra = luogoSquadra;
	}

	public void setVersione(int versione) {
		this.versione = versione;
	}

	// --- metodi sovrascritti -------------------------------------------------

	@Override
	public String toString() {
		return "Squadra [" + idSquadra + "] " + denominazioneSquadra + ", " + luogoSquadra;
	}
	
	// --- propriet� non persistenti (operazioni su propriet� persistenti) -----

	@Transient
	public Set<Partita> getPartiteTotale() {
		Set<Partita> partite = this.getPartiteBianco();
		partite.addAll(this.getPartiteNero());
		return partite;
	}

	// --- relazioni UNO a MOLTI -----------------------------------------------

	// metodo getter sulla collezione PARTITE di squadre in calottina bianca
	// persistanza a cascata (eccetto delete)
	@OneToMany(mappedBy="squadraBianco", cascade= { CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.PERSIST })
	public Set<Partita> getPartiteBianco() {
		return this.partiteBianco;
	}
	// metodo setter sulla collezione PARTITE di squadre in calottina bianca
	public void setPartiteBianco(Set<Partita> partiteBianco) {
		this.partiteBianco = partiteBianco;
	}	
	// funzione che aggiunge elementi alla collezione PARTITE di squadre in calottina bianca
	public void addPartitaBianco(Partita partita) {
		partita.setSquadraBianco(this);
		this.partiteBianco.add(partita);
	}

	// metodo getter sulla collezione PARTITE di squadre in calottina nera
	// persistanza a cascata (eccetto delete)
	@OneToMany(mappedBy="squadraNero", cascade= { CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.PERSIST })
	public Set<Partita> getPartiteNero() {
		return this.partiteNero;
	}
	// metodo setter sulla collezione PARTITE di squadre in calottina nera
	public void setPartiteNero(Set<Partita> partiteNero) {
		this.partiteNero = partiteNero;
	}	
	// funzione che aggiunge elementi alla collezione PARTITE di squadre in calottina nera
	public void addPartitaNero(Partita partita) {
		partita.setSquadraNero(this);
		this.partiteNero.add(partita);
	}
	
	// metodo getter sulla collezione AZIONI di squadre
	// persistenza a cascata e rimozione degli orfani
	@OneToMany(mappedBy="squadra", cascade=CascadeType.ALL, orphanRemoval=true)
	public Set<Azione> getAzioni() {
		return this.azioni;
	}
	// metodo setter sulla collezione AZIONI di squadre
	public void setAzioni(Set<Azione> azioni) {
		this.azioni = azioni;
	}
	// funzione che aggiunge elementi alla collezione AZIONI di squadre
	public void addAzione(Azione azione) {
		azione.setSquadra(this);
		this.azioni.add(azione);
	}
	
	// metodo getter sulla collezione TABELLINI individuali di squadre
	// persistenza a cascata e rimozione degli orfani
	@OneToMany(mappedBy="squadra", cascade=CascadeType.ALL, orphanRemoval=true)
	public Set<Tabellino> getTabellini() {
		return this.tabellini;
	}
	// metodo setter sulla collezione TABELLINI individuali di squadre
	public void setTabellini(Set<Tabellino> tabellini) {
		this.tabellini = tabellini;
	}
	// funzione che aggiunge elementi alla collezione TABELLINI individuali di squadre
	public void addTabellino(Tabellino tabellino) {
		tabellino.setSquadra(this);
		this.tabellini.add(tabellino);
	}
	
	// --- relazioni MOLTI a MOLTI ---------------------------------------------

	// metodo getter sulla collezione ATLETI di squadre
	// caricamento diretto delle entit� squadra collegate, persistanza a cascata (eccetto delete)
	@ManyToMany(fetch = FetchType.EAGER,
				cascade= { CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.PERSIST } )
	// tabella di join (colonna di join ID_SQUADRA e colonna di join inversa ID_ATLETA)
	@JoinTable(name="squadra_atleta",
		joinColumns = @JoinColumn(name="ID_SQUADRA", nullable=false, updatable=false),
		inverseJoinColumns = @JoinColumn(name="ID_ATLETA", nullable=false, updatable=false))
	public Set<Atleta> getAtleti() {
		return this.atleti;
	}
	// metodo setter sulla collezione ATLETI di squadre
	public void setAtleti(Set<Atleta> atleti) {
		this.atleti = atleti;
	}
	// funzione che aggiunge elementi alla collezione ATLETI di squadre
	public void addAtleta(Atleta atleta) {
		this.atleti.add(atleta);
		atleta.getSquadre().add(this);
	}
	// funzione che rimuove elementi dalla collezione ATLETI di squadre
	public void removeAtleta(Atleta atleta) {
		this.atleti.remove(atleta);
		atleta.getSquadre().remove(this);
	}
	
	// metodo getter sulla collezione TECNICI di squadre
	// caricamento diretto delle entit� squadra collegate, persistanza a cascata (eccetto delete)
	@ManyToMany(fetch = FetchType.EAGER,
				cascade= { CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.PERSIST } )
	// tabella di join (colonna di join ID_SQUADRA e colonna di join inversa ID_TECNICO)
	@JoinTable(name="squadra_tecnico",
		joinColumns = @JoinColumn(name="ID_SQUADRA", nullable=false, updatable=false),
		inverseJoinColumns = @JoinColumn(name="ID_TECNICO", nullable=false, updatable=false))
	public Set<Tecnico> getTecnici() {
		return this.tecnici;
	}
	// metodo setter sulla collezione TECNICI di squadre
	public void setTecnici(Set<Tecnico> tecnici) {
		this.tecnici = tecnici;
	}
	// funzione che aggiunge elementi alla collezione TECNICI di squadre
	public void addTecnico(Tecnico tecnico) {
		this.tecnici.add(tecnico);
		tecnico.getSquadre().add(this);
	}
	// funzione che rimuove elementi dalla collezione TECNICI di squadre
	public void removeTecnico(Tecnico tecnico) {
		this.tecnici.remove(tecnico);
		tecnico.getSquadre().remove(this);
	}
	
}
