package wp.model.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;
import wp.model.Persona;

/**
 * [ TECNICO ]
 * Classe che modella l'entit� persistente allenatore di pallanuoto
 * @author Francesco Battistelli - UNIVPM
 * @version 30 luglio 2021
 */
@Entity
@Table(name="tecnico")
@AttributeOverrides({
	@AttributeOverride(name="nome",column=@Column(name="NOME",nullable=false)),
	@AttributeOverride(name="cognome",column=@Column(name="COGNOME",nullable=false)),
	@AttributeOverride(name="sesso",column=@Column(name="SESSO")),
	@AttributeOverride(name="nazione",column=@Column(name="NAZIONE"))
})
@NamedQueries({
	@NamedQuery(name="Partita.findAllSquadre",
			query="SELECT DISTINCT t FROM Tecnico t " +
					"LEFT JOIN FETCH t.squadre s")
})
public class Tecnico extends Persona implements Serializable {

	// --- attributi dell'entit�-----------------------------------------------

	private String idTecnico;
	private int versione;
	// insieme di squadre collegate al tecnico
	private Set<Squadra> squadre = new HashSet<Squadra>();

	// --- metodi GETTER mappati su Base Dati ----------------------------------

	@Id
	@Column(name = "ID")
	public String getIdTecnico() {
		return idTecnico;
	}

	// --- metodi GETTER mappati su versioning ---------------------------------

	@Version
	public int getVersione() {
		return this.versione;
	}

	// --- metodi SETTER -------------------------------------------------------

	public void setIdTecnico(String idTecnico) {
		this.idTecnico = idTecnico;
	}

	public void setVersione(int versione) {
		this.versione = versione;
	}

	// --- metodi sovrascritti -------------------------------------------------

	@Override
	public String toString() {
		return "Tecnico [" + idTecnico + "] " + this.getNome() + " " + this.getCognome();
	}
	
	// --- propriet� non persistenti (operazioni su propriet� persistenti) -----

	@Transient
	public String getNomeCompleto() {
		return(this.getNome() + " " + this.getCognome()).trim();
	}
	
	// --- relazione MOLTI a MOLTI ---------------------------------------------
	
	// metodo getter sulla collezione SQUADRE di tecnici
	// EAGER = caricamento diretto delle entit� squadra collegate, persistanza a cascata (eccetto delete)
	@ManyToMany(fetch = FetchType.EAGER,
			cascade= { CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.PERSIST },
			mappedBy = "tecnici")	
	public Set<Squadra> getSquadre() {
		return this.squadre;
	}
	// metodo setter sulla collezione SQUADRE di tecnici
	public void setSquadre(Set<Squadra> squadre) {
		this.squadre = squadre;
	}
	// funzione che aggiunge elementi alla collezione SQUADRE di tecnici
	public void addSquadra(Squadra squadra) {
		// NOTA: non usiamo il metodo squadra.addTecnico(this) per evitare cicli infiniti
		this.squadre.add(squadra);
		squadra.getTecnici().add(this);
	}
	// funzione che rimuove elementi dalla collezione SQUADRE di tecnici
	public void removeSquadra(Squadra squadra) {
		this.squadre.remove(squadra);
		squadra.getTecnici().remove(this);
	}
}
