package wp.model.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * [ AZIONE ]
 * Classe che modella l'entit� persistente azione di gioco di un partita di pallanuoto femminile
 * @author Francesco Battistelli - UNIVPM
 * @version 30 luglio 2021
 */
@Entity
@Table(name="azione")
public class Azione implements Serializable {

	// --- attributi dell'entit�-----------------------------------------------

	private Long idAzione;
	private boolean supNum;
	private String tipoAzione;
	private String esitoAzione;
	private String risultato;
	private int numeroCalottina;
	private int[] tempoGioco;
	private int versione;
	// attributo Partita
	private Partita partita;
	// attributo Squadra
	private Squadra squadra;
	// attributo Atleta
	private Atleta atleta;

	// --- metodi GETTER mappati su Base Dati ----------------------------------
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	public Long getIdAzione() {
		return idAzione;
	}

	@Column(name = "SUP_NUM")
	public boolean isSupNum() {
		return supNum;
	}

	@Column(name = "TIPO")
	public String getTipoAzione() {
		return tipoAzione;
	}

	@Column(name = "ESITO")
	public String getEsitoAzione() {
		return esitoAzione;
	}

	@Column(name = "RISULTATO")
	public String getRisultato() {
		return risultato;
	}
	
	@Column(name = "TEMPO_GIOCO")
	public int[] getTempoGioco() {
		return tempoGioco;
	}
	
	@Column(name = "CALOTTINA")
	public int getNumeroCalottina() {
		return numeroCalottina;
	}

	// --- metodi GETTER mappati su versioning ---------------------------------
	
	@Version
	public int getVersione() {
		return this.versione;
	}
	
	// --- metodi SETTER -------------------------------------------------------

	public void setIdAzione(Long idAzione) {
		this.idAzione = idAzione;
	}
	
	public void setSupNum(boolean supNum) {
		this.supNum = supNum;
	}	
	
	public void setTipoAzione(String tipoAzione) {
		this.tipoAzione = tipoAzione;
	}
	
	public void setEsitoAzione(String esitoAzione) {
		this.esitoAzione = esitoAzione;
	}
	
	public void setRisultato(String risultato) {
		this.risultato = risultato;
	}
	
	public void setNumeroCalottina(int numeroCalottina) {
		this.numeroCalottina = numeroCalottina;
	}
	
	public void setTempoGioco(int[] tempoGioco) {
		this.tempoGioco = tempoGioco;
	}
	
	public void setVersione(int versione) {
		this.versione = versione;
	}
	
	// --- metodi sovrascritti -------------------------------------------------

	@Override
	public String toString() {
		return "Azione - Id=" + idAzione + ", tipo=" + tipoAzione + ", esito=" + esitoAzione + ", risultato=" + risultato;
	}

	// --- relazioni MOLTI a UNO -----------------------------------------------

	// metodo getter sulla relazione PARTITA di azione
	@ManyToOne
	@JoinColumn(name="PARTITA_ID", nullable=false)
	public Partita getPartita() {
		return this.partita;
	}

	// metodo getter sulla relazione SQUADRA di azione
	@ManyToOne
	@JoinColumn(name="SQUADRA_ID", nullable=false)
	public Squadra getSquadra() {
		return this.squadra;
	}
	
	// metodo getter sulla relazione ATLETA di azione
	@ManyToOne
	@JoinColumn(name="ATLETA_ID")
	public Atleta getAtleta() {
		return this.atleta;
	}

	// metodi setter sulle relazioni molti a uno
	public void setPartita(Partita partita) {
		this.partita = partita;
	}
	public void setSquadra(Squadra squadra) {
		this.squadra = squadra;
	}
	public void setAtleta(Atleta atleta) {
		this.atleta = atleta;
	}
	
}
