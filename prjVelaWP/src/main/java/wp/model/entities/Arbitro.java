package wp.model.entities;

import java.io.Serializable;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;
import wp.model.Persona;

/**
 * [ ARBITRO ]
 * Classe che modella l'entit� persistente arbitro di pallanuoto
 * @author Francesco Battistelli - UNIVPM
 * @version 30 luglio 2021
 */
@Entity
@Table(name="arbitro")
@AttributeOverrides({
	@AttributeOverride(name="nome",column=@Column(name="NOME",nullable=false)),
	@AttributeOverride(name="cognome",column=@Column(name="COGNOME",nullable=false)),
	@AttributeOverride(name="sesso",column=@Column(name="SESSO"))
})
public class Arbitro extends Persona implements Serializable {

	// --- attributi dell'entit�-----------------------------------------------

	private String idArbitro;
	private String provenienza;
	private int versione;
	
	// --- metodi GETTER mappati su Base Dati ----------------------------------

	@Id
	@Column(name = "ID")	
	public String getIdArbitro() {
		return idArbitro;
	}
	
	@Column(name = "PROVENIENZA")
	public String getProvenienza() {
		return provenienza;
	}

	// --- metodi GETTER mappati su versioning ---------------------------------

	@Version
	public int getVersione() {
		return this.versione;
	}

	// --- metodi SETTER -------------------------------------------------------

	public void setIdArbitro(String idArbitro) {
		this.idArbitro = idArbitro;
	}

	public void setProvenienza(String provenienza) {
		this.provenienza = provenienza;
	}

	public void setVersione(int versione) {
		this.versione = versione;
	}

	// --- metodi sovrascritti -------------------------------------------------

	@Override
	public String toString() {
		return "Arbitro [" + idArbitro + "] " + this.getNome() + " " + this.getCognome() + ", " + this.provenienza;
	}
	
	// --- propriet� non persistenti (operazioni su propriet� persistenti) -----

	@Transient
	public String getNomeCompleto() {
		return(this.getNome() + " " + this.getCognome()).trim();
	}

}
