package wp.model.entities;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

/**
 * [ PARTITA ]
 * Classe che modella l'entit� persistente partita di pallanuoto femminile
 * @author Francesco Battistelli - UNIVPM
 * @version 30 luglio 2021
 */
@Entity
@Table(name="partita")
public class Partita implements Serializable {

	// --- attributi dell'entit�-----------------------------------------------
	
	private Long idPartita;
	private Date data;
	private String competizione;
	private String esitoPartita;
	private int periodoGioco;
	private String attacco;
	private int retiBianco;
	private int retiNero;
	private int[] parzialiBianco;
	private int[] parzialiNero;
	private int[] supNumBianco;
	private int[] supNumNero;
	private int versione;
	// attributo Piscina
	private Piscina piscina;
	// attributi Squadra in calottina bianca
	private Squadra squadraBianco;
	private Tecnico tecnicoBianco;
//	private ArrayList<Atleta> formazioneBianco;
	private Tabellino portiereBianco;	
	// attributi Squadra in calottina nera
	private Squadra squadraNero;
	private Tecnico tecnicoNero;
//	private ArrayList<Atleta> formazioneNero;
	private Tabellino portiereNero;
	// attributi Arbitro
	private Arbitro arbitro1;
	private Arbitro arbitro2;
	// insieme di azioni collegate alla partita
	private Set<Azione> azioni = new HashSet<Azione>();
	// insieme di tabellini individuali collegati alla partita
	private Set<Tabellino> tabellini = new HashSet<Tabellino>();

	// --- metodi GETTER mappati su Base Dati ----------------------------------

	// chiave primaria autoincrementante
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")	
	public Long getIdPartita() {
		return this.idPartita;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "DATA")
	public Date getData() {
		return this.data;
	}
	
	@Column(name = "COMPETIZIONE")
	public String getCompetizione() {
		return this.competizione;
	}
	
	@Column(name = "ESITO")
	public String getEsitoPartita() {
		return this.esitoPartita;
	}
	
	@Column(name = "PERIODO_GIOCO")
	public int getPeriodoGioco() {
		return this.periodoGioco;
	}
	
	@Column(name = "ATTACCO")
	public String getAttacco() {
		return this.attacco;
	}
	
	@Column(name = "RETI_BIANCO")
	public int getRetiBianco() {
		return this.retiBianco;
	}
	
	@Column(name = "RETI_NERO")
		public int getRetiNero() {
		return this.retiNero;
	}

	@Column(name = "PARZIALI_BIANCO")
	public int[] getParzialiBianco() {
		return parzialiBianco;
	}

	@Column(name = "PARZIALI_NERO")
	public int[] getParzialiNero() {
		return parzialiNero;
	}

	@Column(name = "SUP_NUM_BIANCO")
	public int[] getSupNumBianco() {
		return supNumBianco;
	}

	@Column(name = "SUP_NUM_NERO")
	public int[] getSupNumNero() {
		return supNumNero;
	}

	// --- metodi GETTER mappati su versioning ---------------------------------

	@Version
	public int getVersione() {
		return this.versione;
	}

	// --- metodi SETTER -------------------------------------------------------

	public void setIdPartita(Long idPartita) {
		this.idPartita = idPartita;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public void setCompetizione(String competizione) {
		this.competizione = competizione;
	}

	public void setEsitoPartita(String esitoPartita) {
		this.esitoPartita = esitoPartita;
	}

	public void setPeriodoGioco(int periodoGioco) {
		this.periodoGioco = periodoGioco;
	}

	public void setAttacco(String attacco) {
		this.attacco = attacco;
	}

	public void setRetiBianco(int retiBianco) {
		this.retiBianco = retiBianco;
	}

	public void setRetiNero(int retiNero) {
		this.retiNero = retiNero;
	}

	public void setParzialiBianco(int[] parzialiBianco) {
		this.parzialiBianco = parzialiBianco;
	}

	public void setParzialiNero(int[] parzialiNero) {
		this.parzialiNero = parzialiNero;
	}

	public void setSupNumBianco(int[] supNumBianco) {
		this.supNumBianco = supNumBianco;
	}

	public void setSupNumNero(int[] supNumNero) {
		this.supNumNero = supNumNero;
	}

	public void setVersione(int versione) {
		this.versione = versione;
	}
	
	// --- metodi sovrascritti -------------------------------------------------

	@Override
	public String toString() {
		return "Partita - Id=" + idPartita + ", data=" + data + ", competizione=" + competizione + ", esito=" + esitoPartita;
	}

	// --- propriet� non persistenti (operazioni su propriet� persistenti) -----

	@Transient
	public String getRisultato() {
		return this.getRetiBianco() + "-" + this.getRetiNero();
	}

	@Transient
	public String getDataEstesa() {
		DateFormat formattaData = DateFormat.getDateInstance(DateFormat.LONG);
		String dataFormattata = formattaData.format(this.data);
		return dataFormattata;
	}
	
	// --- relazioni MOLTI a UNO -----------------------------------------------
	
	// metodo getter sulla relazione PISCINA di partita
	@ManyToOne
	@JoinColumn(name="PISCINA_ID")
	public Piscina getPiscina() {
		return this.piscina;
	}
	
	// metodo getter sulla relazione SQUADRA in calottina bianca di partita
	@ManyToOne
	@JoinColumn(name="SQUADRA_BIANCO_ID", nullable=false)
	public Squadra getSquadraBianco() {
		return this.squadraBianco;
	}

	// metodo getter sulla relazione SQUADRA in calottina nera di partita
	@ManyToOne
	@JoinColumn(name="SQUADRA_NERO_ID", nullable=false)
	public Squadra getSquadraNero() {
		return this.squadraNero;
	}
	
	// metodo getter sulla relazione TECNICO della squadra in calottina bianca di partita
	@ManyToOne
	@JoinColumn(name="TECNICO_BIANCO_ID")
	public Tecnico getTecnicoBianco() {
		return this.tecnicoBianco;
	}

	// metodo getter sulla relazione TECNICO della squadra in calottina nera di partita
	@ManyToOne
	@JoinColumn(name="TECNICO_NERO_ID")
	public Tecnico getTecnicoNero() {
		return this.tecnicoNero;
	}

	// metodi getter sulla relazione 1� ARBITRO di partita
	@ManyToOne
	@JoinColumn(name="ARBITRO1_ID")
	public Arbitro getArbitro1() {
		return this.arbitro1;
	}
	
	// metodi getter sulla relazione 2� ARBITRO di partita
	@ManyToOne
	@JoinColumn(name="ARBITRO2_ID")
	public Arbitro getArbitro2() {
		return this.arbitro2;
	}

	// metodo getter sulla relazione PORTIERE della squadra in calottina bianca di partita
	@ManyToOne
	@JoinColumn(name="PORTIERE_BIANCO_ID")
	public Tabellino getPortiereBianco() {
		return this.portiereBianco;
	}

	// metodo getter sulla relazione PORTIERE della squadra in calottina nera di partita
	@ManyToOne
	@JoinColumn(name="PORTIERE_NERO_ID")
	public Tabellino getPortiereNero() {
		return this.portiereNero;
	}

	// metodi setter sulle relazioni molti a uno
	public void setPiscina(Piscina piscina) {
		this.piscina = piscina;
	}
	public void setSquadraBianco(Squadra squadraBianco) {
		this.squadraBianco = squadraBianco;
	}
	public void setSquadraNero(Squadra squadraNero) {
		this.squadraNero = squadraNero;
	}
	public void setArbitro1(Arbitro arbitro1) {
		this.arbitro1 = arbitro1;
	}
	public void setArbitro2(Arbitro arbitro2) {
		this.arbitro2 = arbitro2;
	}
	public void setTecnicoBianco(Tecnico tecnicoBianco) {
		this.tecnicoBianco = tecnicoBianco;
	}
	public void setTecnicoNero(Tecnico tecnicoNero) {
		this.tecnicoNero = tecnicoNero;
	}
	public void setPortiereBianco(Tabellino portiereBianco) {
		this.portiereBianco = portiereBianco;
	}
	public void setPortiereNero(Tabellino portiereNero) {
		this.portiereNero = portiereNero;
	}

	// --- relazioni UNO a MOLTI -----------------------------------------------

	// metodo getter sulla collezione AZIONI di partite
	// persistanza a cascata (eccetto delete)
	@OneToMany(mappedBy="partita", cascade=CascadeType.ALL, orphanRemoval=true)
	public Set<Azione> getAzioni() {
		return this.azioni;
	}

	// metodo setter sulla collezione AZIONI di partite
	public void setAzioni(Set<Azione> azioni) {
		this.azioni = azioni;
	}
	
	// funzione che aggiunge elementi alla collezione AZIONI di partite
	public void addAzione(Azione azione) {
		azione.setPartita(this);
		this.azioni.add(azione);
	}
	
	// metodo getter sulla collezione TABELLINI individuali di partite
	// persistenza a cascata e rimozione degli orfani
	@OneToMany(mappedBy="partita", cascade=CascadeType.ALL, orphanRemoval=true)
	public Set<Tabellino> getTabellini() {
		return this.tabellini;
	}

	// metodo setter sulla collezione TABELLINI individuali di partite
	public void setTabellini(Set<Tabellino> tabellini) {
		this.tabellini = tabellini;
	}
	
	// funzione che aggiunge elementi alla collezione TABELLINI individuali di partite
	public void addTabellino(Tabellino tabellino) {
		tabellino.setPartita(this);
		this.tabellini.add(tabellino);
	}
	
	// --- metodi che manipolano singoli elementi degli array ------------------
	
	public void addReteBianco() {
		this.setRetiBianco( getRetiBianco() + 1 );
		this.parzialiBianco[this.periodoGioco - 1] = parzialiBianco[this.periodoGioco - 1] + 1;
	}
	public void addReteNero() {
		this.setRetiNero( getRetiNero() + 1 );
		this.parzialiNero[this.periodoGioco - 1] = parzialiBianco[this.periodoGioco - 1] + 1;
	}
	
	public void addSupNumReteBianco() {
		this.supNumBianco[0] = supNumBianco[0] + 1;
		this.supNumBianco[1] = supNumBianco[1] + 1;
	}
	public void addSupNumReteNero() {
		this.supNumNero[0] = supNumNero[0] + 1;
		this.supNumNero[1] = supNumNero[1] + 1;
	}
	public void addSupNumNoReteBianco() {
		this.supNumBianco[1] = supNumBianco[1] + 1;
	}
	public void addSupNumNoReteNero() {
		this.supNumNero[1] = supNumNero[1] + 1;
	}

}