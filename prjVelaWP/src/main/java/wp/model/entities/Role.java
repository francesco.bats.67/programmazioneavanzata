package wp.model.entities;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import wp.model.entities.User;

/**
 * [ ROLE ]
 * Classe che modella l'entit� persistente ruolo utente dell'applicazione
 * @author Esempio visto a lezione - UNIVPM
 * @version dicembre 2020
 */
@Entity
@Table(name="role")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
 
    @Column
    private String name;
    
    @ManyToMany(mappedBy = "roles")
    private Set<User> users = new HashSet<User>();
 
    public String getName() {
    	return this.name;
    }
    
    public void setName(String name) {
    	this.name = name;
    }
}