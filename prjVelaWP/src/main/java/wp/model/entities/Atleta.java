package wp.model.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;
import wp.model.Persona;

/**
 * [ ATLETA ]
 * Classe che modella l'entit� persistente giocatrice di pallanuoto
 * @author Francesco Battistelli - UNIVPM
 * @version 30 luglio 2021
 */
@Entity
@Table(name="atleta")
@AttributeOverrides({
	@AttributeOverride(name="nome",column=@Column(name="NOME",nullable=false)),
	@AttributeOverride(name="cognome",column=@Column(name="COGNOME",nullable=false)),
	@AttributeOverride(name="sesso",column=@Column(name="SESSO")),
	@AttributeOverride(name="nazione",column=@Column(name="NAZIONE"))
})
@NamedQueries({
	@NamedQuery(name="Atleta.findAllSquadre",
			query="SELECT DISTINCT a FROM Atleta a " +
					"LEFT JOIN FETCH a.squadre s")
})
public class Atleta extends Persona implements Serializable {

	// --- attributi dell'entit�-----------------------------------------------

	private String idAtleta;
	private int annoNascita;
	private int versione;
	// insieme di squadre collegate alla giocatrice
	private Set<Squadra> squadre = new HashSet<Squadra>();

	// --- metodi GETTER mappati su Base Dati ----------------------------------

	@Id
	@Column(name = "ID")
	public String getIdAtleta() {
		return idAtleta;
	}

	@Column(name = "ANNO_NASCITA")
	public int getAnnoNascita() {
		return annoNascita;
	}

	// --- metodi GETTER mappati su versioning ---------------------------------

	@Version
	public int getVersione() {
		return this.versione;
	}

	// --- metodi SETTER -------------------------------------------------------

	public void setIdAtleta(String idAtleta) {
		this.idAtleta = idAtleta;
	}

	public void setAnnoNascita(int annoNascita) {
		this.annoNascita = annoNascita;
	}

	public void setVersione(int versione) {
		this.versione = versione;
	}

	// --- metodi sovrascritti -------------------------------------------------

	@Override
	public String toString() {
		return "Atleta [" + idAtleta + "] " + this.getNome() + " " + this.getCognome();
	}
	
	// --- propriet� non persistenti (operazioni su propriet� persistenti) -----

	@Transient
	public String getNomeCompleto() {
		return(this.getNome() + " " + this.getCognome()).trim();
	}

	// --- relazione MOLTI a MOLTI ---------------------------------------------

	// metodo getter sulla collezione SQUADRE di giocatrici
	// caricamento diretto delle entit� squadra collegate, persistanza a cascata (eccetto delete)
	@ManyToMany(fetch = FetchType.EAGER,
			cascade= { CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.PERSIST },
			mappedBy = "atleti")	
	public Set<Squadra> getSquadre() {
		return this.squadre;
	}
	// metodo setter sulla collezione SQUADRE di giocatrici
	public void setSquadre(Set<Squadra> squadre) {
		this.squadre = squadre;
	}
	// funzione che aggiunge elementi alla collezione SQUADRE di giocatrici
	public void addSquadra(Squadra squadra) {
		this.squadre.add(squadra);
		squadra.getAtleti().add(this);
	}
	// funzione che rimuove elementi dalla collezione SQUADRE  di giocatrici
	public void removeSquadra(Squadra squadra) {
		this.squadre.remove(squadra);
		squadra.getAtleti().remove(this);
	}
	
}

