package wp.model.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * [ TABELLINO ]
 * Classe che modella l'entit� persistente tabellino individuale di una giocatrice di pallanuoto in una partita
 * @author Francesco Battistelli - UNIVPM
 * @version 8 agosto 2021
 */
@Entity
@Table(name="tabellino")
public class Tabellino implements Serializable {

	// --- attributi dell'entit�-----------------------------------------------
	
	private Long idTabellino;
	private int numeroCalottina;
	private boolean iniziale;
	private float minuti;
	private int reti;
	private int tiri;
	private int falli;
	private boolean portiere;
	private int[] statReti;
	private int[] statTiri;
	private int[] statFalli;
	private int[] statPorta;
	private int versione;

	// attributo Partita
	private Partita partita;
	// attributo Squadra
	private Squadra squadra;
	// attributo Atleta
	private Atleta atleta;
	
	// metodi GETTER mappati su Base Dati ---------------------

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	public Long getIdTabellino() {
		return idTabellino;
	}

	@Column(name = "CALOTTINA")
	public int getNumeroCalottina() {
		return numeroCalottina;
	}
	
	@Column(name = "INIZIALE")	
	public boolean isIniziale() {
		return iniziale;
	}

	@Column(name = "MINUTI")
	public float getMinuti() {
		return minuti;
	}

	@Column(name = "RETI")
	public int getReti() {
		return reti;
	}

	@Column(name = "TIRI")
	public int getTiri() {
		return tiri;
	}

	@Column(name = "FALLI")
	public int getFalli() {
		return falli;
	}

	@Column(name = "PORTIERE")
	public boolean isPortiere() {
		return portiere;
	}

	@Column(name = "STAT_RETI")
	public int[] getStatReti() {
		return statReti;
	}

	@Column(name = "STAT_TIRI")
	public int[] getStatTiri() {
		return statTiri;
	}
		
	@Column(name = "STAT_FALLI")
	public int[] getStatFalli() {
		return statFalli;
	}
	
	@Column(name = "STAT_PORTA")
	public int[] getStatPorta() {
		return statPorta;
	}

	// --- metodi GETTER mappati su versioning ---------------------------------

	@Version
	public int getVersione() {
		return this.versione;
	}
	
	// --- metodi SETTER -------------------------------------------------------

	public void setIdTabellino(Long idTabellino) {
		this.idTabellino = idTabellino;
	}	

	public void setNumeroCalottina(int numeroCalottina) {
		this.numeroCalottina = numeroCalottina;
	}

	public void setIniziale(boolean iniziale) {
		this.iniziale = iniziale;
	}

	public void setMinuti(float minuti) {
		this.minuti = minuti;
	}

	public void setReti(int reti) {
		this.reti = reti;
	}

	public void setTiri(int tiri) {
		this.tiri = tiri;
	}

	public void setFalli(int falli) {
		this.falli = falli;
	}

	public void setPortiere(boolean portiere) {
		this.portiere = portiere;
	}

	public void setStatReti(int[] statReti) {
		this.statReti = statReti;
	}

	public void setStatTiri(int[] statTiri) {
		this.statTiri = statTiri;
	}

	public void setStatFalli(int[] statFalli) {
		this.statFalli = statFalli;
	}

	public void setStatPorta(int[] statPorta) {
		this.statPorta = statPorta;
	}

	public void setVersione(int versione) {
		this.versione = versione;
	}
	
	// --- metodi sovrascritti -------------------------------------------------

	@Override
	public String toString() {
		return "Score - Id=" + idTabellino + ", calottina=" + numeroCalottina;
	}
	
	// --- relazioni MOLTI a UNO -----------------------------------------------

	// metodo getter sulla relazione PARTITA di tabellino
	@ManyToOne
	@JoinColumn(name="PARTITA_ID", nullable=false)
	public Partita getPartita() {
		return this.partita;
	}
	
	// metodo getter sulla relazione SQUADRA di tabellino
	@ManyToOne
	@JoinColumn(name="SQUADRA_ID", nullable=false)
//	@JoinColumn(name="SQUADRA_ID")
	public Squadra getSquadra() {
		return this.squadra;
	}
	
	// metodo getter sulla relazione ATLETA di tabellino
	@ManyToOne
	@JoinColumn(name="ATLETA_ID")
	public Atleta getAtleta() {
		return this.atleta;
	}
	
	// metodi setter sulle relazioni molti a uno
	public void setPartita(Partita partita) {
		this.partita = partita;
	}
	public void setSquadra(Squadra squadra) {
		this.squadra = squadra;
	}
	public void setAtleta(Atleta atleta) {
		this.atleta = atleta;
	}
	
	// --- metodi che manipolano singoli elementi degli array ------------------

	public void addReteSegnata(int indiceTiro) {
		this.reti = reti + 1;
		this.tiri = tiri + 1;
		this.statReti[indiceTiro] = statReti[indiceTiro] + 1;
		this.statTiri[indiceTiro] = statTiri[indiceTiro] + 1;
	}
	
	public void addTiroSbagliato(int indiceTiro) {
		this.tiri = tiri + 1;
		this.statTiri[indiceTiro] = statTiri[indiceTiro] + 1;
	}
	
	public void addFallo(int indiceTiro) {
		this.falli = falli + 1;
		this.statFalli[indiceTiro] = statFalli[indiceTiro] + 1;
	}

	public void addTiroParato(int indiceTiro) {
			this.statPorta[0] = statPorta[0] + 1;
			this.statPorta[3] = statPorta[3] + 1;
		if ( indiceTiro < 2 ) {
			this.statPorta[indiceTiro + 1] = statPorta[indiceTiro + 1] + 1;
			this.statPorta[indiceTiro + 4] = statPorta[indiceTiro + 4] + 1;
		}
	}

	public void addReteSubita(int indiceTiro) {
			this.statPorta[3] = statPorta[3] + 1;
		if ( indiceTiro < 2 ) {
			this.statPorta[indiceTiro + 4] = statPorta[indiceTiro + 4] + 1;
		}	
	}
	

}
