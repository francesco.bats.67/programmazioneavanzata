package wp.model.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * [ PISCINA ]
 * Classe che modella l'entit� persistente piscina dove si disputa una partita di pallanuoto
 * @author Francesco Battistelli - UNIVPM
 * @version 30 luglio 2021
 */
@Entity
@Table(name="piscina")
public class Piscina implements Serializable {
	
	// --- attributi dell'entit�-----------------------------------------------
	
	private String idPiscina;
	private String nomePiscina;
	private String luogoPiscina;
	private int versione;
	// insieme di partite collegate alla piscina
	private Set<Partita> partite = new HashSet<Partita>();
	
	// --- metodi GETTER mappati su Base Dati ----------------------------------

	@Id
	@Column(name = "ID")
	public String getIdPiscina() {
		return this.idPiscina;
	}

	@Column(name = "NOME")
	public String getNomePiscina() {
		return this.nomePiscina;
	}
	
	@Column(name = "LUOGO")
	public String getLuogoPiscina() {
		return this.luogoPiscina;
	}

	// --- metodi GETTER mappati su versioning ---------------------------------

	@Version
	public int getVersione() {
		return this.versione;
	}

	// --- metodi SETTER -------------------------------------------------------

	public void setIdPiscina(String idPiscina) {
		this.idPiscina = idPiscina;
	}

	public void setNomePiscina(String nomePiscina) {
		this.nomePiscina = nomePiscina;
	}

	public void setLuogoPiscina(String luogoPiscina) {
		this.luogoPiscina = luogoPiscina;
	}

	public void setVersione(int versione) {
		this.versione = versione;
	}

	// --- metodi sovrascritti -------------------------------------------------

	@Override
	public String toString() {
		return "Piscina [" + idPiscina + "] " + nomePiscina + ", " + luogoPiscina;
	}

	// --- relazione UNO a MOLTI -----------------------------------------------

	// metodo getter sulla collezione PARTITE di piscine
	// persistanza a cascata con rimozione orfani
	@OneToMany(mappedBy="piscina", cascade=CascadeType.ALL, orphanRemoval=true)
	public Set<Partita> getPartite() {
		return this.partite;
	}
	// metodo setter sulla collezione PARTITE di piscine
	public void setPartite(Set<Partita> partite) {
		this.partite = partite;
	}
	// funzione che aggiunge elementi alla collezione PARTITE di piscine
	public void addPartita(Partita partita) {
		partita.setPiscina(this);
		this.partite.add(partita);
	}

}
