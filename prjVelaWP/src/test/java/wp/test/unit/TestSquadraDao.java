package wp.test.unit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import wp.model.dao.SquadraDao;
import wp.model.entities.Squadra;
import wp.test.TestDataServiceConfig;

/**
 * TEST SQUADRA DAO
 * Classe per testare il Data Object Access squadra di pallanuoto
 * @author Francesco Battistelli - UNIVPM
 * @version 30 agosto 2021
 */
public class TestSquadraDao {

	@Test
	void testBeginCommitTransaction() {
		try (AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(TestDataServiceConfig.class)) {

			SessionFactory sf = ctx.getBean("sessionFactory", SessionFactory.class);
			
			SquadraDao squadraDao = ctx.getBean("DaoSquadra", SquadraDao.class);
			
			Session s  = sf.openSession();
			
			assertTrue(s.isOpen());
			
			s.beginTransaction();
			
			squadraDao.setSession(s);
			
			assertEquals(s, squadraDao.getSession());
			assertSame(s, squadraDao.getSession());
			assertTrue(s.getTransaction().isActive());
			
			s.getTransaction().commit();
			
			assertFalse(s.getTransaction().isActive());
		}
		
	}
	
	@Test
	void testCreaSquadraNomiDuplicati() {
		try (AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(TestDataServiceConfig.class)) {

			SessionFactory sf = ctx.getBean("sessionFactory", SessionFactory.class);
			
			SquadraDao squadraDao = ctx.getBean("DaoSquadra", SquadraDao.class);
			
			Session s  = sf.openSession();
			
			squadraDao.setSession(s);
			
			Squadra nuovaSquadra = squadraDao.create("TLP", "Tolentino", "Tolentino P.N.", null);
			
//			// opzione 1 (ID DUPLICATO) --> VA IN ERRORE !!!
//			try {
//				Squadra nuovaSquadra2 = squadraDao.create(nuovaSquadra.getIdSquadra(), nuovaSquadra.getNomeSquadra(), nuovaSquadra.getDenominazioneSquadra(), null);
//				assertTrue(true);
//			} catch (Exception e) {
//				// pass
//				assertTrue(false);
//			}
			
			// opzione 2 (NOME DUPLICATO)
			try {
				Squadra nuovaSquadra3 = squadraDao.create("XXX", nuovaSquadra.getNomeSquadra(), nuovaSquadra.getDenominazioneSquadra(), null);
				assertTrue(true);
				
			} catch (Exception e) {
				// pass
				assertTrue(false);
			}
		}
		
		
	}


	@Test
	void testSenzaSquadre() {
		try (AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(TestDataServiceConfig.class)) {

			SessionFactory sf = ctx.getBean("sessionFactory", SessionFactory.class);
			
			SquadraDao squadraDao = ctx.getBean("DaoSquadra", SquadraDao.class);
			
			Session s  = sf.openSession();
			
			squadraDao.setSession(s);
			
			List<Squadra> squadre = squadraDao.findAll();
			
			assertEquals(squadre.size(), 0);
		}
	}
	
}

