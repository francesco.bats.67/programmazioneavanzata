package wp.test.unit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import wp.model.dao.AtletaDao;
import wp.model.entities.Atleta;
import wp.test.TestDataServiceConfig;

/**
 * TEST ATLETA DAO
 * Classe per testare il Data Object Access atleta di pallanuoto
 * @author Francesco Battistelli - UNIVPM
 * @version 30 agosto 2021
 */
public class TestAtletaDao {

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testSenzaAtleti() {
		try (AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(TestDataServiceConfig.class)) {

			SessionFactory sf = ctx.getBean("sessionFactory", SessionFactory.class);
			
			AtletaDao atletaDao = ctx.getBean("DaoAtleta", AtletaDao.class);
			
			Session s  = sf.openSession();
			
			atletaDao.setSession(s);
			
			assertEquals(atletaDao.findAll().size(), 0);
		}
	}
	
	@Test
	void testCreaTrova() {
		try (AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(TestDataServiceConfig.class)) {

			SessionFactory sf = ctx.getBean("sessionFactory", SessionFactory.class);
			
			AtletaDao atletaDao = ctx.getBean("DaoAtleta", AtletaDao.class);
			
			Session s  = sf.openSession();
			
			s.beginTransaction();
			
			atletaDao.setSession(s);
			
			Atleta nuovoAtleta = atletaDao.create("COLLETTA_F", "Francesca", "Colletta", 'F', "ITA", null, 2000);
			try {
				atletaDao.findById(nuovoAtleta.getIdAtleta());
			} catch (Exception e) {
				fail("Eccezione: " + e.getMessage());
			}
			
			try {
				Atleta notFound = atletaDao.findById("PETRACCI_A");
				assertEquals(notFound, null);
			} catch (Exception e) {
				assertTrue(true);
			}
			
			s.getTransaction().commit();
			
			List<Atleta> atleti = atletaDao.findAll();
			assertEquals(atleti.size(), 1);
		}
	}
	

	@Test
	void testCreaModifica() {
		try (AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(TestDataServiceConfig.class)) {

			SessionFactory sf = ctx.getBean("sessionFactory", SessionFactory.class);
			
			AtletaDao atletaDao = ctx.getBean("DaoAtleta", AtletaDao.class);
			
			Session s  = sf.openSession();
			
			atletaDao.setSession(s);
			
			s.beginTransaction();
			
			Atleta nuovoAtleta1 = atletaDao.create("COLLETTA_F", "Francesca", "Colletta", 'F', "ITA", null, 2000);
			
			s.getTransaction().commit();
			
			s.beginTransaction();
			assertEquals(atletaDao.findAll().size(), 1);

			Atleta nuovoAtleta2 = new Atleta();
			nuovoAtleta2.setIdAtleta(nuovoAtleta1.getIdAtleta());
			nuovoAtleta2.setNome("Francy");
			nuovoAtleta2.setCognome(nuovoAtleta1.getCognome());
			
			assertEquals(nuovoAtleta1.getNome(), "Francesca");
			assertNotEquals(nuovoAtleta1.getNome(), "Francy");
			
			Atleta nuovoAtleta3 = atletaDao.update(nuovoAtleta2);
			s.getTransaction().commit();
			
			s.beginTransaction();
			assertNotEquals(nuovoAtleta2, nuovoAtleta3);
			
			assertEquals(nuovoAtleta3, nuovoAtleta1);		
			
			assertNotEquals(nuovoAtleta1.getNome(), "Francesca");
			assertEquals(nuovoAtleta1.getNome(), "Francy");
			
			assertEquals(atletaDao.findAll().size(), 1);
			
			s.getTransaction().commit();
		}
		catch (Exception e) {
			fail("Errore inatteso: " + e);
		}
	}
	
}

