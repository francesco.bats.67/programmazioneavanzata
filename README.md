PROGETTO DA CARICARE IN ECLIPSE


Effettuare il RUN ON SERVER (ho usato Tomcat 8.0)


Lanciare le seguenti applicazioni per popolare la base di dati (package wp.test):
 (1) InitDBWaterPolo.java (obbligatoria) ---> inizializza il DB con giocatrici, squadre e partite
 (2) InitDBWP_CaricaFormazioni.java (opzionale) ---> carica le formazioni di alcune partite da disputare, opzione che 
può essere fatta tramite applicazione ma che è più lunga e complessa.
 (3) InitDBWP_CaricaScore.java (opzionale) ---> carica gli score di alcune partite realmente disputate, in modo che si possano consultare i tabellini delle gare nelle tre modalità.

Per utilizzare l'applicazione, dalla home page:
- gli utenti generici possono consultare giocatrici, squadre e partite, con i tabellini in tre modalità diverse.
- l'utente admin può associare e dissociare le giocatrici dalle squadre, e può modificare i dati delle giocatrici.
- l'utente user può inserire le formazioni delle squadre nelle partite e acquisire lo score di una partita (vedi sotto).

Utenti per accedere alle aree riservate:
- user1 (password: user1) ---> acquisizione score delle partite
- user2 (password: user2) ---> acquisizione score delle partite
 -admin1 (password: admin1) ---> gestione giocatrici e assoc. squadre


--- ACQUISIZIONE DELLO SCORE DI UNA PARTITA ---

E' possibile acquisire lo SCORE di una partita solo se le formazioni sono state già inserite (in questo caso le partite in lista sono evidenziate in giallo), dal link che appare dalla pagina REPORT della partita.
Iniziando dallo sprint iniziale, si possono inserire gli eventi della gara selezionando il bottone apposito a seconda della squadra in attacco, e scegliendo le opzioni di ogni tipo di azione, l'applicazione aggiorna automaticamente sia le statistiche delle giocatrici e delle squadre, sia l'elenco delle azioni.
E' possibile passare alla visualizzazione del tabellino (in modalità REPORT, BOXSCORE o PLAY BY PLAY) e tornare allo score. Lo score può anche essere interrotto cambiando pagina, rientrando dalla lista partite o dalla lista score (partite evidenziate in rosso) ritorna al punto in cui si era interrotto.
Alla fine di ogni tempo si deve selezionare FINE TEMPO, i dati vengono aggiornati e rientrando si riparte dal tempo successivo.



